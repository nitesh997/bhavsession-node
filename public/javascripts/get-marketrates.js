var globalMatches = [], globalTournaments = [], globalCompetitions = [];

$(document).ready( function() {

    var form_error = $('#form-error');

    var token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }

    setCompetitionSelect()
    .then(() => {
        setTournamentSelect()
        .then(() => setMatchSelect())
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));

    $('#competition-select').on('change', e => {
        setCompetitionDetails($(e.target).val());
        setTournamentSelect()
        .then(() => setMatchSelect())
        .catch(err => console.log(err));
    });

    $('#tournament-select').on('change', e => {
        setTournamentDetails($(e.target).val());
        setMatchSelect();
    });

    $('#match-select').on('change', e => {
        setMatchDetails($(e.target).val());
    });


} );


var getCompetitions = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: api_url + 'common/get_sport_list',
            type: 'POST',
            success: data => {
                // console.log(data);
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                resolve(data);
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
            }
        });
    });
};

var setCompetitionSelect = () => {
    return new Promise((resolve, reject) => {
        getCompetitions().then(competitions => {
            // console.log(competitions);
            var select = $('#competition-select');
            select.html('');
            select.append('<option value="" disabled selected>--Select Competitions--</option>');
            globalCompetitions = [];
            for (var i in competitions) {
                // console.log(competitions[i]);
                select.append('<option value="'+ competitions[i].EventType.Id +'">'+ competitions[i].EventType.Name +'</option>');

                // console.log(competitions[i].EventType.Id);
                // console.log(competitions[i]);

                globalCompetitions[competitions[i].EventType.Id] = competitions[i];
            }
            // console.log(globalCompetitions);
            setCompetitionDetails(select.val());

            resolve();
        }).catch(err => reject(err));
    });
};

var setCompetitionDetails = currentCompetition => {
    // console.log(globalCompetitions);
    if (currentCompetition) {
        $('#competition-name').val(globalCompetitions[currentCompetition.trim()].EventType.Name);
    } else {
        $('#competition-name').val('');
    }
};


var getTournaments = competitionId => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: api_url + 'common/get_tournament_list',
            type: 'POST',
            data: {"strEventTypeId": competitionId},
            success: data => {
                // console.log(data);
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                resolve(data);
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
            }
        });
    });
};

var setTournamentSelect = () => {
    return new Promise((resolve, reject) => {
        var competitionId = parseInt( $('#competition-select').val(), 10 );
        getTournaments(competitionId).then(tournaments => {
            var select = $('#tournament-select');
            select.html('');
            select.append('<option value="" disabled selected>--Select Tournaments--</option>');
            globalTournaments = [];
            for (var i in tournaments) {
                select.append('<option value="'+ tournaments[i].Competition.Id +'">'+ tournaments[i].Competition.Name +'</option>');
                globalTournaments[tournaments[i].Competition.Id] = tournaments[i];
            }
            setTournamentDetails(select.val());

            resolve();
        }).catch(err => reject(err));
    });
};

var setTournamentDetails = currentTournament => {
    if (currentTournament) {
        $('#tournament-name').val(globalTournaments[currentTournament.trim()].Competition.Name);
    } else {
        $('#tournament-name').val('');
    }
};


var getMatches = tournamentId => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: api_url + 'common/get_match_list',
            type: 'POST',
            data: {
                "strCompetitionId": tournamentId
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                resolve(data);
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
            }
        });
    });
};

var setMatchSelect = () => {
    return new Promise((resolve, reject) => {
        var tournamentId = parseInt( $('#tournament-select').val(), 10 );
        getMatches(tournamentId).then(matches => {
            var select = $('#match-select');
            select.html('');
            select.append('<option value="" disabled selected>--Select Matches--</option>');
            globalMatches = [];
            for (var i in matches) {
                var dateTimeStamp = parseInt(matches[i].Event.OpenDate.replace(/[^0-9]/g,''));
                // console.log(dateTimeStamp);
                var date = new Date(dateTimeStamp);
                var dateStr = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                // var dateStr = moment.unix(dateTimeStamp).format('YYYY-MM-DD HH:mm:ss');
                select.append('<option value="'+ matches[i].Event.Id +'">'+ matches[i].Event.Name +' ('+ dateStr +')</option>');
                matches[i].Event.DateTimeStamp = dateTimeStamp;
                globalMatches[matches[i].Event.Id] = matches[i];
            }
            setMatchDetails(select.val());

            resolve();
        }).catch(err => reject(err));
    });
};

var setMatchDetails = currentMatch => {
    if (currentMatch) {
        $('#match-name').val(globalMatches[currentMatch.trim()].Event.Name);
        var date = new Date(globalMatches[currentMatch.trim()].Event.DateTimeStamp);
        var dateStr = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        $('#match-date').val(dateStr);
    } else {
        $('#match-name').val('');
        $('#match-date').val('');
    }
};

var rateOldValue = "";
$(document).on('keyup', '#rate-difference', function(e) {
    var currentElementObj = this;
    var rateValue = $(currentElementObj).val();
    if(isNaN(rateValue) == true) {
        e.target.value = rateOldValue;
    }
    if(rateValue < 0 || rateValue > 99) {
        e.target.value = rateOldValue;
    }
    rateOldValue = e.target.value;
});
