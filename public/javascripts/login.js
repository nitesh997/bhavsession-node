$(document).ready( function() {
    $('#login-form').on('submit', (e) => {
        e.preventDefault();

        var username = $(this).find('input#username').val().trim();
        var password = $(this).find('input#password').val().trim();

        var form_error = $('#form-error');

        if ('' === username || '' === password) {
            form_error.css('display', 'block');
            form_error.html('Please enter username and password');

            return false;
        }

        var data = {
            username: username,
            password: password
        }

        $.post( '/login', data ).done( data => {
            if (data.err_message) {
                form_error.css('display', 'block');
                form_error.html(data.err_message);
                return;
            }
            
            if (data.token) {
                window.localStorage.setItem('bhav-session-token', data.token);
            }

            window.location.href = '/list-market';
        } );

        return false;
    });
} );