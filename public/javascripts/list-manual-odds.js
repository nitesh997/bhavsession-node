var form_error = false;
var token = false;

$(document).ready( function() {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }

    setManualOddsList();

} );