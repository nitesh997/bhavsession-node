var form_error = false;
var token = false;

var agentCode = "bhavsession";
var secretkey = "4ade4e7319425d06b18a7ccf560b6ebe6556g477";
var importmarketToken = "";

var sportsTable = null;
var tournamentTable = null;
var matchesTable = null;
var marketTable = null;
var selectedData = {
    selectedSportsType: {
        id: -1,
        name: ""
    },
    selectedTournament: {
        id: -1,
        name: ""
    },
    selectedMatch: {
        id: -1,
        name: ""
    },
}

$(document).ready(function () {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }
    getAccessToken();
});

var getAccessToken = () => {

    var data = {
        agentcode: agentCode,
        secretkey: secretkey
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_access_token',
            type: 'POST',
            data: data,
            beforeSend: xhr => {
                xhr.setRequestHeader('x-forwarded-for', '185.193.36.221');
            },
            success: data => {
                if (data != null && data != undefined && data.token) {
                    importmarketToken = data.token;
                    getSports();
                    getAgentList();
                }
                resolve();
            },
            error: err => {
                toastr.error('Error while getting token: ' + err.responseText);
                reject();
            }
        });
    });
}

var getSports = () => {
    return new Promise((resolve, reject) => {
        $('#agent,#market-type-container').addClass('d-none');
        $.ajax({
            url: '/api/get_sport_list',
            type: 'post',
            data: {
                'requestFrom': 'self'
            },
            success: data => {
                if (data != null && data != undefined) {
                    if (sportsTable != null)
                        sportsTable.destroy();
                    sportsTable = $('#sports-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "EventType.Name", "title": "Sports Name"},
                            {"data": "MarketCount", "title": "No Of Markets"},
                        ]
                    });
                    goToStep(1);
                    $('#sports-table tbody').on('click', 'tr', function () {
                        var rowdata = sportsTable.row(this).data();
                        selectedData.selectedSportsType.id = rowdata.EventType.Id;
                        selectedData.selectedSportsType.name = rowdata.EventType.Name;
                        getTournaments();
                    });
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting sports data');
                reject();
            }
        });
    });
};

var getTournaments = () => {
    $('#agent,#market-type-container').addClass('d-none');
    var data = {
        strEventTypeId: parseInt(selectedData.selectedSportsType.id),
        'requestFrom': 'self'
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_tournament_list',
            type: 'post',
            data: data,

            success: data => {
                console.log(data);
                if (data != null && data != undefined) {
                    if (tournamentTable != null)
                        tournamentTable.destroy();
                    tournamentTable = $('#tournament-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "Competition.Name", "title": "Tournament Name"},
                            {"data": "MarketCount", "title": "No of Markets"},
                        ]
                    });
                    goToStep(2);
                    $('#tournament-table tbody').on('click', 'tr', function () {
                        var rowdata = tournamentTable.row(this).data();
                        selectedData.selectedTournament.id = rowdata.Competition.Id;
                        selectedData.selectedTournament.name = rowdata.Competition.Name;
                        getMatches();
                    });
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting tournaments data');
                reject();
            }
        });
    });
};

var getMatches = () => {
    $('#agent,#market-type-container').addClass('d-none');
    var data = {
        strEventTypeId: parseInt(selectedData.selectedSportsType.id),
        strCompetitionId: parseInt(selectedData.selectedTournament.id),
        'requestFrom': 'self'
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_match_list',
            type: 'post',
            data: data,
            success: data => {
                console.log(data);
                if (data != null && data != undefined) {
                    if (matchesTable != null)
                        matchesTable.destroy();
                    matchesTable = $('#matches-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "Event.Name", "title": "Match Name"},
                            {
                                "data": "Event.OpenDate",
                                "title": "Match Date",
                                "render": function (data, type, full, meta) {
                                    var dateTimeStamp = parseInt(full.Event.OpenDate.replace(/[^0-9]/g, ''));
                                    var date = new Date(dateTimeStamp);
                                    var dateStr = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                                    return dateStr;
                                }
                            },
                            {"data": "MarketCount", "title": "No Of Markets"}
                        ]
                    });
                    goToStep(3);
                    $('#matches-table tbody').on('click', 'tr', function () {
                        var rowdata = matchesTable.row(this).data();
                        selectedData.selectedMatch.id = rowdata.Event.Id;
                        selectedData.selectedMatch.name = rowdata.Event.Name;
                        getMarkets();
                    });
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting match data');
                reject();
            }
        });
    });
};


var getMarkets = () => {
    var data = {
        strEventTypeId: parseInt(selectedData.selectedSportsType.id),
        strEventID: parseInt(selectedData.selectedMatch.id),
        'requestFrom': 'self'
    };
    $('#market-type-container').removeClass('d-none');
    let isFancy = $('input[name="isFancy"]:checked').val();
    if (isFancy == "1") {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: '/api/get_fancy_list',
                type: 'post',
                data: data,
                success: data => {
                    if (data != null && data != undefined) {
                        if (marketTable != null)
                            marketTable.destroy();
                        marketTable = $('#market-table').DataTable({
                            data: data.appdata,
                            lengthMenu: [10,50, 100, 150, 200],
                            pageLength: 10,
                            columns: [
                                {"data": "marketName", "title": "Market Name"},
                                {
                                    "title": "Import",
                                    'searchable': false,
                                    'orderable': false,
                                    'className': 'text-center',
                                    "render": function (data, type, full, meta) {
                                        return "<button class='btn btn-danger' onclick=\"importFancyMarket('" + full.marketID + "')\" ><i class='md md-cloud-download'></i></button>";
                                    }
                                }
                            ]
                        });
                        goToStep(4);
                        resolve();
                        return;
                    } else {
                        reject();
                        return;
                    }
                },
                error: err => {
                    toastr.error('Error while getting markets data: ');
                    reject();
                }
            });

        });
    } else {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: '/api/get_market_list',
                type: 'post',
                data: data,
                success: data => {
                    console.log(data);
                    if (data != null && data != undefined) {
                        if (marketTable != null)
                            marketTable.destroy();
                        marketTable = $('#market-table').DataTable({
                            data: data,
                            lengthMenu: [10,50, 100, 150, 200],
                            pageLength: 10,
                            columns: [
                                {"data": "MarketName", "title": "Market Name"},
                                {
                                    "title": "Import",
                                    'searchable': false,
                                    'orderable': false,
                                    'className': 'text-center',
                                    "render": function (data, type, full, meta) {
                                        //return "<button class='btn btn-danger' onclick=\"importMarket('" + full.MarketId + "')\" ><i class='md md-cloud-download'></i></button>";
                                        return "<button class='btn btn-danger' onclick=\"importMatchOdsMarket('" + full.MarketId + "')\" ><i class='md md-cloud-download'></i></button>";
                                    }
                                }
                            ]
                        });
                        goToStep(4);
                        resolve();
                        return;
                    } else {
                        reject();
                        return;
                    }
                },
                error: err => {
                    toastr.error('Error while getting markets data: ');
                    reject();
                }
            });

        });
    }

};

var importMarket = (market_id) => {
    var data = {
        sport_id: parseInt(selectedData.selectedSportsType.id),
        tournament_id: parseInt(selectedData.selectedTournament.id),
        event_id: parseInt(selectedData.selectedMatch.id),
        market_id: parseInt(market_id),
        access_token: importmarketToken
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/import_market',
            type: 'post',
            data: data,
            success: data => {
                toastr.success('Market Imported successfully.');
                if (data != null && data != undefined) {
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while Import Market');
                reject();
            }
        });
    });
}
var importMatchOdsMarket = (market_id) => {
    var data = {
        sport_id: parseInt(selectedData.selectedSportsType.id),
        tournament_id: parseInt(selectedData.selectedTournament.id),
        event_id: parseInt(selectedData.selectedMatch.id),
        market_id: market_id.toString(),
        access_token: importmarketToken,
        agentIDs: JSON.stringify($('#agent').val()),
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/import_matchodds_bs',
            type: 'post',
            data: data,
            success: data => {
                if (data.status == "error") {
                    toastr.error(data.message);
                } else if (data.status == "success") {
                    if(data.message == "")
                        toastr.success('Market Imported successfully.');
                    else
                        toastr.success(data.message);
                }

                if (data != null && data != undefined) {
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while Import Market');
                reject();
            }
        });
    });
}


var importFancyMarket = (market_id) => {
    var data = {
        sport_id: parseInt(selectedData.selectedSportsType.id),
        tournament_id: parseInt(selectedData.selectedTournament.id),
        event_id: parseInt(selectedData.selectedMatch.id),
        market_id: parseInt(market_id),
        sportName: selectedData.selectedSportsType.name,
        tournamentName: selectedData.selectedTournament.name,
        access_token: importmarketToken,
        agentIDs: JSON.stringify($('#agent').val()),
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/import_fancy_bs',
            type: 'post',
            data: data,
            success: data => {
                if (data.status == "error") {
                    toastr.error(data.message);
                } else if (data.status == "success") {
                    if(data.message == "")
                        toastr.success('Market Imported successfully.');
                    else
                        toastr.success(data.message);
                }

                if (data != null && data != undefined) {
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while Import Market');
                reject();
            }
        });
    });
}


var setBreadcum = () => {
    var navdiv = $('#Navigation');
    navdiv.html('');
    var html = "";

    if (selectedData.selectedSportsType.id > -1) {
        html += "<a href='javascript:void(0)' onclick=\"goToStep(1)\"> All Sports </a>";
        if (selectedData.selectedTournament.id > -1) {
            html += " / <a href='javascript:void(0)' onclick=\"goToStep(2)\"> " + selectedData.selectedSportsType.name + " </a>";

            if (selectedData.selectedMatch.id > -1) {
                html += " / <a href='javascript:void(0)' onclick=\"goToStep(3)\"> " + selectedData.selectedTournament.name + " </a>";
                html += " / <span> " + selectedData.selectedMatch.name + " </span>";
            } else {
                html += " / <span> " + selectedData.selectedTournament.name + " </span>";
            }
        } else {
            html += " / <span> " + selectedData.selectedSportsType.name + " </span>";
        }
    } else {
        html += "<span> All Sports </span>"
    }
    navdiv.html(html);
}


var goToStep = (step) => {
    switch (step) {
        case 1:
            $('#sports-table').parents('div.dataTables_wrapper').first().show();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            selectedData = {
                selectedSportsType: {
                    id: -1,
                    name: ""
                },
                selectedTournament: {
                    id: -1,
                    name: ""
                },
                selectedMatch: {
                    id: -1,
                    name: ""
                },
            }
            break;
        case 2:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().show();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            selectedData.selectedTournament = {
                id: -1,
                name: ""
            }
            selectedData.selectedMatch = {
                id: -1,
                name: ""
            }
            break;
        case 3:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().show();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            selectedData.selectedMatch = {
                id: -1,
                name: ""
            }
            break;
        case 4:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().show();
            break;
    }
    setBreadcum();
}
$('input[name="isFancy"]').change(function (e) {
    getMarkets();
});

var getAgentList = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/agent/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();
                    return;
                }
                if (data) {
                    response = data.agents;
                    let html = '';
                    if (response.length > 0) {
                        for (key in response) {
                            html += '<option value="' + response[key].id + '">' + response[key].name + '</option>';
                        }
                    }
                    $('#agent').html(html);
                    $('#agent').select2({
                        multiple: true,
                        closeOnSelect: false
                    });
                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    reject();
                    return;
                }
                reject();
            }
        });
    });
}
