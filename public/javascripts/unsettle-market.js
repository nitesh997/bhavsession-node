var form_error = false;
var token = false;
var tempMarketId = 0;
var unsettledTable = undefined;
$(document).ready(function () {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }  
    setUnsettleMarketList();

});

var setUnsettleMarketList = () => {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/unsettle-market/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();

                    return;
                }

                if (data.market) {
                    form_error.html('');
                    if ( $.fn.DataTable.isDataTable('#unsettled-table') ) {
                        $('#unsettled-table').DataTable().destroy();
                    }
                    
                    $('#tblRemittanceList tbody').empty();
                    unsettledTable = $('#unsettled-table').DataTable({
                        data: data.market,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            { "data": "centralMarketID", "title": "Central MarketId"},
                            { "data": "marketName" , "title": "Market Name"},
                            { "data": "matchName" , "title": "Match Name"},
                            { "data": "matchDate" , "title": "Match Date",
                              "render" : function(data, type, full, meta){
                                  var date = new Date(data);
                                  var dateStr = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                                return dateStr;
                              }
                            },
                            { "title": "Result", 
                                'searchable': false,
                                'orderable': false,
                                'className': 'text-center',
                                "render": function (data, type, full, meta) {
                                    return "<button class='btn btn-danger' onclick=\"getRunners('" + full.centralMarketID + "')\" data-toggle='modal' data-target='#runner-modal'><i class='md md-block'></i></button>";
                                }
                            }
                        ],
                        createdRow: function (row, data, index) {
                            $(row).addClass("id_"+data.centralMarketID);
                        }
                    });

                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    reject();
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
                reject();
            }
        });
    });
};

var getRunners = marketId => {
    $.ajax({
        url: '/market/get_runners/' + marketId,
        type: 'GET',
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);
                reject();

                return;
            }
            $("#hdnMarketId").val(marketId);
            $('#runner-modal #runner-modal-title').html(data.market[0].matchName + ' - ' + data.market[0].marketName);
            const runners = data.runners;
            var tbody = $('tbody#runner-table-body');
            tbody.html('');
            if (runners.length > 0) {
                for (const [index, item] of runners.entries()) {
                    var html = '<tr>';
                    html += '<td>' + (index + 1) + '</td>';
                    html += '<td>' + item.name + '</td>';
                    html += '<td><button class="btn btn-danger" onclick="setRunner(' + data.market[0].centralMarketID + ', ' + item.id + ')"><i class="md md-block"></i></button></td>'
                    html += '</tr>'
                    tbody.append(html);
                }
            } else {
                tbody.append('<tr colspan="3>No data found!</td>');
            }
        },
        error: err => {
            if ('Forbidden' === err.responseText) {
                alert('Please login again.');
                window.location.href = '/login';
                reject();
                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while getting match data: ' + err.responseText);
            reject();
        }
    });
}
$(document).on("click","#submitResult",function(){
    var marketId = $("#hdnMarketId").val();
    var runnerId = $("#resultText").val();
    if(confirm("Are you sure you want to announce result?"))
    {
        setRunner(marketId, runnerId);
    }
    
});
$(document).on("click","#cancelMarket",function(){
    var marketId = $("#hdnMarketId").val();
    var runnerId = "-999";
    if(confirm("Are you sure you want to announce result?"))
    {
        setRunner(marketId, runnerId);
    }
});
$(document).on("click","#tieMarket",function(){
    var marketId = $("#hdnMarketId").val();
    var runnerId = "-888";
    if(confirm("Are you sure you want to announce result?"))
    {
        setRunner(marketId, runnerId);
    }
});
var setRunner = (marketId, runnerId) => {
    tempMarketId = marketId;
    let data = {
        Result: runnerId
    };
    $.ajax({
        url: '/bookmaker/update/' + marketId,
        type: 'POST',
        data: data,
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);

                return;
            }

            form_error.prop('class', 'form-success');
            form_error.html('Request made successfully.');
            $("#runner-modal").modal("toggle");
            unsettledTable.row($(".id_"+tempMarketId)).remove().draw(false);
        },
        error: err => {
            console.log(err);

            elem.prop('disabled', '');

            if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');

                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while updating data: ' + err.responseText);
        }
    });
}