var form_error = false;
var token = false;

$(document).ready( function() {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }

    setMarketList();

} );

var getBetStatusColor = betStatus => {
    switch (betStatus) {
        case 'Open': return 'primary';
        case 'Inactive': return 'secondary';
        case 'Suspended': return 'success';
        case 'Closed': return 'danger';
        case 'Settled': return 'warning';
        case 'Inplay': return 'info';
        case 'OverLimitSuspended': return 'light';
        case 'ReopenBat': return 'dark';
        case 'BallStart': return 'primary';
        default: return 'info';
    }
};

var setMarketListHtml = (marketList, betStatus = false) => {
    var tbody = $('tbody#fancy-table-tbody');
    var isEmpty = true;

    for (var i in marketList) {
        if (false !== betStatus && betStatus !== marketList[i].betStatus) {
            continue;
        }

        isEmpty = false;
        var hiddenID = 'hidden-marketObj-' + marketList[i].centralMarketID;
        var date = new Date(marketList[i].matchDate);
        var dateStr = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

        var html = '<tr>';
        html += '<td><input type="checkbox"</td>';
        html += '<td>' + marketList[i].marketName + '</td>';
        html += '<td>' + marketList[i].matchName + '</td>';
        html += '<td>' + dateStr + '</td>';
        html += '<td><span class="badge badge-'+ getBetStatusColor(marketList[i].betStatus) +'">' + marketList[i].betStatus + '</span></td>';
        html += '<td><a href="/fancy-rate/'+ marketList[i].centralMarketID +'" target="_blank" class="btn btn-success" style="border-radius: 50%;"><i class="md md-add"></i></a></td>';
        html += '<td>' + '<label class="switch"><input onchange="toggleSwitch(this, '+ marketList[i].centralMarketID +', \'isFancyCode\')" type="checkbox" ' + (1 === marketList[i].isFancyCode ? 'checked' : '') + '><span class="slider round"></span></label>' + '</td>';
        html += '<td>' + '<label class="switch"><input onchange="toggleSwitch(this, '+ marketList[i].centralMarketID +', \'isCommissionOn\')" type="checkbox" ' + (1 === marketList[i].isCommissionOn ? 'checked' : '') + '><span class="slider round"></span></label>' + '</td>';
        html += '<td>' + '<label class="switch"><input onchange="toggleSwitch(this, '+ marketList[i].centralMarketID +', \'isActive\')" type="checkbox" ' + (1 === marketList[i].isActive ? 'checked' : '') + '><span class="slider round"></span></label>' + '</td>';
        html += '<td>';
        html += '<button onclick="setModalValues(\''+ hiddenID +'\')" type="button" class="btn btn-warning" data-toggle="modal" data-target="#action-modal"><i class="md md-edit"></i></button>';
        html += '<input type="hidden" value=\''+ JSON.stringify(marketList[i]) +'\' id="'+ hiddenID +'">';
        html += '</td>';
        html += '</tr>';

        tbody.append(html);
    }

    if (isEmpty) {
        tbody.append('<tr><td colspan="100"><center>No Records Found</center></td></tr>');
    }
};

var setModalValues = hiddenID => {
    marketObj = JSON.parse($('#'+hiddenID).val());
    $('#action-modal #modal-title-market-name').html(marketObj.marketName);
    $('#action-modal #max-stack').val(marketObj.maxStack);
    $('#action-modal #rate-difference').val(marketObj.rateDifference);
    $('#action-modal #auto-ball-start-duration').val(marketObj.autoBallStartDuration);
    $('#action-modal #market-id').val(marketObj.marketID);
    $('#modal-error').html('');
};

var saveMarketValues = () => {
    var marketID = $('#market-id').val();
    var maxStack = $('#max-stack').val();
    var rateDiff = $('#rate-difference').val();
    var autoBall = $('#auto-ball-start-duration').val();

    var modal_error = $('#modal-error');
    modal_error.html('');

    var data = {
        maxStack: maxStack,
        rateDifference: rateDiff,
        autoBallStartDuration: autoBall
    }

    var saveBtn = $('#modal-save-btn');
    saveBtn.prop('disabled', 'disabled');

    $.ajax({
        url: '/market/update/' + marketID,
        type: 'POST',
        data: data,
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            saveBtn.prop('disabled', '');

            if (data.err_message) {
                modal_error.prop('class', 'form-error');
                modal_error.html(data.err_message);

                return;
            }

            modal_error.prop('class', 'form-success');
            modal_error.html('Data changed successfully.');

            var marketObj = JSON.parse($('#hidden-marketObj-'+marketID).val());
            marketObj.maxStack = maxStack;
            marketObj.rateDifference = rateDiff;
            marketObj.autoBallStartDuration = autoBall;
            $('#hidden-marketObj-'+marketID).val(JSON.stringify(marketObj));
        },
        error: err => {
            console.log(err);

            saveBtn.prop('disabled', '');

            if ('Forbidden' === err.responseText) {
                modal_error.prop('class', 'form-error');
                modal_error.html('Token expired. You need to login again.');

                return;
            }

            modal_error.prop('class', 'form-error');
            modal_error.html('Error while saving data: ' + err.responseText);
        }
    });

    return false;
};

var toggleSwitch = (elem, marketID, field) => {
    elem = $(elem);
    if (!confirm('Are you sure about the changes?')) {
        if (elem.prop('checked')) {
            elem.prop('checked', '');
        } else {
            elem.prop('checked', 'checked');
        }

        return;
    }

    var data = {};
    form_error.html('');
    data[field] = elem.prop('checked') ? 1 : 0;

    elem.prop('disabled', 'disabled');
    $.ajax({
        url: '/market/update/' + marketID,
        type: 'POST',
        data: data,
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            elem.prop('disabled', '');

            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);

                return;
            }

            form_error.prop('class', 'form-success');
            form_error.html(field + ' field changed successfully.');
        },
        error: err => {
            console.log(err);

            elem.prop('disabled', '');

            if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');

                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while updating data: ' + err.responseText);
        }
    });
};


var setMarketList = (betStatus = false) => {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/market/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();

                    return;
                }

                if (data.market) {
                    form_error.html('');

                    setMarketListHtml(data.market, betStatus);
                    $('#fancy-table').DataTable();

                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    window.location.href = '/login';
                    reject();
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
                reject();
            }
        });
    });
};

var changeFancyList = selectObj => {
    selectObj = $(selectObj);
    selectObj.prop('disabled', 'disabled');

    $('#fancy-table-tbody').html('');

    var betStatus = false;
    if (selectObj.val() !== '0') {
        betStatus = selectObj.val();
    }

    setMarketList(betStatus)
    .then(() => selectObj.prop('disabled', ''))
    .catch(() => selectObj.prop('disabled', ''));
};

var rateOldValue = "";
$(document).on('keyup', '#rate-difference', function(e) {
    var currentElementObj = this;
    var rateValue = $(currentElementObj).val();
    if(isNaN(rateValue) == true) {
        e.target.value = rateOldValue;
    }
    if(rateValue < 0 || rateValue > 99) {
        e.target.value = rateOldValue;
    }
    rateOldValue = e.target.value;
});
