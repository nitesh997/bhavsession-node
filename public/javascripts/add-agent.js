var agentObj = null;
var form_error = $('#form-error');
var token = window.localStorage.getItem('bhav-session-token');
$(document).ready( function() {
    if (!token) {
        window.location.href = '/login';
        return;
    }
    var getAgentIDfromUrl = () => {
        var ar = window.location.href.split('/');
        return parseInt(ar[ar.length - 1], 10);
      };
      var getAgentDetails = () => {
        var id = getAgentIDfromUrl();
      
        return new Promise((resolve, reject) => {
          $.ajax({
            url: '/agent/get_details/' + id,
            type: 'GET',
            beforeSend: xhr => {
              xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
              if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);
                reject();
      
                return;
              }
      
              agentObj = data.agent.length > 0 ? data.agent[0]: {};
              resolve();
      
            },
            error: err => {
              console.log(err);
      
              if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');
                reject();
      
                return;
              }
      
              form_error.prop('class', 'form-error');
              form_error.html('Error while getting market data: ' + err.responseText);
              reject();
            }
          });
        });
      };
      var id = getAgentIDfromUrl();
      if(id > 0){
        getAgentDetails().then(() => {
            $('#name').val(agentObj.name);
            $('#username').val(agentObj.username);
            $('#ip1').val(agentObj.ip1);
            $('#ip2').val(agentObj.ip2);
            $('#ip3').val(agentObj.ip3);
            $('#ip4').val(agentObj.ip4);
            $('#agentcode').val(agentObj.agentcode);
            $('#import_api_url').val(agentObj.import_api_url);
            $('#bs_secret_key').val(agentObj.bs_secret_key);
            $('#isAutoImportMatchOdds').prop('checked', agentObj.isAutoImportMatchOdds == 1);
          });
      }
   

    $('#agentform').bootstrapValidator({
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Agent Name is required.'
                    }
                }
            },
            username: {
                validators: {
                    notEmpty: {
                        message: 'Username is required.'
                    }
                }
            },
            agentcode: {
                validators: {
                    notEmpty: {
                        message: 'Agent Code is required.'
                    }
                }
            },
            import_api_url: {
                validators: {
                    notEmpty: {
                        message: 'Import API Url is required.'
                    },
                }
            }
        },
        submitHandler: function (validator, form, submitButton) {

        }
    }).on('reset', function (event) {
        $('#form-tab').data('bootstrapValidator').resetForm();
        //this.getBystateId();
    }).on('submit', function (event) {
        $("#save-btn").attr('disabled', false);
    }).on('error.field.bv', function (e, data) {
        if (data.bv.getInvalidFields().length > 0) {
            data.bv.disableSubmitButtons(false);
        }
    }).on('success.field.bv', function (e, data) {
        if (data.bv.getSubmitButton()) {
            data.bv.disableSubmitButtons(false);
        }
    });
    $("#agentform").submit(function(ev){ev.preventDefault();});
    $('#save-btn').on('click', e => {
        submitForm(e);
    }); 
});

var submitForm = (e)=> {
    form_error.html('');
    $("#agentform").submit(function(ev){ev.preventDefault();});
    var data = $("#agentform").serialize();
    var formData = JSON.parse(JSON.stringify(jQuery('#agentform').serializeArray()));
    var data = {};
    for (var i in formData) {            
        data[formData[i].name] = formData[i].value;
    }
    data.isAutoImportMatchOdds = $('#isAutoImportMatchOdds').prop('checked') ? 1 : 0;
    var isValid = $('#agentform').data('bootstrapValidator').isValid();
    if(isValid){
        var saveBtn = $('button#save-btn');
        saveBtn.prop('disabled', true);
        
        var apiUrl = '/agent/add_agent';
        if(agentObj && agentObj.id > 0)
            apiUrl = '/agent/update/'+ agentObj.id;

        $.ajax({
            url: apiUrl,
            type: 'POST',
            data: data,
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                saveBtn.prop('disabled', false);

                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                form_error.prop('class', 'form-success');
               
                if(agentObj && agentObj.id > 0){
                    toastr.success('Agent updated successfully.');
                }
                else
                {
                    toastr.success('Agent added successfully.');
                }
                location.href="/agents";
            },
            error: err => {
                saveBtn.prop('disabled', false);

                if ('Forbidden' === err.responseText) {
                    form_error.prop('class', 'form-error');
                    form_error.html('Token expired. You need to login again.');

                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while inserting agent data: ' + err.responseText);
            }
        });
    }
    return false;
}