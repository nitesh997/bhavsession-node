var globalMatches = [], globalTournaments = [], globalCompetitions = [];
var form_error, runnerListCounter=1, runnerList=[];

$(document).ready( function() {

    form_error = $('#form-error');

    var token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }

    setCompetitionSelect(function(result){
        console.log('select competition populated');
    });

    $('#competition-select').on('change', e => {
        setTournamentSelect($(e.target).val(),function(result){
            console.log('select competition populated');
        });
    });

    $('#tournament-select').on('change', e => {
        setMatchSelect($(e.target).val(),function(result){
            console.log('select tournament populated');
        });
    });

    $('#match-select').on('change', e => {
        setMarketSelect($(e.target).val(),function(result){
            console.log('select match populated');
        });
    });

    $('#appMarketName').on('change', e => {
        setRunnerList($(e.target).val(),function(result){
            console.log('runner list populated');
        });
    });

    $(".allow-number").keypress(function(e){
        if(e.keyCode<48 || e.keyCode>57){
            e.preventDefault();
        }
    });

    $(".allow-decimal").keypress(function(e){
        if((e.keyCode>=48 && e.keyCode<=57) || (e.keyCode==46)){
            
        }
        else{
            e.preventDefault();
        }
    });

    $(".length-5").keypress(function(e){
        if(e.target.value.length>=5){
            e.preventDefault();
        }
    });

    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY hh:mm:ss A',
        sideBySide: true
    });

    $('.datepicker').keypress(function(e){
        e.preventDefault();
    });

    $('form#market-form').on('submit', (e) => {
        e.preventDefault();
        form_error.html('');var dataList = [];
        dataList['marketName'] = {selector: '#appMarketTitle', msg: 'Invalid market Title'};
        dataList['maxStack'] = {selector: '#appMaxStack', msg: 'Invalid max stack'};
        dataList['competitionID'] = {selector: '#competition-select', msg: 'Invalid Competition'};
        dataList['tournamentID'] = {selector: '#tournament-select', msg: 'Invalid Tournament'};

        var attrList = [];
        attrList['matchID'] = {selector: '#match-select', msg: 'Invalid match ID', attribute: 'data-match-id'};
        attrList['matchDate'] = {selector: '#match-select', msg: 'Invalid match date', attribute: 'data-match-date'};
        attrList['matchName'] = {selector: '#match-select', msg: 'Invalid match details', attribute: 'data-match-name'};
        attrList['tournamentName'] = {selector: '#tournament-select', msg: 'Invalid tournament details', attribute: 'data-tournament-name'};
        attrList['competitionName'] = {selector: '#competition-select', msg: 'Invalid competition details', attribute: 'data-competition-name'};
        
        let date = new Date().toISOString();
        let afterBetAllowFalse = $('#appAfterBetAllowFalse').data('DateTimePicker').date();
        let afterSuspend = $('#appAfterSuspend').data('DateTimePicker').date();
        let afterClose = $('#appAfterClose').data('DateTimePicker').date();
        
        if(afterBetAllowFalse!=null && afterBetAllowFalse!=undefined)
            afterBetAllowFalse=afterBetAllowFalse.toISOString();
        if(afterSuspend!=null && afterSuspend!=undefined)
            afterSuspend=afterSuspend.toISOString();
        if(afterClose!=null && afterClose!=undefined)
            afterClose=afterClose.toISOString();

        var data = {
            autoBallStartDuration: $('#appBallStartSec').val().trim() == "" ? 0 : $('#appBallStartSec').val().trim(),
            rateDifference: $('#appRateDifferent').val().trim(),
            description: $('#description').val().trim(),
            afterBetAllowFalse:  dataList['afterBetAllowFalse'],
            afterSuspend: dataList['afterSuspend'],
            afterClose: dataList['afterClose'],
            isCommissionOn: $('#appIsApplyComission').prop('checked') ? 1 : 0,
            isActive: $('#appIsActive').prop('checked') ? 1 : 0,
            betStatus: 'Inactive',
            status: 2,
            createdDateTime: date,
            updatedDateTime: date,
            marketType: 5,
            runnerList: []
        };

        $("#tables_ui #tblBody tr").each(function() {
            let runner = {};
            runner.name = $(this).find("td:eq(2)").html();
            runner.marketID = 0;
            data.runnerList.push(runner);
        });

        for (var i in dataList) {
            var value = $(dataList[i].selector).val();
            if (!value || (value && '' === value.trim())) {
                form_error.prop('class', 'form-error');
                form_error.html(dataList[i].msg);

                return false;
            }
            data[i] = value;
        }

        for (var i in attrList) {
            var value = $('option:selected', attrList[i].selector).attr(attrList[i].attribute);
            if (!value || (value && '' === value.trim())) {
                form_error.prop('class', 'form-error');
                form_error.html(dataList[i].msg);

                return false;
            }
            data[i] = value;
        }

        var saveBtn = $('button#saveBtn');
        saveBtn.prop('disabled', true);

        $.ajax({
            url: '/manual-odds/add',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: "application/json",
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                saveBtn.prop('disabled', false);

                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                form_error.prop('class', 'form-success');
                form_error.html('Market added');
                location.href="/manual-odds";
            },
            error: err => {
                saveBtn.prop('disabled', false);

                if ('Forbidden' === err.responseText) {
                    form_error.prop('class', 'form-error');
                    form_error.html('Token expired. You need to login again.');

                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while inserting market data: ' + err.responseText);
            }
        });

        return false;
    });

});

var setCompetitionSelect = (callback) => {
    
    $.ajax({
        url: api_url + 'common/get_sport_list',
        type: 'POST',
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer abcd');
        },
        success: data => {
            var competitions = data;
            let options = '<option value="">-----Select One-----</option>';
            for (var i in competitions) {
                // console.log(competitions[i]);
                options += '<option data-competition-name="'+competitions[i].EventType.Name +'" value="'+ competitions[i].EventType.Id +'">'+ competitions[i].EventType.Name +'</option>';
            }
            $("#competition-select").html(options);
            callback(true);
        },
        error: err => {
            console.log(err);
            //elem.prop('disabled', '');
                callback(true);

            //form_error.prop('class', 'form-error');
            //form_error.html('Error while updating data: ' + err.responseText);
        }
    });
};

var setTournamentSelect = (competition,callback) => {
    
    $.ajax({
        url: api_url + 'common/get_tournament_list',
        type: 'POST',
        data: {"strEventTypeId": competition},
        success: data => {
            var tournaments = data;
            let options = '<option value="">-----Select One-----</option>';
            for (var i in tournaments) {
                options += '<option data-tournament-name="'+tournaments[i].Competition.Name +'" value="'+ tournaments[i].Competition.Id +'">'+ tournaments[i].Competition.Name +'</option>';
            }
            $("#tournament-select").html(options);
            callback(true);
        },
        error: err => {
            console.log(err);
            if ('error' === err.statusText) {
                let options = '<option value="">-----Select One-----</option>';
                $("#tournament-select").html(options);
                callback(true);
            }
        }
    });
};

var setMatchSelect = (tournament,callback) => {
    
    $.ajax({
        url: api_url + 'common/get_match_list',
        type: 'POST',
        data: {
            "strCompetitionId": tournament
        },
        success: data => {
            matches = data;

            let options = '<option value="">-----Select One-----</option>';
            for (var i in matches) {
                var dateTimeStamp = parseInt(matches[i].Event.OpenDate.replace(/[^0-9]/g,''));
                // console.log(dateTimeStamp);
                var date = new Date(dateTimeStamp);
                var dateStr = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                // var dateStr = moment.unix(dateTimeStamp).format('YYYY-MM-DD HH:mm:ss');
                options += '<option data-match-name="'+matches[i].Event.Name +'" data-match-id="'+matches[i].Event.Id+'" data-match-date="'+dateStr+'" value="'+matches[i].Event.Id+'">'+matches[i].Event.Name +' ('+ dateStr +')'+'</option>';
            }
            $("#match-select").html(options);
            callback(true);
        },
        error: err => {
            console.log(err);
            if ('error' === err.statusText) {
                let date = new Date().toISOString();
                let options = '<option value="">-----Select One-----</option>';
                $("#match-select").html(options);
                callback(true);
            }
        }
    });
};

var setMarketSelect = (match,callback) => {
    
    $.ajax({
        url: api_url + 'common/get_market_list',
        type: 'POST',
        data: {
            "strEventID": match
        },
        success: data => {
            $("#market_json").val(JSON.stringify(data));
            var markets = data;
            let options = '<option value="">-----Select One-----</option>';
            for (var i in markets) {
                options += '<option data-market-name="'+markets[i].MarketName+'" value="'+ markets[i].MarketId +'">'+ markets[i].MarketName +'</option>';
            }
            $("#appMarketName").html(options);
            callback(true);
        },
        error: err => {
            console.log(err);
            //elem.prop('disabled', '');

            if ('error' === err.statusText) {
                let options = '<option value="">-----Select One-----</option>';
                $("#appMarketName").html(options);
                callback(true);
            }

            //form_error.prop('class', 'form-error');
            //form_error.html('Error while updating data: ' + err.responseText);
        }
    });
};


var setRunnerList = (market,callback) => {
    var market_json = $("#market_json").val();
    var markets = JSON.parse(market_json);
    runnerListCounter=1;
    for(var i=0;i<markets.length;i++)
    {
        if(markets[i]["MarketId"] == market)
        {
            var runners = markets[i]["Runners"];
            let runnerBody = '';
            for(var j =0;j<runners.length;j++)
            {
                var runner = runners[j];
                runnerList.push(runner["RunnerName"]);
                runnerBody += '<tr><td class="text-center"><a href="javascript:" onclick="RemoveRunnerData(this, \''+runner["RunnerName"]+'\')" data-toggle="tooltip" title="Remove Runner"><i class="fa fa-minus-circle " style="font-size:20px;"></i></a></td><td class="weight600 text-center" style="width: 7px;">'+(runnerListCounter++)+'</td><td class="text-left">'+runner["RunnerName"]+'</td></tr>';
            }
            $("#tblBody").html(runnerBody);
            $("#frmrunner").show();
            callback(true);       
        }
    }
};

var SaveNewRunner = () => {
    let name = $("#appRunnerName").val();
    if(runnerList.indexOf(name)==-1){
        $("#tblBody").append('<tr><td class="text-center"><a href="javascript:" onclick="RemoveRunnerData(this,\''+name+'\')" data-toggle="tooltip" title="Remove Runner"><i class="fa fa-minus-circle " style="font-size:20px;"></i></a></td><td class="weight600 text-center" style="width: 7px;">'+(runnerListCounter++)+'</td><td class="text-left">'+name+'</td></tr>');
        runnerList.push(name);
        $("#appRunnerName").val('');
    }
    else{
        form_error.prop('class', 'form-error');
        form_error.html('Runner already exists');
    }
};

var RemoveRunnerData = (elem, name) => {
    if(runnerList.indexOf(name)>-1){
        runnerList.splice(runnerList.indexOf(name),1);
        $(elem).closest('tr').remove();
        runnerListCounter--;
    }
};