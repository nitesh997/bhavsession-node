var form_error = false;
var token = false;

$(document).ready( function() {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    console.log(token);
    if (!token) {
        window.location.href = '/login';
        return;
    }

    setManualOddsList();

});

var setManualOddsList = (betStatus = 'Inactive') => {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/manual-odds/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();

                    return;
                }

                if (data.market) {
                    form_error.html('');

                    setManualOddsListHtml(data.market, betStatus);
                    $('#manual-odds-table').DataTable();

                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    reject();
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
                reject();
            }
        });
    });
};

var setManualOddsListHtml = (marketList, betStatus = 'Inactive') => {
    var tbody = $('tbody#manual-odds-table-tbody');
    tbody.html('');
    var isEmpty = true;

    for (var i in marketList) {
        if ('Inactive' !== betStatus && betStatus !== marketList[i].betStatus) {
            continue;
        }

        isEmpty = false;
        var hiddenID = 'hidden-marketObj-' + marketList[i].centralMarketID;
        var date = new Date(marketList[i].matchDate);
        var dateStr = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

        var html = '<tr>';
        html += '<td><input type="checkbox"></td>';
        html += '<td>' + dateStr + '</td>';
        html += '<td>' + marketList[i].marketName + '</td>';
        html += '<td>' + marketList[i].matchName + '</td>';
        html += '<td><span class="badge badge-'+ getBetStatusColor(marketList[i].betStatus) +'">' + marketList[i].betStatus + '</span></td>';
        html += '<td><a href="/manual-odds-rate/'+ marketList[i].centralMarketID +'" target="_blank" class="btn btn-success" style="border-radius: 50%;"><i class="md md-add"></i></a></td>';
        html += '<td>' + '<label class="switch"><input onchange="toggleSwitch(this, '+ marketList[i].centralMarketID +', \'isActive\')" type="checkbox" ' + (1 === marketList[i].isActive ? 'checked' : '') + '><span class="slider round"></span></label>' + '</td>';
        html += '<td>' + '<label class="switch"><input onchange="toggleSwitch(this, '+ marketList[i].centralMarketID +', \'isCommissionOn\')" type="checkbox" ' + (1 === marketList[i].isCommissionOn ? 'checked' : '') + '><span class="slider round"></span></label>' + '</td>';
        html += '<td>' + getStatus(marketList[i]) + '</td>';
        html += '<td>';
        html += '<button onclick="setModalValues(\''+ hiddenID +'\')" type="button" class="btn btn-warning" data-toggle="modal" data-target="#action-modal"><i class="md md-edit"></i></button>';
        html += '<input type="hidden" value=\''+ JSON.stringify(marketList[i]) +'\' id="'+ hiddenID +'">';
        html += '</td>';
        html += '</tr>';

        tbody.append(html);
    }

    if (isEmpty) {
        tbody.append('<tr><td colspan="100"><center>No Records Found</center></td></tr>');
    }
};

var getStatus = market => {
    if (market.Result != null && market.Result != '') {
        return '<button class="btn btn-success"><i class="md md-check"></button>';
    } else if (market.status == '4') {
        return '<button class="btn btn-danger" onclick="getRunners(' + market.centralMarketID + ')" data-toggle="modal" data-target="#runner-modal"><i class="md md-block"></i></button>';
    } else {
        return '--';
    }
}

var getBetStatusColor = betStatus => {
    switch (betStatus) {
        case 'Open': return 'primary';
        case 'Inactive': return 'secondary';
        case 'Suspended': return 'success';
        case 'Closed': return 'danger';
        case 'Settled': return 'warning';
        case 'Inplay': return 'info';
        case 'OverLimitSuspended': return 'light';
        case 'ReopenBat': return 'dark';
        case 'BallStart': return 'primary';
        default: return 'info';
    }
};

var toggleSwitch = (elem, marketID, field) => {
    elem = $(elem);
    if (!confirm('Are you sure about the changes?')) {
        if (elem.prop('checked')) {
            elem.prop('checked', '');
        } else {
            elem.prop('checked', 'checked');
        }

        return;
    }

    var data = {};
    form_error.html('');
    data[field] = elem.prop('checked') ? 1 : 0;

    elem.prop('disabled', 'disabled');
    $.ajax({
        url: '/manual-odds/update/' + marketID,
        type: 'POST',
        data: data,
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            elem.prop('disabled', '');

            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);

                return;
            }

            form_error.prop('class', 'form-success');
            form_error.html(field + ' field changed successfully.');
        },
        error: err => {
            console.log(err);

            elem.prop('disabled', '');

            if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');

                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while updating data: ' + err.responseText);
        }
    });
};

var getRunners = marketId => {
    $.ajax({
        url: '/manual-odds/get_details/' + marketId,
        type: 'GET',
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);
                reject();

                return;
            }
            console.log(data);
            $('#runner-modal #runner-modal-title').html(data.market.matchName + ' - ' + data.market.marketName);
            const runners = data.market.runnerList;
            var tbody = $('tbody#runner-table-body');
            tbody.html('');
            if (runners.length > 0) {
                for (const [index, item] of runners.entries()) {
                    var html = '<tr>';
                    html += '<td>' + (index + 1) + '</td>';
                    html += '<td>' + item.runner_name + '</td>';
                    html += '<td><button class="btn btn-danger" data-dismiss="modal" onclick="setRunner(' + data.market.centralMarketID + ', ' + item.runner_id + ')"><i class="md md-block"></i></button></td>'
                    html += '</tr>'
                    tbody.append(html);
                }
            } else {
                tbody.append('<tr colspan="3>No data found!</td>');
            }
        },
        error: err => {
            if ('Forbidden' === err.responseText) {
                alert('Please login again.');
                window.location.href = '/login';
                reject();
                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while getting match data: ' + err.responseText);
            reject();
        }
    });
}

var setRunner = (marketId, runnerId) => {
    let data = {
        Result: runnerId
    };
    $.ajax({
        url: '/manual-odds/update/' + marketId,
        type: 'POST',
        data: data,
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);

                return;
            }

            form_error.prop('class', 'form-success');
            form_error.html('Request made successfully.');
            setManualOddsList();
        },
        error: err => {
            console.log(err);

            elem.prop('disabled', '');

            if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');

                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while updating data: ' + err.responseText);
        }
    });
}

var rateOldValue = "";
$(document).on('keyup', '#rate-difference', function(e) {
    var currentElementObj = this;
    var rateValue = $(currentElementObj).val();
    if(isNaN(rateValue) == true) {
        e.target.value = rateOldValue;
    }
    if(rateValue < 0 || rateValue > 99) {
        e.target.value = rateOldValue;
    }
    rateOldValue = e.target.value;
});