var vminvolume = 1;
var vmaxvolume = 9;
var showRateSize = $("#txtShowRate").val();
// set Valid Rate on Rate change event
function findAdjustedValue(strRate) {
  var vPoint = 0.01;
  var retObj = { diffPoint: 0, minVal: 0, maxVal: 0 };
  if (strRate >= 0 && strRate < 2) {
    vPoint = 0.01;
    retObj = { diffPoint: 0.01, minVal: 0, maxVal: 2 };
  }
  else if (strRate >= 2 && strRate < 3) {
    vPoint = 0.02;
    retObj = { diffPoint: 0.02, minVal: 2, maxVal: 3 };
  }
  else if (strRate >= 3 && strRate < 4) {
    vPoint = 0.05;
    retObj = { diffPoint: 0.05, minVal: 3, maxVal: 4 };
  }
  else if (strRate >= 4 && strRate < 6) {
    vPoint = 0.1;
    retObj = { diffPoint: 0.1, minVal: 4, maxVal: 6 };
  }
  else if (strRate >= 6 && strRate < 10) {
    vPoint = 0.2;
    retObj = { diffPoint: 0.2, minVal: 6, maxVal: 10 };
  }
  else if (strRate >= 10 && strRate < 20) {
    vPoint = 0.5;
    retObj = { diffPoint: 0.5, minVal: 10, maxVal: 20 };
  }
  else if (strRate >= 20 && strRate < 30) {
    vPoint = 1;
    retObj = { diffPoint: 1, minVal: 20, maxVal: 30 };
  }
  else if (strRate >= 30 && strRate < 50) {
    vPoint = 2;
    retObj = { diffPoint: 2, minVal: 30, maxVal: 50 };
  }
  else if (strRate >= 50 && strRate < 100) {
    vPoint = 5;
    retObj = { diffPoint: 5, minVal: 50, maxVal: 100 };
  }
  else if (strRate >= 100) {
    vPoint = 10;
    retObj = { diffPoint: 10, minVal: 100, maxVal: 1000 };
  }
  var strResponse = parseFloat(strRate);
  var strMode = (strResponse % vPoint).toFixed(2);
  if (strMode != 0) {
    var strtemp = strResponse - strMode;
    var ifinalRate = (strtemp + vPoint).toFixed(2);
    strResponse = ifinalRate;
  }
  retObj.value = strResponse;
  return retObj;
}
// Calculate Rate and return right value
function calculateRate(rate, isIncrement, iminBetRate, imaxBetRate) {
  var finalRate = 0;
  try {
    if (isIncrement) {
      finalRate = SetIncrementRate(rate, true);
    } else {
      finalRate = SetDecrementRate(rate, false);
    }
    if (finalRate <= iminBetRate) finalRate = iminBetRate;
    else if (finalRate >= imaxBetRate) finalRate = imaxBetRate;
  }
  catch (ex) {
    console.log("error:=" + ex);
  }
  return finalRate;
}
function SetIncrementRate(strRate, Isincrement) {
  var vPoint = 0.01;
  if (strRate >= 0 && strRate < 2) {
    vPoint = 0.01;
  }
  else if (strRate >= 2 && strRate < 3) {
    vPoint = 0.02;
  }
  else if (strRate >= 3 && strRate < 4) {
    vPoint = 0.05;
  }
  else if (strRate >= 4 && strRate < 6) {
    vPoint = 0.1;
  }
  else if (strRate >= 6 && strRate < 10) {
    vPoint = 0.2;
  }
  else if (strRate >= 10 && strRate < 20) {
    vPoint = 0.5;
  }
  else if (strRate >= 20 && strRate < 30) {
    vPoint = 1;
  }
  else if (strRate >= 30 && strRate < 50) {
    vPoint = 2;
  }
  else if (strRate >= 50 && strRate < 100) {
    vPoint = 5;
  }
  else if (strRate >= 100) {
    vPoint = 10;
  }
  var strResponse = parseFloat(strRate);
  if (Isincrement == true) {
    strResponse = parseFloat(parseFloat(strRate) + vPoint).toFixed(2);
  }
  else {
    strResponse = parseFloat(parseFloat(strRate) - vPoint).toFixed(2);
  }
  if (Isincrement == true) {
    var strMode = (parseFloat(strResponse) % vPoint).toFixed(2);
    if (strMode != 0) {
      var strtemp = parseFloat(parseFloat(strResponse) - parseFloat(strMode));
      var ifinalRate = (strtemp + vPoint).toFixed(2);
      strResponse = ifinalRate;
    }
  }
  return strResponse;
}
function SetDecrementRate(strRate, Isincrement) {
  var vPoint = 0.01;
  if (strRate > 0 && strRate <= 2) {
    vPoint = 0.01;
  }
  else if (strRate > 2 && strRate <= 3) {
    vPoint = 0.02;
  }
  else if (strRate > 3 && strRate <= 4) {
    vPoint = 0.05;
  }
  else if (strRate > 4 && strRate <= 6) {
    vPoint = 0.1;
  }
  else if (strRate > 6 && strRate <= 10) {
    vPoint = 0.2;
  }
  else if (strRate > 10 && strRate <= 20) {
    vPoint = 0.5;
  }
  else if (strRate > 20 && strRate <= 30) {
    vPoint = 1;
  }
  else if (strRate > 30 && strRate <= 50) {
    vPoint = 2;
  }
  else if (strRate > 50 && strRate <= 100) {
    vPoint = 5;
  }
  else if (strRate > 100) {
    vPoint = 10;
  }
  var strResponse = parseFloat(strRate);
  if (Isincrement == true) {
    strResponse = parseFloat(parseFloat(strRate) + vPoint).toFixed(2);
  }
  else {
    strResponse = parseFloat(parseFloat(strRate) - vPoint).toFixed(2);
  }
  if (Isincrement == true) {
    var strMode = (parseFloat(strResponse) % vPoint).toFixed(2);
    if (strMode != 0) {
      var strtemp = parseFloat(strResponse) - parseFloat(strMode);
      var ifinalRate = (strtemp + vPoint).toFixed(2);
      strResponse = ifinalRate;
    }
  }
  return strResponse;
}
$(document).keypress(function (event) {
  try {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (event.shiftKey && keycode == 73) {
      changeMarketStatusTo(2);
    } else if (event.shiftKey && keycode == 67) {
      changeMarketStatusTo(4);
    } else if (event.shiftKey && keycode == 66) {
      setMarketBallStart();
    } else if (event.shiftKey && keycode == 83) {
      changeMarketStatusTo(3);
    } else if (event.shiftKey && keycode == 13) {
      setBookmarkerRates(true, false);
    } else if (keycode == 13) {
      setBookmarkerRates(false);
    }
  } catch (ex) {
    console.log("Error on Page keypres Event : " + ex.message);
  }
});

function isNumber(evt) {
  var iKeyCode = evt.which ? evt.which : evt.keyCode;
  if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
    return false;
  } else {
    return true;
  }
}

function isDecimalNumberKey(evt, obj) {
  var charCode = evt.which ? evt.which : event.keyCode;
  var value = obj.value;
  var dotcontains = value.indexOf(".") != -1;
  if (dotcontains) {
    if (charCode == 46) {
      return false;
    }
  }
  if (charCode == 46) {
    return true;
  }
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}

function setFocusmainBox() {
  $("#txtMaketOdds").focus();
}
function setRunnerforEnterRate() {
  try {
    setRunnervolume(false);
    setActiveRunner($("#ddlOddsRunner").val());
  } catch (ex) {
    console.log(
      "Error on page in funcation setRunnerforEnterRate ::" + ex.message
    );
  }
  return false;
}

function setShowRateSize() {
  try {
    var vShowRate = $("#txtShowRate").val();
    if (vShowRate != undefined && vShowRate != "") {
      try {
        var vIntValue = parseInt(vShowRate);
        var minBetRate = 1;
        var maxBetRate = 3;
        if (vIntValue >= minBetRate && vIntValue <= maxBetRate) {
          showRateSize = vIntValue;
        } else {
          if (vIntValue < minBetRate) {
            showRateSize = minBetRate;
            $.growl.error({
              title: "Show Rate",
              message:
                "The minimum Show Rate are " +
                minBetRate +
                ". Your odds have been updated accordingly."
            });
          } else if (vIntValue > maxBetRate) {
            showRateSize = maxBetRate;
            $.growl.error({
              title: "Show Rate",
              message:
                "The maximum Show Rate are " +
                maxBetRate +
                ". Your odds have been updated accordingly."
            });
          }
        }
      } catch (ex) {
        showRateSize = 1;
      }
      $("#txtShowRate").val(showRateSize);
      $(".txtVolumeClear").val("");
      setRunnervolume(false);
      $(".txtRateClear").val("");
      checkMaretOdds();
    }
  } catch (ex) {
    console.log("Error on page in funcation setVolumelength ::" + ex.message);
  }
  return false;
}

function setVolumelength() {
  try {
    var vVolumelength = $("#txtVolumelength").val();
    if (vVolumelength != undefined && vVolumelength != "") {
      var objMin = "";
      var objMax = "";
      try {
        for (i = 0; i < parseInt(vVolumelength); i++) {
          objMin += "1";
          objMax += "9";
        }
        vminvolume = parseInt(objMin);
        vmaxvolume = parseInt(objMax);
      } catch (ex) {
        vminvolume = 0;
        vmaxvolume = 0;
      }
    } else {
      vminvolume = 0;
      vmaxvolume = 0;
    }
    setRunnervolume(true);
  } catch (ex) {
    console.log("Error on page in funcation setVolumelength ::" + ex.message);
  }
  return false;
}

function setRunnervolume(vIsAutoLoad) {
  try {
    var vRunnerlist = vRunnerInfo;
    if (vRunnerlist != undefined && vRunnerlist != null) {
      var items;
      try {
        items = JSON.parse(vRunnerlist);
      } catch (err) {
        items = vRunnerlist;
      }
      items.map(function (vobj) {
        var vRunnerSelectionID = vobj.runner_id;
        var vIsAutoVolume = $("#chkIsAutoVolume" + vRunnerSelectionID).prop(
          "checked"
        );
        if (vIsAutoVolume) {
          for (var i = 1; i <= showRateSize; i++) {
            var vRandomNumber =
              Math.floor(Math.random() * (vmaxvolume - vminvolume + 1)) +
              vminvolume;
            $(".txtBVolume" + vRunnerSelectionID + "_" + i.toString()).val(
              vRandomNumber
            );
            vRandomNumber =
              Math.floor(Math.random() * (vmaxvolume - vminvolume + 1)) +
              vminvolume;
            $(".txtLVolume" + vRunnerSelectionID + "_" + i.toString()).val(
              vRandomNumber
            );
          }
        }
      });
    }
    if (vIsAutoLoad == true)
      setTimeout(function () {
        setRunnervolume(vIsAutoLoad);
      }, 3000);
  } catch (ex) {
    console.log("Error on page in function setRunnervolume ::" + ex.message);
    if (vIsAutoLoad == true)
      setTimeout(function () {
        setRunnervolume(vIsAutoLoad);
      }, 3000);
  }
}

function setActiveRunner(vSelectionID) {
  try {
    $(".txtRateClear").val("");
    $(".tdoddsRunner")
      .parent()
      .removeClass("selectedrunner");
    $(".tdoddsRunner")
      .parent()
      .addClass("unselectedrunner");
    $("#tdoddsRunner_" + vSelectionID)
      .parent()
      .removeClass("unselectedrunner");
    $("#tdoddsRunner_" + vSelectionID)
      .parent()
      .addClass("selectedrunner");
    $("#ddlOddsRunner").val(vSelectionID);
    //$(".ddlOddsRunner").select2();
    $("#txtMaketOdds").val("");
    setFocusmainBox();
  } catch (ex) {
    console.log("Error on page in funcation setActiveRunner ::" + ex.message);
  }
}

var vCounter = 1;
function setMarketOdds(element, evt) {
  var iKeyCode = evt.which ? evt.which : evt.keyCode;
  if (iKeyCode == 38 && evt.ctrlKey) {
    //keycode for : up arrow with ctrl
    vCounter += 1;
    checkMaretOdds();
  }
  if (iKeyCode == 38 && evt.ctrlKey != true) {
    //keycode for : up arrow
    //vCounter += 1;
    if ($("#txtMaketOdds").val() != "") {
      var a = $("#txtMaketOdds").val();
      $("#txtMaketOdds").val(parseFloat(a) + 1);
    }
    checkMaretOdds();
  }
  if (iKeyCode == 40 && evt.ctrlKey) {
    //keycode for : down arrow with ctrl
    vCounter -= 1;
    checkMaretOdds();
  }
  if (iKeyCode == 40 && evt.ctrlKey != true) {
    //keycode for : down arrow
    //vCounter-=1;
    if ($("#txtMaketOdds").val() != "") {
      var a = $("#txtMaketOdds").val();
      $("#txtMaketOdds").val(parseFloat(a) - 1);
    }
    checkMaretOdds();
  } else if (iKeyCode == 8) {
    //keycode for : backspace
    checkMaretOdds();
  } else if (iKeyCode == 107) {
    //keycode for : +
    vCounter += 1;
    checkMaretOdds();
  } else if (iKeyCode == 109) {
    //keycode for : -
    vCounter -= 1;
    checkMaretOdds();
  } else if (iKeyCode == 37 || iKeyCode == 39) {
    //keycode for : left and Right arrow
    return false;
  } else if (
    (iKeyCode >= 48 && iKeyCode <= 57) ||
    (iKeyCode >= 96 && iKeyCode <= 105)
  ) {
    checkMaretOdds();
  } else if (iKeyCode == 76) {
    //keycode for : l
    setSameBackOddsOtherRunner(false, true);
    return false;
  } else if (iKeyCode == 66) {
    //keycode for : b
    setSameBackOddsOtherRunner(true, false);
    return false;
  }
}

function setSameBackOddsOtherRunner(isSetBack, isSetLay) {
  try {
    var txtMaketOdds = $("#txtMaketOdds").val();
    var vMarketRateOdd = parseFloat(txtMaketOdds);
    var vSelectionRunnerID = $("#ddlOddsRunner").val();
    if (
      vMarketRateOdd != undefined &&
      vMarketRateOdd.toString() != "" &&
      vSelectionRunnerID != "0"
    ) {
      var items;
      try {
        items = vRunnerInfo;
      } catch (err) { }
      items.map(function (vobj) {
        if (vobj.runner_id != vSelectionRunnerID) {
          for (var i = 1; i <= showRateSize; i++) {
            var vSelectBackRunnertextbox = $(
              "#txtBRate" + vSelectionRunnerID + "_" + i.toString()
            );
            var vSelectLayRunnertextbox = $(
              "#txtLRate" + vSelectionRunnerID + "_" + i.toString()
            );
            if (isSetBack) {
              var vBackRate = vSelectBackRunnertextbox.val();
              $("#txtBRate" + vobj.runner_id + "_" + i.toString()).val(
                vBackRate
              );
            }
            if (isSetLay) {
              var vLayRate = vSelectLayRunnertextbox.val();
              $("#txtLRate" + vobj.runner_id + "_" + i.toString()).val(
                vLayRate
              );
            }
          }
        }
        for (var i = 1; i <= showRateSize; i++) {
          if (isSetBack) {
            $("#txtLRate" + vobj.runner_id + "_" + i.toString()).val(
              ""
            );
          }
          if (isSetLay) {
            $("#txtBRate" + vobj.runner_id + "_" + i.toString()).val(
              ""
            );
          }
        }
      });
    }
  } catch (ex) {
    console.log("Error on page in funcation setBackOddsAll ::" + ex.message);
  }
}

var minoddsRate = 0;
var maxoddsRate = 99;
function checkMaretOdds() {
  try {
    var txtMaketOdds = $("#txtMaketOdds").val();
    if (txtMaketOdds == "") {
      $(".txtRateClear").val("");
      vCounter = 1;
      return false;
    }
    var vMarketRateOdd = parseFloat(txtMaketOdds);
    if (vMarketRateOdd != undefined && vMarketRateOdd.toString() != "") {
      var vmarketOdds = 0;
      try {
        var vIntValue = parseFloat(vMarketRateOdd);
        if (vIntValue >= minoddsRate && vIntValue <= maxoddsRate) {
          vmarketOdds = vIntValue;
        } else {
          if (vIntValue < minoddsRate) {
            vmarketOdds = minoddsRate;
            $.growl.error({
              title: "Market odds",
              message:
                "The minimum odds are " +
                minoddsRate +
                ". Your odds have been updated accordingly."
            });
          } else if (vIntValue > maxoddsRate) {
            vmarketOdds = maxoddsRate;
            $.growl.error({
              title: "Market odds",
              message:
                "The maximum odds are " +
                maxoddsRate +
                ". Your odds have been updated accordingly."
            });
          }
        }
      } catch (ex) {
        vmarketOdds = 0;
        vCounter = 1;
      }
    } else {
      vmarketOdds = 0;
      vCounter = 1;
    }
    var txtPrefixOdds = $("#txtPrefixOdds").val();
    if (txtPrefixOdds != null && txtPrefixOdds != "") {
      var vFinaltMaketOdds =
        parseFloat(vmarketOdds) / 100 + parseInt(txtPrefixOdds);
      var vAdjustValue = findAdjustedValue(vFinaltMaketOdds);

      if (vFinaltMaketOdds != parseFloat(vAdjustValue.value)) {
        var vob = (
          parseFloat(vAdjustValue.value) - parseFloat(txtPrefixOdds)
        ).toFixed(2);
        vmarketOdds =
          (parseFloat(vAdjustValue.value) - parseFloat(txtPrefixOdds)).toFixed(
            2
          ) * 100;
      }
    }
    $("#txtMaketOdds").val(vmarketOdds);
    setSelectedRunnerOdds();
  } catch (ex) {
    console.log("Error on page in funcation checkMaretOdds ::" + ex.message);
  }
  return false;
}
var iMinRate = 1.01;
var iMaxRate = 2;
function setSelectedRunnerOdds() {
  try {
    var vMarketRateOdd = $("#txtMaketOdds").val();
    var vMainRatedifferent = $("#txtRatedifferent").val();
    var vSelectionRunnerID = $("#ddlOddsRunner").val();
    if (vMarketRateOdd != "" && vSelectionRunnerID != "0") {
      var vRatedifferent = parseFloat(
        (parseFloat(vMainRatedifferent) / 100).toFixed(2)
      );
      var vtxtBackOdds = $("#txtBRate" + vSelectionRunnerID + "_1");
      var vtxtLayOdds = $("#txtLRate" + vSelectionRunnerID + "_1");
      var txtPrefixOdds = $("#txtPrefixOdds").val();
      var txtPrefixOdds;
      if (txtPrefixOdds != null && txtPrefixOdds != "") {
        txtPrefixOdds = parseFloat(txtPrefixOdds);
      }

      var vbackOdds = parseFloat(vMarketRateOdd) / 100 + txtPrefixOdds;
      vtxtBackOdds.val(vbackOdds.toFixed(2));

      if (vbackOdds >= vbackOdds + (vRatedifferent + (vCounter - 1) / 100)) {
        vCounter = 1;
      }
      if (vbackOdds < iMaxRate) {
        if (
          vbackOdds + (vRatedifferent + vCounter / 100) > iMaxRate &&
          vCounter != 1
        ) {
          vCounter -= 1;
        }
        vtxtLayOdds.val(
          (vbackOdds + (vRatedifferent + (vCounter - 1) / 100)).toFixed(2)
        );
      } else {
        vtxtLayOdds.val("");
        vCounter = 1;
      }
      setSelectedRunnerotherOddsbox();
    } else {
      $(".txtRateClear").val("");
    }
  } catch (ex) {
    console.log(
      "Error on page in funcation setSelectedRunnerOdds ::" + ex.message
    );
  }
}

function setSelectedRunnerotherOddsbox() {
  try {
    var vSelectionRunnerID = $("#ddlOddsRunner").val();
    if (vSelectionRunnerID != "0") {
      var vtxtBackOdds = $("#txtBRate" + vSelectionRunnerID + "_1");
      var vtxtLayOdds = $("#txtLRate" + vSelectionRunnerID + "_1");
      var vBackRate = vtxtBackOdds.val();
      var vLayRate = vtxtLayOdds.val();

      if (vBackRate != "" || vLayRate != "") {
        var VBackDifferent = $("#txtBackRateDiff").val();
        var VLayDifferent = $("#txtLayRateDiff").val();
        var vBDifferent = parseFloat(
          (parseFloat(VBackDifferent) / 100).toFixed(2)
        );
        var vLDifferent = parseFloat(
          (parseFloat(VLayDifferent) / 100).toFixed(2)
        );
        for (var i = 1; i <= showRateSize - 1; i++) {
          var vBackRunnertextbox = $(
            "#txtBRate" + vSelectionRunnerID + "_" + (i + 1).toString()
          );
          var vLayRunnertextbox = $(
            "#txtLRate" + vSelectionRunnerID + "_" + (i + 1).toString()
          );
          vBackRunnertextbox.val("");
          if (iMinRate <= parseFloat(vBackRate) - vBDifferent * i) {
            vBackRunnertextbox.val(
              (parseFloat(vBackRate) - vBDifferent * i).toFixed(2)
            );
          }
          vLayRunnertextbox.val("");
          if (iMaxRate >= parseFloat(vLayRate) + vLDifferent * i) {
            vLayRunnertextbox.val(
              (parseFloat(vLayRate) + vLDifferent * i).toFixed(2)
            );
          }
        }
      }
    } else {
      $(".txtRateClear").val("");
    }
  } catch (ex) {
    console.log(
      "Error on page in funcation setOtherRunnerOdds ::" + ex.message
    );
  }
}

function setBookmarkerRates(isReEnterRate, changeStatus = true) {
  try {
    var vRunnerlist = vRunnerInfo;
    if (vRunnerlist != undefined && vRunnerlist != null) {
      var vmarketOdds = [];
      var items;
      try {
        items = JSON.parse(vRunnerlist);
      } catch (err) {
        items = vRunnerlist;
      }
      var x = 0;
      items.map(function (vobj) {
        x++;
        var vRunnerSelectionID = vobj.runner_id;
        for (var i = 1; i <= showRateSize; i++) {
          var vBackRunnertextbox = $("#txtBRate" + vRunnerSelectionID + "_" + i.toString());
          var vLayRunnertextbox = $(
            "#txtLRate" + vRunnerSelectionID + "_" + i.toString()
          );
          if (vBackRunnertextbox.val() != "") {
            var vBackRunnerVolumetextbox = $(
              "#txtBVolume" + vRunnerSelectionID + "_" + i.toString()
            );
            var vBackVolume = 0.0;
            if (vBackRunnerVolumetextbox.val() != "") {
              vBackVolume = parseInt(vBackRunnerVolumetextbox.val());
            }
            vmarketOdds.push({
              appBFBetRateID: x,
              appMatchID: parseInt(marketObj.matchID),
              appBetID: marketObj.centralMarketID,
              appMarketID: marketObj.centralMarketID,
              appMarketID_BF: marketObj.centralMarketID,
              appBetDetailID: marketObj.centralMarketID,
              appSelectionID_BF: "" + vRunnerSelectionID,
              appRate: parseFloat(vBackRunnertextbox.val()).toFixed(2),
              appIsBackBet: true,
              appIsBack: true,
              appBFVolume: vBackVolume,
              appCustomevolume: 0.0,
              appIsBestRate: true,
              appMatchCustomevolume: 0.0,
              appPriority: 0,
              appRunnerStatus: 0,
              appTotalMatched: 0.0,
              appPoint: 0,
              appLPT: null,
              appRupees: vBackVolume
            });
          }
          if (vLayRunnertextbox.val() != "") {
            var vLayRunnerVolumetextbox = $(
              "#txtLVolume" + vRunnerSelectionID + "_" + i.toString()
            );
            var vLayVolume = 0.0;
            if (vLayRunnerVolumetextbox.val() != "") {
              vLayVolume = parseInt(vLayRunnerVolumetextbox.val());
            }
            vmarketOdds.push({
              appBFBetRateID: x,
              appMatchID: parseInt(marketObj.matchID),
              appBetID: marketObj.centralMarketID,
              appMarketID_BF: marketObj.centralMarketID,
              appBetDetailID: marketObj.centralMarketID,
              appSelectionID_BF: "" + vRunnerSelectionID,
              appRate: parseFloat(vLayRunnertextbox.val()).toFixed(2),
              appIsBackBet: false,
              appIsBack: false,
              appBFVolume: vLayVolume,
              appCustomevolume: 0.0,
              appIsBestRate: true,
              appMatchCustomevolume: 0.0,
              appPriority: 0,
              appRunnerStatus: 0,
              appTotalMatched: 0.0,
              appPoint: 0,
              appLPT: null,
              appRupees: vLayVolume
            });
          }
        }
      });
      if (vmarketOdds != null) {
        if (isReEnterRate) {
          var isActive = $("#rbtnActive").prop("checked");
          if (!isActive) {
            $.growl.error({
              title: "Fancy Rate !",
              message: "Only work when Rate Active in market"
            });
            return false;
          }
        }
        var status = $('input[name=rbtnstatus]:checked').val();
        if (changeStatus) {
          if (status == undefined || status != 1) {
            $("#rbtnActive").prop('checked', true);
          } else {
            $('#rbtndeActive').prop('checked', true);
          }
        }
        var bookmakerJson = [{
          "appBetMarketID": null,
          "appMarketID": marketObj.centralMarketID,
          "appMarketStatus": $('input[name=rbtnstatus]:checked').val(),
          "appMatchEventID": null,
          "appRate": JSON.stringify(vmarketOdds),
          "Fancytype": "7",
          "appTotalMatched": null,
        }];
        SetMarketOddsinRunner(bookmakerJson, isReEnterRate);
      } else {
        $.growl.error({
          title: "Market Odds",
          message: "Please Enter market Odds."
        });
      }
    }
  } catch (ex) { }
}

var vOldMarketOdds;
function SetMarketOddsinRunner(vmarketodds, isReEnterRate) {
  try {
    if (
      vOldMarketOdds == null ||
      (vOldMarketOdds == undefined && vmarketodds != null)
    ) {
      SetMarketRunnerOdds(vmarketodds, isReEnterRate);
    } else {
      var vMarketOdds = parseInt($("#txtMaketOdds").val());
      var vMinRate =
        parseInt(vOldMarketOdds) - parseInt($("#txtRateRang").val());
      var vMaxRate =
        parseInt(vOldMarketOdds) + parseInt($("#txtRateRang").val());
      if (vMinRate <= vMarketOdds && vMaxRate >= vMarketOdds) {
        SetMarketRunnerOdds(vmarketodds, isReEnterRate);
      } else {
        swal({
          title: "Market Rate Rang overflow !",
          text: "Are you sure to insert Out of Rang Rate?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes"
        }).then(
          function (json_data) {
            $('input').blur();
            SetMarketRunnerOdds(vmarketodds, isReEnterRate);
          },
          function (dismiss) {
            if (dismiss === "cancel") {
              // you might also handle 'close' or 'timer' if you used those
              // ignore
            } else {
              throw dismiss;
            }
          }
        );
        $(".swal2-show").focus();
      }
    }
  } catch (ex) {
    console.log(ex);
  }
}
var vOldBallStartAfter;
var vOldMaxStakePerRate;
function SetMarketRunnerOdds(vMarketOddslist, isReEnterRate) {
  if (vMarketOddslist != null && vMarketOddslist != undefined) {
    var vMarketOddsRate = $("#txtMaketOdds").val();
    if (vMarketOddsRate != "") vOldMarketOdds = vMarketOddsRate;
    var vBallStartAfter = $("#txtBallStartAfter").val();
    var vMaxStakePerRate = $("#txtMaxStack").val();
    var isUpdateLocal = false;

    $.ajax({
      url: 'https://bfrates.com:8888/marketapi/' + marketObj.centralMarketID,
      type: 'POST',
      data: { data: JSON.stringify(vMarketOddslist), "messageType": "fancy" },
      success: response => {

        if ($('input[name=rbtnstatus]:checked').val() == "9") {
          setBallStarted();
        }
        if ($('input[name=rbtnstatus]:checked').val() == "1") {
          setActiveStatus(vMarketOddsRate);
        }
        iStatus = $('input[name=rbtnstatus]:checked').val();
        $.growl.notice({ title: "Success", message: response.msg });
      },
      error: err => {
        alert('Couldn\'t process the request.');
      }
    });
  } else {
    swal({
      title: "Bookmarker odds !",
      text: "Fill Data after Set Data !",
      type: "error",
      allowEscapeKey: true
    });
  }
  return false;
}


function changeMarketStatusTo(status) {
  let statusString = "";
  switch (status) {
    case 2: statusString = "Inactive";
      break;
    case 3: statusString = "Suspended";
      break;
    case 4: statusString = "Closed";
      break;
  }
  if (confirm("Are you sure to change the market status to " + statusString + "?")) {
    setMarketStatus(status);
    return true;
  }
  else {
    return false;
  }
}

function setMarketStatus(status) {
  let data = {};
  closedMarket = false;
  if (status == 4)
    closedMarket = true;
  if (status == 1)
    data.isActive = 1;
  data.status = status;

  // switch(status){
  //     case 1: $("#active-radio").prop("checked",true);
  //         break;
  //     case 9: $("#ball-started-radio").prop("checked",true);
  //         break;
  // }
  updateMarket(data);
}

function updateMarket(data) {
  var status = data.status;
  token = window.localStorage.getItem('bhav-session-token');
  if (!token) {
    window.location.href = '/login';
    return;
  }
  let requestData = data;
  $.ajax({
    url: '/market/update/' + marketObj.centralMarketID,
    type: 'POST',
    data: requestData,
    beforeSend: xhr => {
      xhr.setRequestHeader('Authorization', 'bearer ' + token);
    },
    success: requestData => {
      if (requestData.err_message) {
        form_error.prop('class', 'form-error');
        form_error.html(data.err_message);
        return;
      }

      //alert('Market updated succesfully.');
      if (closedMarket) {
        $("#marketResultDiv").show();
      }
      else {
        $("#marketResultDiv").hide();
      }
      if (requestData.result && status != 1 && status != 9) {
        alert("Market result saved successfully!");
        $("#marketResult").val("");
        var data = [{
          "appBetMarketID": null,
          "appMarketID": marketObj.centralMarketID,
          "appMarketStatus": $('input[name=rbtnstatus]:checked').val(),
          "appMatchEventID": null,
          "appRate": [],
          "Fancytype": "6",
          "appTotalMatched": null,
        }];

        // return false;
        $.ajax({
          // url: 'http://178.239.168.142:1339/marketapi/' + marketObj.marketCode,
          url: 'https://bfrates.com:8888/marketapi/' + marketObj.centralMarketID,
          type: 'POST',
          data: { data: JSON.stringify(data), "messageType": "fancy" },
          success: res => {
            console.log(res);
          },
          error: err => {
            alert('Couldn\'t process the request.');
          }
        });
      }
    },
    error: err => {
      console.log(err);

      if ('Forbidden' === err.responseText) {
        form_error.prop('class', 'form-error');
        form_error.html('Token expired. You need to login again.');
      }

      form_error.prop('class', 'form-error');
      form_error.html('Error while updating market status: ' + err.responseText);
    }
  });
}

var getMarketIDfromUrl = () => {
  var ar = window.location.href.split('/');
  return parseInt(ar[ar.length - 1], 10);
};

var getMarketDetails = () => {
  var id = getMarketIDfromUrl();

  return new Promise((resolve, reject) => {
    $.ajax({
      url: '/bookmaker/get_details/' + id,
      type: 'GET',
      beforeSend: xhr => {
        xhr.setRequestHeader('Authorization', 'bearer ' + token);
      },
      success: data => {
        if (data.err_message) {
          form_error.prop('class', 'form-error');
          form_error.html(data.err_message);
          reject();

          return;
        }

        marketObj = data.market;
        resolve();

      },
      error: err => {
        console.log(err);

        if ('Forbidden' === err.responseText) {
          form_error.prop('class', 'form-error');
          form_error.html('Token expired. You need to login again.');
          reject();

          return;
        }

        form_error.prop('class', 'form-error');
        form_error.html('Error while getting market data: ' + err.responseText);
        reject();
      }
    });
  });
};

var setRunnerList = (runnerList) => {
  try {
    let vBetID = 0;
    var vRunnerlist = runnerList;
    vRunnerInfo = runnerList;

    if (vRunnerlist != undefined && vRunnerlist != null) {
      $("#betDetailsView").tmpl(vRunnerlist).appendTo("#marketbody");
      $('#ddlOddsRunner').empty();
      if (vRunnerlist.length > 0) {
        var vSelectID = 0;
        for (var i = 0; i < vRunnerlist.length; i++) {
          $('#ddlOddsRunner').append($('<option></option>').val(vRunnerlist[i].runner_id).html(vRunnerlist[i].runner_name));
          if (i == 0) vSelectID = vRunnerlist[i].runner_id;
        }
        $('#ddlOddsRunner').val(vSelectID);

        if (vSelectID != 0) setActiveRunner(vSelectID);
      }
    }
    else {
      $('#ddlRunnerinfo' + vBetID).empty();
      $('#ddlRunnerinfo' + vBetID).append('<option  value="0">--Runner--</option>');
      $('#ddlRunnerinfo' + vBetID).val(0);
    }
    $(".ddlrunner" + vBetID).select2();
  }
  catch (ex) {
    console.log("Error:" + ex.message)
  }
};

var setBallStarted = function () {
  //$(".txtRateClear").val("");
  $("#rbtndeActive").prop("checked", true);
  //$("#txtMaketOdds").val('');
  $("#txtMaketOdds").select();
  $("#txtMaketOdds").focus();
}

var setActiveStatus = function (vCRate) {
  var vMinRate = parseInt(vCRate) - parseInt($("#txtRateRang").val());
  var vMaxRate = parseInt(vCRate) + parseInt($("#txtRateRang").val());
  $("#spanRang").html(vMinRate + " - " + vMaxRate);
  $("#rbtnActive").prop("checked", true);
  //calculateSessionPosition($("#<%= hdnBetId.ClientID %>").val());
  $("#txtMaketOdds").focus();
}


$(document).ready(function () {

  form_error = $('#form-error');

  token = window.localStorage.getItem('bhav-session-token');
  if (!token) {
    window.location.href = '/login';
    return;
  }

  activeRadio = $('#active-radio');
  ballStartedRadio = $('#ball-started-radio');

  getMarketDetails().then(() => {
    $('#rate-title').html('Manual Odds Rate for ' + marketObj.marketName);
    $('#txtRatedifferent').val(marketObj.rateDifference);
    $('#txtBallStartAfter').val(marketObj.autoBallStartDuration);
    $('#rbtn' + marketObj.betStatus).prop("checked", true);
    $('#txtMaxStack').val(marketObj.maxStack);
    $('#ChkIsApplyCommissin').prop('checked', marketObj.isCommissionOn);
    var rateInput = $('#rate-input');
    rateInput.prop('disabled', '');
    rateInput.val('');
    rateInput.focus();
    setRunnerList(marketObj.runnerList);
  });

  $('#rate-input').focus();

});