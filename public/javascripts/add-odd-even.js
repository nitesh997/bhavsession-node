var globalMatches = [], globalTournaments = [], globalCompetitions = [];

$('.datepicker').datetimepicker({
    format: 'DD/MM/YYYY hh:mm:ss A',
    sideBySide: true
});

$('.datepicker').keypress(function(e){
    e.preventDefault();
});
$(document).ready( function() {

    var form_error = $('#form-error');

    var token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }

    setCompetitionSelect()
    .then(() => {
        setTournamentSelect()
        .then(() => setMatchSelect())
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));

    $('#competition-select').on('change', e => {
        setCompetitionDetails($(e.target).val());
        setTournamentSelect()
        .then(() => setMatchSelect())
        .catch(err => console.log(err));
    });

    $('#tournament-select').on('change', e => {
        setTournamentDetails($(e.target).val());
        setMatchSelect();
    });

    $('#match-select').on('change', e => {
        setMatchDetails($(e.target).val());
    });

    $('form#market-form').on('submit', () => {
        form_error.html('');

        var dataList = [];
        dataList['matchID'] = {selector: '#match-select', msg: 'Invalid match'};
        dataList['matchName'] = {selector: '#match-name', msg: 'Invalid match details'};
        dataList['matchDate'] = {selector: '#match-date', msg: 'Invalid match details'};
        dataList['tournamentID'] = {selector: '#tournament-select', msg: 'Invalid tournament'};
        dataList['tournamentName'] = {selector: '#tournament-name', msg: 'Invalid tournament details'};
        dataList['competitionID'] = {selector: '#competition-select', msg: 'Invalid competition'};
        dataList['competitionName'] = {selector: '#competition-name', msg: 'Invalid competition details'};
        dataList['marketName'] = {selector: '#market-name', msg: 'Invalid market name'};
        dataList['betStatus'] = {selector: '#bet-status', msg: 'Invalid bet status'};
        dataList['maxStack'] = {selector: '#max-stack', msg: 'Invalid max stack'};
        
        var data = {
            status: 2,
            isFancyCode: $('#is-fancy-code').prop('checked') ? 1 : 0,
            isCommissionOn: $('#apply-commission').prop('checked') ? 1 : 0,
            rateDifference: $('#rate-difference').val().trim(),
            description: $('#description').val().trim(),
            autoBallStartDuration: $('#auto-ball-start').val().trim(),
            isActive: $('#is-active').prop('checked') ? 1 : 0
        }

        let afterBetAllowFalse = $('#appAfterBetAllowFalse').data('DateTimePicker').date();
        let afterSuspend = $('#appAfterSuspend').data('DateTimePicker').date();
        let afterClose = $('#appAfterClose').data('DateTimePicker').date();

        if(afterBetAllowFalse!=null && afterBetAllowFalse!=undefined)
            data['afterBetAllowFalse']=afterBetAllowFalse.toISOString();
        if(afterSuspend!=null && afterSuspend!=undefined)
            data['afterSuspend']=afterSuspend.toISOString();
        if(afterClose!=null && afterClose!=undefined)
            data['afterClose']=afterClose.toISOString();

        for (var i in dataList) {
            var value = $(dataList[i].selector).val();
            if (!value || (value && '' === value.trim())) {
                form_error.prop('class', 'form-error');
                form_error.html(dataList[i].msg);

                return false;
            }
            data[i] = value;
        }

        var saveBtn = $('button#save-btn');
        saveBtn.prop('disabled', true);

        $.ajax({
            url: '/odd-even/add',
            type: 'POST',
            data: data,
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                saveBtn.prop('disabled', false);

                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                form_error.prop('class', 'form-success');
                form_error.html('Market added');
                location.href="/odd-even";
            },
            error: err => {
                saveBtn.prop('disabled', false);

                if ('Forbidden' === err.responseText) {
                    form_error.prop('class', 'form-error');
                    form_error.html('Token expired. You need to login again.');

                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while inserting market data: ' + err.responseText);
            }
        });

        return false;
    });

});
var getCompetitions = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: api_url + 'common/get_sport_list',
            type: 'POST',
            success: data => {
                 //console.log(data);
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                resolve(data);
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
            }
        });
    });
};

var setCompetitionSelect = () => {
    return new Promise((resolve, reject) => {
        getCompetitions().then(competitions => {
            // console.log(competitions);
            var select = $('#competition-select');
            select.html('');

            globalCompetitions = [];
            for (var i in competitions) {
                // console.log(competitions[i]);
                select.append('<option value="'+ competitions[i].EventType.Id +'">'+ competitions[i].EventType.Name +'</option>');

                // console.log(competitions[i].EventType.Id);
                // console.log(competitions[i]);

                globalCompetitions[competitions[i].EventType.Id] = competitions[i];
            }
            // console.log(globalCompetitions);
            setCompetitionDetails(select.val());

            resolve();
        }).catch(err => reject(err));
    });
};

var setCompetitionDetails = currentCompetition => {
    if (currentCompetition) {
        $('#competition-name').val(globalCompetitions[currentCompetition.trim()].EventType.Name);
    } else {
        $('#competition-name').val('');
    }
};


var getTournaments = competitionId => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: api_url + 'common/get_tournament_list',
            type: 'POST',
            data: {"strEventTypeId": competitionId},
            success: data => {
                // console.log(data);
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                resolve(data);
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
            }
        });
    });
};

var setTournamentSelect = () => {
    return new Promise((resolve, reject) => {
        var competitionId = parseInt( $('#competition-select').val(), 10 );
        getTournaments(competitionId).then(tournaments => {
            var select = $('#tournament-select');
            select.html('');
            globalTournaments = [];
            for (var i in tournaments) {
                select.append('<option value="'+ tournaments[i].Competition.Id +'">'+ tournaments[i].Competition.Name +'</option>');
                globalTournaments[tournaments[i].Competition.Id] = tournaments[i];
            }
            setTournamentDetails(select.val());

            resolve();
        }).catch(err => reject(err));
    });
};

var setTournamentDetails = currentTournament => {
    if (currentTournament) {
        $('#tournament-name').val(globalTournaments[currentTournament.trim()].Competition.Name);
    } else {
        $('#tournament-name').val('');
    }
};


var getMatches = tournamentId => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: api_url + 'common/get_match_list',
            type: 'POST',
            data: {
                "strCompetitionId": tournamentId
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);

                    return;
                }

                resolve(data);
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
            }
        });
    });
};

var setMatchSelect = () => {
    return new Promise((resolve, reject) => {
        var tournamentId = parseInt( $('#tournament-select').val(), 10 );
        getMatches(tournamentId).then(matches => {
            var select = $('#match-select');
            select.html('');
            globalMatches = [];
            for (var i in matches) {
                var dateTimeStamp = parseInt(matches[i].Event.OpenDate.replace(/[^0-9]/g,''));
                // console.log(dateTimeStamp);
                var date = new Date(dateTimeStamp);
                var dateStr = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                // var dateStr = moment.unix(dateTimeStamp).format('YYYY-MM-DD HH:mm:ss');
                select.append('<option value="'+ matches[i].Event.Id +'">'+ matches[i].Event.Name +' ('+ dateStr +')</option>');
                matches[i].Event.DateTimeStamp = dateTimeStamp;
                globalMatches[matches[i].Event.Id] = matches[i];
            }
            setMatchDetails(select.val());

            resolve();
        }).catch(err => reject(err));
    });
};

var setMatchDetails = currentMatch => {
    if (currentMatch) {
        $('#match-name').val(globalMatches[currentMatch.trim()].Event.Name);
        var date = new Date(globalMatches[currentMatch.trim()].Event.DateTimeStamp);
        var dateStr = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        $('#match-date').val(dateStr);
    } else {
        $('#match-name').val('');
        $('#match-date').val('');
    }
};

var rateOldValue = "";
$(document).on('keyup', '#rate-difference', function(e) {
    var currentElementObj = this;
    var rateValue = $(currentElementObj).val();
    if(isNaN(rateValue) == true) {
        e.target.value = rateOldValue;
    }
    if(rateValue < 0 || rateValue > 99) {
        e.target.value = rateOldValue;
    }
    rateOldValue = e.target.value;
});
