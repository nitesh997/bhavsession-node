// Dependencies.
var express = require('express');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var utils = require('../lib/utils');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const axios = require('axios');

// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

/* Add market. */
router.post('/add', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let dataKeys = [
      'matchID', 'matchName', 'matchDate', 'tournamentID', 'tournamentName', 'competitionID', 'competitionName',
      'marketName', 'betStatus', 'maxStack', 'isFancyCode', 'isCommissionOn', 'rateDifference',
      'autoBallStartDuration', 'description', 'isActive'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ('undefined' === typeof value) {
        res.json({err_message: 'Invalid market data.'});
        return;
      }

      data[dataKeys[i]] = value;
    }
    data.createdBy = authData.userID;

    // Add market data.
    return common_model.addData(utils.TBL_CENTRAL_MARKET, data)
    .then((result) => {
      //Call api to import market to Website
      let params = {
        "marketID": data["matchID"],
        "marketName": data["marketName"],
        "eventID": data["matchID"],
        "eventName": data["matchName"],
        "competitionID": data["tournamentID"],
        "competitionName": data["tournamentName"],
        "eventTypeID": data["competitionID"],
        "eventTypeName": data["competitionName"],
        "betStatus": 0,
        "marketRate": 0,
        "isBetAllow": true,
        "isActive": true,
        "isFancy": true,
        "fancyType": 6,
        "centralizationID": result.insertId,
        "rateDifferent": data["rateDifference"],
        "maxStack": data["maxStack"],
        "isApplyCommision": true,
        "rules": data["description"],
        "ballStartSec": data["autoBallStartDuration"],
        "isInplay": true,
        "isCheckVolume": true,
        "afterClose": "2019-11-15T19:39:21.970Z",
        "afterSuspend": "2019-11-15T19:39:21.970Z",
        "afterBetAllowFalse": "2019-11-15T19:39:21.970Z",
        "siteCode": "101",
        "siteGroup": "",
        "runners": [
          {
            "selectionID_BF": "",
            "title": "Bet"
          }
        ]
      };
      axios.post('http://centralbhav.rowbet.com/System/AddNewFancy1',params).then(response => {
        console.log(response.data);
      });


      res.json({result});
    })
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while adding market data.'});
    });
  });

});

/* Add market rate json. */
router.post('/add_rate_json', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let dataKeys = [
      'marketCode', 'rateJson'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ('undefined' === typeof value) {
        res.json({err_message: 'Invalid market data.'});
        return;
      }

      data[dataKeys[i]] = value;
    }
    data.createdBy = authData.userID;

    // Add market data.
    return common_model.addData('tblmarketrate', data)
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while adding market data.'});
    });
  });

});


/* Update market. */
router.post('/update/:id', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let marketID = escape(req.params.id);
    if (typeof marketID === 'undefined') {
      res.json({err_message: 'Invalid market data.'});

      return;
    }
    marketID = parseInt(marketID.trim(), 10);

    let dataKeys = [
      'matchID', 'matchName', 'matchDate', 'tournamentID', 'tournamentName', 'competitionID', 'competitionName',
      'marketCode', 'marketName', 'betStatus', 'maxStack', 'isFancyCode', 'isCommissionOn', 'rateDifference',
      'autoBallStartDuration', 'description', 'status', 'isActive', 'isDelete'
    ];
    let data = {
      marketID: marketID
    };

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if (typeof value !== 'undefined') {
        data[dataKeys[i]] = value.trim();
      }
    }

    let length = 0;
    for (let i in data) {
      length ++;
      break;
    }

    if (!length) {
      res.json({err_message: 'Invalid market data.'});
      return;
    }

    common_model.updateData('tblmarket', data, {'marketID': data.marketID})
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while updating market data.'})
    });

  });

});

router.get('/get_list', verifyToken, function(req, res) {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      // console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    common_model.getRowWhere(utils.TBL_CENTRAL_MARKET,'*',{'marketType':1}).then(result => {
      res.json({market: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error occurred while getting market data.'})
    });

  });
});

router.get('/get_details/:id', verifyToken, (req, res) => {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(403);
      return;
    }

    var id = escape(req.params.id);
    if (!id) {
      res.send({err_message: 'Invalid market ID'});
      return;
    }

    id = parseInt(id.trim(), 10);
    if ('' === id) {
      res.send({err_message: 'Invalid market ID'});
      return;
    }

    common_model.getAllData(utils.TBL_CENTRAL_MARKET, '*', {marketID: id}).then(result => {
      res.json({market: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error while getting market data.'});
    });
  });
});

module.exports = router;
