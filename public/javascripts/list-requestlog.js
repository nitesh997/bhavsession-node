var form_error = false;
var token = false;

$(document).ready( function() {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }
    getAgentList();
    if ($('#requestlogTable').length > 0) {
        requestlogTable = $('#requestlogTable').DataTable({
            "ajax": {
                url: '/reports/requestlog',
                type: 'POST',
                beforeSend: xhr => {
                    xhr.setRequestHeader('Authorization', 'bearer ' + token);
                 },
                timeout: 10000,
            },
            "processing": true,
            "serverSide": true,
            "paging":   true,
            "ordering": false,
            "searching": true,
            "fnServerParams": function (aoData) {
                var agent = $('select[name="agent"]').val();
                var toDate = $('#toDate').val();
                var fromDate = $('#fromDate').val();
                aoData.agent = agent;
                aoData.toDate = toDate;
                aoData.fromDate = fromDate;
            },columns: [
                {
                    data: "agent_username"
                },
                {
                    data: "api_name"
                },
                {
                    data: "apiCount"
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // Total over this page
            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // Update footer
            $( api.column( 2 ).footer()).html('Total : '+pageTotal);
        }
        });

        $('#agent').on('change',function(e) {
            requestlogTable.draw();
        });
        $('#toDate,#fromDate').datepicker().on('changeDate', function(e) {
       // $('#toDate,#fromDate').on('blur',function(e) {
            requestlogTable.draw();
        });
    }


});

var getAgentList = () => {

    return new Promise((resolve, reject) => {
            $.ajax({
                url: '/reports/requestlog_agent_list',
                type: 'POST',
                beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);
                reject();
                return;
            }
            if (data) {
               response = data.data;
                let html = '';
                if(response.length > 0){
                    for(key in response){
                        html += '<option value="'+response[key].agent_username+'">'+response[key].agent_username+'</option>';
                    }
                }
                $('#agent').html(html);
                resolve();
            }
        },
        error: err => {
            if ('Forbidden' === err.responseText) {
                alert('Please login again.');
                window.location.href = '/login';
                reject();
                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while getting match data: ' + err.responseText);
            reject();
        }
        });
        });
};