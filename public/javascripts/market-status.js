var form_error = false;
var token = false;
var marketStatustable;
$(document).ready(function () {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }  
    setMarketList();
    getAgentList();
});

var setMarketList = () => {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/market-status/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();

                    return;
                }

                if (data.market) {
                    form_error.html('');
                    if ( $.fn.DataTable.isDataTable('#market-status-table') ) {
                        $('#market-status-table').DataTable().destroy();
                    }
                    
                    $('#tblRemittanceList tbody').empty();
                    marketStatustable = $('#market-status-table').DataTable({
                        data: data.market,
                        aaSorting:[],
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            { "data": "centralMarketID", "title": "Central MarketId"},
                            { "data": "marketName" , "title": "Market Name"},
                            { "data": "matchName" , "title": "Match Name"},
                            { "data": "matchDate" , "title": "Match Date",
                              "render" : function(data, type, full, meta){
                                  var date = new Date(data);
                                  return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                              }
                            },
                            { "title": "Result", 
                                'searchable': false,
                                'orderable': false,
                                'className': 'text-center',
                                "render": function (data, type, full, meta) {
                                    let html = '';
                                    html = "<button class='btn btn-success' onclick=\"showMarket('" + full.centralMarketID + "')\"><i class='fa fa-eye'></i></button>";
                                    html+= "<button class='btn btn-danger' onclick=\"hideMarket('" + full.centralMarketID + "')\"><i class='md md-block'></i></button>";
                                    html+= "<button class='btn btn-danger' onclick=\"closeMarket('"+meta.row+"','" + full.centralMarketID + "')\"><i class='md md-delete'></i></button>";
                                    return html
                                }
                            }
                        ]
                    });

                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    reject();
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
                reject();
            }
        });
    });
};
$(document).on("click","#submitResult",function(){
    var marketId = $("#hdnMarketId").val();
    var runnerId = $("#resultText").val();
    if(confirm("Are you sure you want to announce result?"))
    {
        setRunner(marketId, runnerId);
    }
    
});


var showMarket = (centralMarketID) =>
{
    return new Promise((resolve, reject) => {
            $.ajax({
                url: '/api/showMarket',
                type: 'POST',
                data: {'centralMarketID' : centralMarketID, 'requestFrom' : 'self', agentIDs : JSON.stringify($('#agent').val())},
                beforeSend: xhr => {
                    xhr.setRequestHeader('Authorization', 'bearer ' + token);
                },
                success: data => {
                    if (data.status == "error") {
                        toastr.error(data.message);
                        reject();
                        return;
                    }
                    if (data.status == "success") {
                        toastr.success(data.message);
                        resolve();
                    }
                },
                error: err => {
                    if ('Forbidden' === err.responseText) {
                        alert('Please login again.');
                        window.location.href = '/login';
                        reject();
                        return;
                    }
                    reject();
                }
        });
    });
}

var hideMarket = (centralMarketID) =>
{
    return new Promise((resolve, reject) => {
            $.ajax({
                url: '/api/hideMarket',
                type: 'POST',
                data: {'centralMarketID' : centralMarketID, 'requestFrom' : 'self', agentIDs : JSON.stringify($('#agent').val())},
                beforeSend: xhr => {
                    xhr.setRequestHeader('Authorization', 'bearer ' + token);
                },
                success: data => {
                    if (data.status == "error") {
                        toastr.error(data.message);
                        reject();
                        return;
                    }
                    if (data.status == "success") {
                        toastr.success(data.message);
                        resolve();
                    }
                },
                error: err => {
                    if ('Forbidden' === err.responseText) {
                        alert('Please login again.');
                        window.location.href = '/login';
                        reject();
                        return;
                    }
                    reject();
                }
                });
        });
}

var closeMarket = (row,centralMarketID) =>
{

    return new Promise((resolve, reject) => {
            $.ajax({
                url: '/api/closeMarket',
                type: 'POST',
                data: {'centralMarketID' : centralMarketID, 'requestFrom' : 'self', agentIDs : JSON.stringify($('#agent').val())},
                beforeSend: xhr => {
                    xhr.setRequestHeader('Authorization', 'bearer ' + token);
                },
                success: data => {
                    if (data.status == "error") {
                        toastr.error(data.message);
                        reject();
                        return;
                    }
                    if (data.status == "success") {
                        toastr.success(data.message);
                        marketStatustable.row(row).remove().draw(false);
                        resolve();
                    }
                },
                error: err => {
                    if ('Forbidden' === err.responseText) {
                        alert('Please login again.');
                        window.location.href = '/login';
                        reject();
                        return;
                    }
                    reject();
                }
                });
        });
}

var getAgentList = () =>
{
    return new Promise((resolve, reject) => {
            $.ajax({
                url: '/agent/get_list',
                type: 'GET',
                beforeSend: xhr => {
                    xhr.setRequestHeader('Authorization', 'bearer ' + token);
                },
                success: data => {
                    if (data.err_message) {
                        form_error.prop('class', 'form-error');
                        form_error.html(data.err_message);
                        reject();
                        return;
                    }
                    if (data) {
                        response = data.agents;
                        let html = '';
                        if(response.length > 0){
                            for(key in response){
                                html += '<option value="'+response[key].id+'">'+response[key].name+'</option>';
                            }
                        }
                        $('#agent').html(html);
                        $('#agent').select2({
                            multiple: true,
                            closeOnSelect: false
                        });
                        resolve();
                    }
                },
                error: err => {
                    if ('Forbidden' === err.responseText) {
                        alert('Please login again.');
                        window.location.href = '/login';
                        reject();
                        return;
                    }
                    reject();
                }
            });
    });
}