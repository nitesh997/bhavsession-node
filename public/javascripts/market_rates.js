
var form_error = false;
var token = false;
var betid = [];
var agentCode = "bhavsession";
var secretkey = "4ade4e7319425d06b18a7ccf560b6ebe6556g477";
var importmarketToken = "";
var token = window.localStorage.getItem('bhav-session-token');
var sportsTable = null;
var tournamentTable = null;
var matchesTable = null;
var marketTable = null;
var ratestabel =  null;
var selectedData = {
    selectedSportsType: {
        id: -1,
        name: ""
    },
    selectedTournament: {
        id: -1,
        name: ""
    },
    selectedMatch: {
        id: -1,
        name: ""
    },
    selectedRates: {
        id: -1,
        name: ""
    },
}

$(document).ready(function () {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }
    getAccessToken();
});

var getAccessToken = () => {

    var data = {
        agentcode: agentCode,
        secretkey: secretkey
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_access_token',
            type: 'POST',
            data: data,
            beforeSend: xhr => {
                xhr.setRequestHeader('x-forwarded-for', '185.193.36.221');
            },
            success: data => {
                if (data != null && data != undefined && data.token) {
                    importmarketToken = data.token;
                    getSports();
                }
                resolve();
            },
            error: err => {
                toastr.error('Error while getting token: ' + err.responseText);
                reject();
            }
        });
    });
}

var getSports = () => {
    return new Promise((resolve, reject) => {
        $('#agent,#market-type-container').addClass('d-none');
        $.ajax({
            url: '/api/get_sport_list',
            type: 'post',
            data: {
                'requestFrom': 'self'
            },
            success: data => {
                if (data != null && data != undefined) {
                    if (sportsTable != null)
                        sportsTable.destroy();
                    sportsTable = $('#sports-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "EventType.Name", "title": "Sports Name"},
                            {"data": "MarketCount", "title": "No Of Markets"},
                        ]
                    });
                    goToStep(1);
                    $('#sports-table tbody').on('click', 'tr', function () {
                        var rowdata = sportsTable.row(this).data();
                        selectedData.selectedSportsType.id = rowdata.EventType.Id;
                        selectedData.selectedSportsType.name = rowdata.EventType.Name;
                        getTournaments();
                    });
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting sports data');
                reject();
            }
        });
    });
};

var getTournaments = () => {
    $('#agent,#market-type-container').addClass('d-none');
    var data = {
        strEventTypeId: parseInt(selectedData.selectedSportsType.id),
        'requestFrom': 'self'
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_tournament_list',
            type: 'post',
            data: data,

            success: data => {
                //console.log(data);
                if (data != null && data != undefined) {
                    if (tournamentTable != null)
                        tournamentTable.destroy();
                    tournamentTable = $('#tournament-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "Competition.Name", "title": "Tournament Name"},
                            {"data": "MarketCount", "title": "No of Markets"},
                        ]
                    });
                    goToStep(2);
                    $('#tournament-table tbody').on('click', 'tr', function () {
                        var rowdata = tournamentTable.row(this).data();
                        selectedData.selectedTournament.id = rowdata.Competition.Id;
                        selectedData.selectedTournament.name = rowdata.Competition.Name;
                        getMatches();
                    });
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting tournaments data');
                reject();
            }
        });
    });
};

var getMatches = () => {
    $('#agent,#market-type-container').addClass('d-none');
    var data = {
        strEventTypeId: parseInt(selectedData.selectedSportsType.id),
        strCompetitionId: parseInt(selectedData.selectedTournament.id),
        'requestFrom': 'self'
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_match_list',
            type: 'post',
            data: data,
            success: data => {
                //console.log(data);
                if (data != null && data != undefined) {
                    if (matchesTable != null)
                        matchesTable.destroy();
                    matchesTable = $('#matches-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "Event.Name", "title": "Match Name"},
                            {
                                "data": "Event.OpenDate",
                                "title": "Match Date",
                                "render": function (data, type, full, meta) {
                                    var dateTimeStamp = parseInt(full.Event.OpenDate.replace(/[^0-9]/g, ''));
                                    var date = new Date(dateTimeStamp);
                                    var dateStr = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                                    return dateStr;
                                }
                            },
                            {"data": "MarketCount", "title": "No Of Markets"}
                        ]
                    });
                    goToStep(3);
                    $('#matches-table tbody').on('click', 'tr', function () {
                        var rowdata = matchesTable.row(this).data();
                        selectedData.selectedMatch.id = rowdata.Event.Id;
                        selectedData.selectedMatch.name = rowdata.Event.Name;
                        getMarkets();
                    });
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting match data');
                reject();
            }
        });
    });
};


var getMarkets = () => {
    var data = {
        strEventTypeId: parseInt(selectedData.selectedSportsType.id),
        strEventID: parseInt(selectedData.selectedMatch.id),
        'requestFrom': 'self'
    };
    $('#market-type-container').removeClass('d-none');
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/get_market_list',
            type: 'post',
            data: data,
            success: data => {
                //console.log(data);
                if (data != null && data != undefined) {
                    if (marketTable != null)
                        marketTable.destroy();
                    marketTable = $('#market-table').DataTable({
                        data: data,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            {"data": "MarketName", "title": "Market Name"},
                            {
                                "title": "Rates",
                                'searchable': false,
                                'orderable': false,
                                'className': 'text-center',
                                "render": function (data, type, full, meta) {
                                    //return "<button class='btn btn-warning' data-id='" + full.MarketId + "'><i class='md md-remove-red-eye'></i></button>";
                                    return "<button class='btn btn-warning' onclick=\"getCentralData('" + full.MarketId + "')\" ><i class='md md-remove-red-eye'></i></button>";
                                }
                            }
                        ]
                    });
                    goToStep(4);
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while getting markets data: ');
                reject();
            }
        });

    });

};

var importMarket = (market_id) => {
    var data = {
        sport_id: parseInt(selectedData.selectedSportsType.id),
        tournament_id: parseInt(selectedData.selectedTournament.id),
        event_id: parseInt(selectedData.selectedMatch.id),
        market_id: parseInt(market_id),
        access_token: importmarketToken
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/import_market',
            type: 'post',
            data: data,
            success: data => {
                toastr.success('Market Imported successfully.');
                if (data != null && data != undefined) {
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while Import Market');
                reject();
            }
        });
    });
}
var importMatchOdsMarket = (market_id) => {
    var data = {
        sport_id: parseInt(selectedData.selectedSportsType.id),
        tournament_id: parseInt(selectedData.selectedTournament.id),
        event_id: parseInt(selectedData.selectedMatch.id),
        market_id: market_id.toString(),
        access_token: importmarketToken,
        agentIDs: JSON.stringify($('#agent').val()),
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/import_matchodds_bs',
            type: 'post',
            data: data,
            success: data => {
                if (data.status == "error") {
                    toastr.error(data.message);
                } else if (data.status == "success") {
                    if(data.message == "")
                        toastr.success('Market Imported successfully.');
                    else
                        toastr.success(data.message);
                }

                if (data != null && data != undefined) {
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while Import Market');
                reject();
            }
        });
    });
}

var importFancyMarket = (market_id) => {
    var data = {
        sport_id: parseInt(selectedData.selectedSportsType.id),
        tournament_id: parseInt(selectedData.selectedTournament.id),
        event_id: parseInt(selectedData.selectedMatch.id),
        market_id: parseInt(market_id),
        sportName: selectedData.selectedSportsType.name,
        tournamentName: selectedData.selectedTournament.name,
        access_token: importmarketToken,
        agentIDs: JSON.stringify($('#agent').val()),
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/import_fancy_bs',
            type: 'post',
            data: data,
            success: data => {
                if (data.status == "error") {
                    toastr.error(data.message);
                } else if (data.status == "success") {
                    if(data.message == "")
                        toastr.success('Market Imported successfully.');
                    else
                        toastr.success(data.message);
                }

                if (data != null && data != undefined) {
                    resolve();
                    return;
                } else {
                    reject();
                    return;
                }
            },
            error: err => {
                toastr.error('Error while Import Market');
                reject();
            }
        });
    });
}


var setBreadcum = () => {
    var navdiv = $('#Navigation');
    navdiv.html('');
    var html = "";

    if (selectedData.selectedSportsType.id > -1) {
        html += "<a href='javascript:void(0)' onclick=\"goToStep(1)\"> All Sports </a>";
        if (selectedData.selectedTournament.id > -1) {
            html += " / <a href='javascript:void(0)' onclick=\"goToStep(2)\"> " + selectedData.selectedSportsType.name + " </a>";

            if (selectedData.selectedMatch.id > -1) {
                html += " / <a href='javascript:void(0)' onclick=\"goToStep(3)\"> " + selectedData.selectedTournament.name + " </a>";
                html += " / <span> " + selectedData.selectedMatch.name + " </span>";
                if (selectedData.selectedRates.id > -1) {
                    html += " / <a href='javascript:void(0)' onclick=\"goToStep(4)\"> " + selectedData.selectedRates.name + " </a>";
                    html += " / <span> " + selectedData.selectedRates.name + " </span>";
                } else {
                    html += " / <span> " + selectedData.selectedRates.name + " </span>";
                }
            } else {
                html += " / <span> " + selectedData.selectedTournament.name + " </span>";
            }
        } else {
            html += " / <span> " + selectedData.selectedSportsType.name + " </span>";
        }
    } else {
        html += "<span> All Sports </span>"
    }
    navdiv.html(html);
}

var getCentralData = (sourceMarketID) => {
    $.ajax({
        url: '/market/get_marketdetails/'+sourceMarketID,
        type: 'GET',
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            //console.log(data);
            var centralid = 0;
            var matchtitle = "";
            var matchDate = "";
            var marketName = "";
            var isBetAllow = "";
            var numberOfRunners = "";
            var isApplyCommision = ""
            var isFancy = ""
            var tpSourceMarketID = '';
            var agentcode = '';
            $.each(data.market, function (i) {
                $.each(data.market[i], function (key, val) {
                    if(key == "centralMarketID"){
                        centralid = val;
                    }
                    if(key == "matchName"){
                        matchtitle = val;
                    }
                    if(key == "matchDate"){
                        matchDate = val;
                    }
                    if(key == "marketName"){
                        marketName = val;
                    }
                    if(key == "isBetAllow"){
                        isBetAllow = val;
                    }
                    if(key == "numberOfRunners"){
                        numberOfRunners = val;
                    }
                    if(key == "isApplyCommision"){
                        isApplyCommision = val;
                    }
                    if(key == "isFancy"){
                        isFancy = val;
                    }
                    if(key == "tpSourceMarketID"){
                        tpSourceMarketID = val;
                    }
                    if(key == "agentcode"){
                        agentcode = val;
                    }
                    //console.log(key+": " + val);
                });
            });
            $('#model_title').text(matchtitle);
            $('#market-centralid').val(centralid);
            $('#market-matchDate').text(matchDate);
            $('#market-marketName').text(marketName);
            isBetAllow == null ? 0 : isBetAllow;
            isBetAllow == 1 ? $('#market-isBetAllow').prop('checked',true) : $('#market-isBetAllow').prop('checked',false)
            $('#market-numberOfRunners').text(numberOfRunners);
            isApplyCommision == null ? 0 : isApplyCommision
            isApplyCommision == 1 ? $('#market-isApplyCommision').prop('checked',true): $('#market-isApplyCommision').prop('checked',false);
            isFancy == null ? 0 : isFancy
            isFancy == 1 ? $('#market-isFancy').prop('checked',true): $('#market-isFancy').prop('checked',false);
            $('#market-tpSourceMarketID').val(tpSourceMarketID);
            $('#market-agentcode').text(agentcode);
            if(centralid == 0)
                return toastr.error('Error while getting centralID: ');
            getCentralRunner(centralid);
        },
        error: err => {
            toastr.error('Error while getting markets data: ');
        }
    });
}

var getCentralRunner = (centralID) => {
    $.ajax({
        url: '/market/get_runnersList/'+centralID,
        type: 'GET',
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            //console.log(data);
            if (data != null && data != undefined) {
                var html = ""
                $.each(data.runners, function (i) {
                    html +='<tr class="odd">'
                    $.each(data.runners[i], function (key, val) {
                        if(key == 'name'){ 
                            html +='<td valign="top" colspan="1" class="dataTables_empty">'+val+'</td>';
                        }
                        if(key == 'tpRunnerID'){ 
                            html +='<td valign="top" colspan="1" class="dataTables_empty"><div class="row" id="'+val+'">Market Suspended</div></td>';
                        }
                        //console.log(key +": "+ val);
                    });
                    html +='</tr>'
                });
                $('#rates-tbody').empty()
                // For testing
                var html1 = '<tr class="odd"><td valign="top" colspan="1" class="dataTables_empty">Naomi Osaka</td><td valign="top" colspan="1" class="dataTables_empty"><div class="row" id="7640637"></div></td></tr>'
                 html1 += '<tr class="odd"><td valign="top" colspan="1" class="dataTables_empty">Serena Williams</td><td valign="top" colspan="1" class="dataTables_empty"><div class="row" id="2538236"></div></td></tr>'
                $("#rates-tbody").append(html);
                $("#ratesModal").modal('show');
                //for testing u can simple add a static value
                //displaytd();init(201656)
                init(centralID)
            } else {
                return;
            }
 
        },
        error: err => {
            toastr.error('Error while getting markets data: ');
        }
    });
}

var goToStep = (step) => {
    switch (step) {
        case 1:
            $('#sports-table').parents('div.dataTables_wrapper').first().show();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            $('#rates-table').parents('div.dataTables_wrapper').first().hide();
            selectedData = {
                selectedSportsType: {
                    id: -1,
                    name: ""
                },
                selectedTournament: {
                    id: -1,
                    name: ""
                },
                selectedMatch: {
                    id: -1,
                    name: ""
                },
                selectedRates: {
                    id: -1,
                    name: ""
                },
            }
            break;
        case 2:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().show();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            $('#rates-table').parents('div.dataTables_wrapper').first().hide();
            selectedData.selectedTournament = {
                id: -1,
                name: ""
            }
            selectedData.selectedMatch = {
                id: -1,
                name: ""
            }
            selectedData.selectedRates = {
                id: -1,
                name: ""
            }
            break;
        case 3:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().show();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            $('#rates-table').parents('div.dataTables_wrapper').first().hide();
            selectedData.selectedMatch = {
                id: -1,
                name: ""
            }
            selectedData.selectedRates = {
                id: -1,
                name: ""
            }
            break;
        case 4:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().show();
            $('#rates-table').parents('div.dataTables_wrapper').first().hide();
            selectedData.selectedRates = {
                id: -1,
                name: ""
            }
            break;
        case 5:
            $('#sports-table').parents('div.dataTables_wrapper').first().hide();
            $('#tournament-table').parents('div.dataTables_wrapper').first().hide();
            $('#matches-table').parents('div.dataTables_wrapper').first().hide();
            $('#market-table').parents('div.dataTables_wrapper').first().hide();
            $('#rates-table').parents('div.dataTables_wrapper').first().show();
            break;
    }
    setBreadcum();
}
$('input[name="isFancy"]').change(function (e) {
    getMarkets();
});

var getAgentList = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/agent/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();
                    return;
                }
                if (data) {
                    response = data.agents;
                    let html = '';
                    if (response.length > 0) {
                        for (key in response) {
                            html += '<option value="' + response[key].id + '">' + response[key].name + '</option>';
                        }
                    }
                    $('#agent').html(html);
                    $('#agent').select2({
                        multiple: true,
                        closeOnSelect: false
                    });
                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    reject();
                    return;
                }
                reject();
            }
        });
    });
}

//Last init funtion to connect websocket
let ws;
var count = 0;
function init(centralMarketID) {
    //Declare a websocket
    ws = new WebSocket('wss://bfrates.com:8888?token=park_webclient_jt97eyJhbGciOiJIUzI1NiIsInR5cCI6Ik');
    //open method a websocket
    ws.onopen = function() {
      console.log('WebSocket Client Connected');
      console.log('{"action":"set","markets":"'+centralMarketID+'"}')
      ws.send('{"action":"set","markets":"'+centralMarketID+'"}');
    };

    //send Message method a websocket
    ws.onmessage = function(e) {
      //message Received Bacis
        console.log("Received-'"+count+"' : '"+ e.data + "'");       
        count++; 
      getsocketMsg(e.data);
    };

    //close method a websocket
    ws.onclose = function(ev) {
    console.log('Socket Closed');
       init(centralMarketID);
    };

    //If error
    ws.onerror = function(e) {
      console.log("Error: '" + e.message + "'");
    };
}

function getsocketMsg(data){
    var JsonObj = JSON.parse(data);
    console.log(JsonObj);
    //data json from socket
    JsonObj = JSON.parse(JsonObj.data);
    console.log(JsonObj);   
    //get datalike marketid and time and appRate
    var html = "";
    JsonObj = JSON.parse(JsonObj[0].appRate);
    var html_id = []
    console.log(JsonObj);
    if(JsonObj == null || JsonObj.length == 0){
        $("#ratesModal").modal('hide');
        toastr.error('Market is suspended');
        return false;
    }
    for(var k in JsonObj) {
        //console.log($("#textrate_"+k).attr("data-id"))
        if($("#textrate_"+k).attr("data-id") == JsonObj[k].appRate){
            html= ""
            html +='    <div class="col-md-2" style=" text-align: center;">'
            html +='        <div class="form-group agent-container">'
            html +='         <a href="javascript:void(0);">'
            JsonObj[k].appIsBack == true? html +='<p data-id="'+JsonObj[k].appRate+'" id="textrate_'+k+'" style="background-color: #a7d8fd; color: black;">'+JsonObj[k].appRate+'<br/><small>('+JsonObj[k].appRupees+'00)</small></p></a>' : html +='<p data-id="'+JsonObj[k].appRate+'" id="textrate_'+k+'" style="background-color: #f9c9d4;color: black;">'+JsonObj[k].appRate+'<br/><small>('+JsonObj[k].appRupees+'00)</small></p></a>'
            html +='        </div>'
            html +='    </div>'
            if( html_id[JsonObj[k].appSelectionID_BF]){
                html_id[JsonObj[k].appSelectionID_BF] += html
            }else{
                html_id[JsonObj[k].appSelectionID_BF] = html
            }
        }
        else{
        html= ""
            html +='    <div class="col-md-2" style=" text-align: center;">'
            html +='        <div class="form-group agent-container">'
            html +='         <a href="javascript:void(0);">'
            JsonObj[k].appIsBack == true? html +='<p data-id="'+JsonObj[k].appRate+'" id="textrate_'+k+'" style="background-color: #ffd740; color: black;">'+JsonObj[k].appRate+'<br/><small>('+JsonObj[k].appRupees+'00)</small></p></a>' : html +='<p data-id="'+JsonObj[k].appRate+'" id="textrate_'+k+'" style="background-color: #44dedb;color: black;">'+JsonObj[k].appRate+'<br/><small>('+JsonObj[k].appRupees+'00)</small></p></a>'
            html +='        </div>'
            html +='    </div>'
            if( html_id[JsonObj[k].appSelectionID_BF]){
                html_id[JsonObj[k].appSelectionID_BF] += html
            }else{
                html_id[JsonObj[k].appSelectionID_BF] = html
            }
            // $("#"+JsonObj[k].appSelectionID_BF).empty();
            // $("#"+JsonObj[k].appSelectionID_BF).html(html);
        }
    }

    for (var key of Object.keys(html_id)) { 
        $("#"+key).empty();
        $("#"+key).html(html_id[key]);
    } 
}

//Testinf puspose
var Jobj = '[{\r\n\t\t\"appBFBetRateID\": 1085977,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"8326752\",\r\n\t\t\"appLPT\": \"1.3\",\r\n\t\t\"appRate\": 1.3,\r\n\t\t\"appIsBackBet\": true,\r\n\t\t\"appBFVolume\": 4533,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 4533,\r\n\t\t\"appIsBack\": true\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085978,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"8326752\",\r\n\t\t\"appLPT\": \"1.3\",\r\n\t\t\"appRate\": 1.29,\r\n\t\t\"appIsBackBet\": true,\r\n\t\t\"appBFVolume\": 2069,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 2069,\r\n\t\t\"appIsBack\": true\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085979,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"8326752\",\r\n\t\t\"appLPT\": \"1.3\",\r\n\t\t\"appRate\": 1.28,\r\n\t\t\"appIsBackBet\": true,\r\n\t\t\"appBFVolume\": 2281,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 2281,\r\n\t\t\"appIsBack\": true\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085980,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"8326752\",\r\n\t\t\"appLPT\": \"1.3\",\r\n\t\t\"appRate\": 1.31,\r\n\t\t\"appIsBackBet\": false,\r\n\t\t\"appBFVolume\": 1716,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 1716,\r\n\t\t\"appIsBack\": false\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085981,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"8326752\",\r\n\t\t\"appLPT\": \"1.3\",\r\n\t\t\"appRate\": 1.32,\r\n\t\t\"appIsBackBet\": false,\r\n\t\t\"appBFVolume\": 2355,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 2355,\r\n\t\t\"appIsBack\": false\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085982,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"8326752\",\r\n\t\t\"appLPT\": \"1.3\",\r\n\t\t\"appRate\": 1.33,\r\n\t\t\"appIsBackBet\": false,\r\n\t\t\"appBFVolume\": 1707,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 1707,\r\n\t\t\"appIsBack\": false\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085983,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"9633137\",\r\n\t\t\"appLPT\": \"4.3\",\r\n\t\t\"appRate\": 4.2,\r\n\t\t\"appIsBackBet\": true,\r\n\t\t\"appBFVolume\": 535,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 535,\r\n\t\t\"appIsBack\": true\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085984,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"9633137\",\r\n\t\t\"appLPT\": \"4.3\",\r\n\t\t\"appRate\": 4.1,\r\n\t\t\"appIsBackBet\": true,\r\n\t\t\"appBFVolume\": 829,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 829,\r\n\t\t\"appIsBack\": true\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085985,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"9633137\",\r\n\t\t\"appLPT\": \"4.3\",\r\n\t\t\"appRate\": 4,\r\n\t\t\"appIsBackBet\": true,\r\n\t\t\"appBFVolume\": 495,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 495,\r\n\t\t\"appIsBack\": true\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085986,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"9633137\",\r\n\t\t\"appLPT\": \"4.3\",\r\n\t\t\"appRate\": 4.3,\r\n\t\t\"appIsBackBet\": false,\r\n\t\t\"appBFVolume\": 255,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 255,\r\n\t\t\"appIsBack\": false\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085987,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"9633137\",\r\n\t\t\"appLPT\": \"4.3\",\r\n\t\t\"appRate\": 4.4,\r\n\t\t\"appIsBackBet\": false,\r\n\t\t\"appBFVolume\": 1092,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 1092,\r\n\t\t\"appIsBack\": false\r\n\t},\r\n\t{\r\n\t\t\"appBFBetRateID\": 1085988,\r\n\t\t\"appMatchID\": 30292577,\r\n\t\t\"appBetID\": \"1.179452253\",\r\n\t\t\"appMarketID_BF\": \"1.179452253\",\r\n\t\t\"appBetDetailID\": 1,\r\n\t\t\"appSelectionID_BF\": \"9633137\",\r\n\t\t\"appLPT\": \"4.3\",\r\n\t\t\"appRate\": 4.5,\r\n\t\t\"appIsBackBet\": false,\r\n\t\t\"appBFVolume\": 634,\r\n\t\t\"appCustomevolume\": 0,\r\n\t\t\"appIsBestRate\": true,\r\n\t\t\"appMatchCustomevolume\": 0,\r\n\t\t\"appPriority\": 1,\r\n\t\t\"appRunnerStatus\": 0,\r\n\t\t\"appTotalMatched\": 621476,\r\n\t\t\"appPoint\": null,\r\n\t\t\"appRupees\": 634,\r\n\t\t\"appIsBack\": false\r\n\t}\r\n]';

function displaytd(){
    var JsonObj = JSON.parse(Jobj);
    console.log(JsonObj);
    var html_id =  {};
    for(var k in JsonObj) {
            console.log(k);
            console.log(JsonObj[k].appSelectionID_BF);
            var html= ""
            html +='    <div class="col-md-2" style=" text-align: center;">'
            html +='        <div class="form-group agent-container">'
            html +='         <a href="javascript:void(0);">'
            JsonObj[k].appIsBack == true? html +='<p style="background-color: #a7d8fd; color: black;">'+JsonObj[k].appRate+'<br/><small>('+JsonObj[k].appRupees+'00)</small></p></a>' : html +='<p style="background-color: #f9c9d4;color: black;">'+JsonObj[k].appRate+'<br/><small>('+JsonObj[k].appRupees+'00)</small></p></a>'
            html +='        </div>'
            html +='    </div>'
            if( html_id[JsonObj[k].appSelectionID_BF]){
                html_id[JsonObj[k].appSelectionID_BF] += html
            }else{
                html_id[JsonObj[k].appSelectionID_BF] = html
            }
            // $("#"+JsonObj[k].appSelectionID_BF).empty();
            // $("#"+JsonObj[k].appSelectionID_BF).html(html);

    }
    console.log(JSON.stringify(html_id));
    for (var key of Object.keys(html_id)) { 
        console.log(key);
        $("#"+key).empty();
        $("#"+key).html(html_id[key]);
    } 

}

$(".modalbtn").on('click',function(){

});