var form_error = false;
var token = false;

var activeRadio = false;
var ballStartedRadio = false;
var closedMarket = false;

var marketObj = null;
var vCount = 1;
function setformula(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

    if (iKeyCode == 38 && evt.ctrlKey) {
        if ($("#txtNoRate1").val() != "") {
            var vYesrate = $("#txtYesRate1").val();
            var vTotal = parseFloat(vYesrate) + 1;
            if (!isNaN(vTotal)) {
                $("#txtYesRate1").val(vTotal);
                var vNoRate2 = $("#txtNoRate2").val();
                var vYesRate2 = $("#txtYesRate2").val();
                if (vNoRate2 != "") { $("#txtNoRate2").val(vTotal); }
                if (vYesRate2 != "") { $("#txtYesRate2").val(vTotal); }
                vCount++;
            }
        }
        else {
            vCount = 0;
            $("#txtYesRate1").val('');
        }
    }
    if (iKeyCode == 40 && evt.ctrlKey) {
        if ($("#txtNoRate1").val() != "") {
            var vRateDifferent = $("#txtRatedifferent").val();
            var vNorate = $("#txtNoRate1").val();
            var vYesRate = $("#txtYesRate1").val();
            var vCheck = parseFloat(vRateDifferent) + parseFloat(vNorate);
            var vTotal = (parseFloat(vYesRate) - 1);
            if (vCheck <= vTotal) {
                if (!isNaN(vTotal)) {
                    $("#txtYesRate1").val(vTotal);
                    var vNoRate2 = $("#txtNoRate2").val();
                    var vYesRate2 = $("#txtYesRate2").val();
                    if (vNoRate2 != "") { $("#txtNoRate2").val(vTotal); }
                    if (vYesRate2 != "") { $("#txtYesRate2").val(vTotal); }
                    vCount--;
                }
            }
            else {
                vCount = 0;
            }
        }
        else {
            vCount = 0;
        }
    }

    if (iKeyCode == 38 && evt.ctrlKey != true) {
        //keycode for : up arrow
        if ($("#rate-input").val() != "") {
            var vrateFormula = $("#rate-input").val();
            var vTotal = parseFloat(vrateFormula) + 1;
            var vRatedifferent = $("#txtRatedifferent").val();
            if (!isNaN(vTotal)) {
                $("#rate-input").val(vTotal);
                $("#txtNoRate1").val(vTotal);
                var vRate = parseFloat(vTotal) + parseFloat(vRatedifferent);
                $("#txtYesRate1").val(vRate);
                vCount++;
            }
        }
        else {
            vCount = 0;
            $("#rate-input").val('');
        }
    }
    else if (iKeyCode == 40 && evt.ctrlKey != true) {
        //keycode for : down arrow
        if ($("#rate-input").val() != "") {
            if ($("#rate-input").val() > 0) {
                var vrateFormula = $("#rate-input").val();
                var vTotal = parseFloat(vrateFormula) - 1;
                var vRatedifferent = $("#txtRatedifferent").val();
                if (!isNaN(vTotal)) {
                    $("#rate-input").val(vTotal);
                    $("#txtNoRate1").val(vTotal);
                    var vRate = parseFloat(vTotal) + parseFloat(vRatedifferent);
                    $("#txtYesRate1").val(vRate);
                    vCount--;
                }
            }
            else {
                vCount = 0;
                $("#rate-input").val(0);
            }
        }
        else {
            vCount = 0;
            $("#rate-input").val('');
        }

    }
    else if (iKeyCode == 8) {
        //keycode for : backspace
        var vFormulaRate = $("#rate-input").val();
        var vRate = vFormulaRate.substr(0, vFormulaRate.length - 1);
        var vRemoveRate = $("#rate-input").val();
        var vMinusRate = vRemoveRate.split('-');
        var vPlusRate = vRemoveRate.split('+');
        var vFormulalength = vFormulaRate.length;

        if (vMinusRate[1] == "") {
            $(".txtMinusRate").val("");
            $(".txtMinusPoint").val("");
        }
        if (vPlusRate[1] == "") {
            $(".txtPlusRate").val("");
            $(".txtPlusPoint").val("");
        }
        if (vRate == "") {
            $(".txtbox").val("");
            $("#txtNoPoint1").val("100");
            $("#txtYesPoint1").val("100");
        }
        if (vFormulalength >= 1) {
            var vIsAlph = $.isNumeric(vRemoveRate);
            if (vIsAlph == false) {
                var vNorate1 = vRemoveRate.replace(/\D/g, '');
                $("#txtNoRate1").val(vNorate1);
                var vYesRate = parseFloat(vNorate1) + parseFloat($("#txtRatedifferent").val());
                if ($("#txtNoRate1").val() != $("#txtYesRate1").val()) {
                    $("#txtYesRate1").val(vYesRate);
                }
            }
            else {
                $("#txtNoRate1").val(vRemoveRate);
                var vYesRate = parseFloat(vRemoveRate) + parseFloat($("#txtRatedifferent").val());
                $("#txtYesRate1").val(vYesRate);
                $("#txtNoPoint1").val("100");
                $("#txtYesPoint1").val("100");
            }
            return false;
        }
    }
    else if (iKeyCode == 107) {
        //keycode for : +
        var vstrFormula = $("#rate-input").val();
        if (vstrFormula != "") {
            var vFormulaInPlus = $("#rate-input").val().split('+').length;
            if (vFormulaInPlus === 2) {
                return false;
            }
            else {
                var vPlusRate = $("#rate-input").val().split('+');
                if (vPlusRate[1] != "") {
                    var vRate = $("#rate-input").val();
                    $("#rate-input").val(vRate + '+');
                }
                else {
                    $("#rate-input").val(vstrFormula + '+');
                }
            }
        }
        else {
            return false;
        }
    }
    else if (iKeyCode == 109) {
        //keycode for : -
        var vstrFormula = $("#rate-input").val();
        if (vstrFormula != "") {
            var vFormulaInMinush = $("#rate-input").val().split('-').length;
            if (vFormulaInMinush === 2) {
                return false;
            }
            else {
                var vminusRate = $("#rate-input").val().split('-');
                if (vminusRate[1] != "") {
                    var vRate = $("#rate-input").val();
                    $("#rate-input").val(vRate + '-');
                }
                else {
                    $("#rate-input").val(vstrFormula + '-');
                }
            }
        }
        else {
            return false;
        }
    }
    else if (iKeyCode == 46) {
        //keycode for : Delete
        $(".txtbox").val("");
        $("#txtNoPoint1").val("100");
        $("#txtYesPoint1").val("100");
        $("#rate-input").val('');
    }
    else if (iKeyCode == 37 || iKeyCode == 39) {
        //keycode for : left and Right arrow
        return false;
    }
    else if (iKeyCode == 81) {
        //keycode for : q
        //setFormulaWiseValue( iKeyCode, 125, 105);
        setFormulaWiseValue(iKeyCode, $("#txtQNoRate").val(), $("#txtQYesRate").val());
        return false;
    }
    else if (iKeyCode == 87) {
        //keycode for : w
        setFormulaWiseValue(iKeyCode, $("#txtWNoRate").val(), $("#txtWYesRate").val());
        return false;
    }
    else if (iKeyCode == 69) {
        //keycode for : e
        setFormulaWiseValue(iKeyCode, $("#txtENoRate").val(), $("#txtEYesRate").val());
        return false;
    }
    else if (iKeyCode == 82) {
        //keycode for : r
        setFormulaWiseValue(iKeyCode, $("#txtRNoRate").val(), $("#txtRYesRate").val());
        return false;
    }
    else if (iKeyCode == 84) {
        //keycode for : t
        setFormulaWiseValue(iKeyCode, $("#txtTNoRate").val(), $("#txtTYesRate").val());
        return false;
    }
    else if (iKeyCode == 89) {
        //keycode for : y
        setFormulaWiseValue(iKeyCode, $("#txtYNoRate").val(), $("#txtYYesRate").val());
        return false;
    }
    else if (iKeyCode == 85) {
        //keycode for : u
        setFormulaWiseValue(iKeyCode, $("#txtUNoRate").val(), $("#txtUYesRate").val());
        return false;
    }
    else if (iKeyCode == 73) {
        //keycode for : i
        setFormulaWiseValue(iKeyCode, $("#txtINoRate").val(), $("#txtIYesRate").val());
        return false;
    }
    else if (iKeyCode == 79) {
        //keycode for : o
        setFormulaWiseValue(iKeyCode, $("#txtONoRate").val(), $("#txtOYesRate").val());
        return false;
    }
    else if (iKeyCode == 80) {
        //keycode for : p
        setFormulaWiseValue(iKeyCode, $("#txtPNoRate").val(), $("#txtPYesRate").val());
        return false;
    }
    else if (iKeyCode == 65) {
        //keycode for : a
        setFormulaWiseValue(iKeyCode, $("#txtANoRate").val(), $("#txtAYesRate").val());
        return false;
    }
    else if (iKeyCode == 83) {
        //keycode for : s
        setFormulaWiseValue(iKeyCode, $("#txtSNoRate").val(), $("#txtSYesRate").val());
        return false;
    }
    else if (iKeyCode == 68) {
        //keycode for : d
        setFormulaWiseValue(iKeyCode, $("#txtDNoRate").val(), $("#txtDYesRate").val());
        return false;
    }
    else if (iKeyCode == 70) {
        //keycode for : f
        setFormulaWiseValue(iKeyCode, $("#txtFNoRate").val(), $("#txtFYesRate").val());
        return false;
    }
    else if (iKeyCode == 71) {
        //keycode for : g
        setFormulaWiseValue(iKeyCode, $("#txtGNoRate").val(), $("#txtGYesRate").val());
        return false;
    }
    else if (iKeyCode == 75) {
        //keycode for : k
        setFormulaWiseValue(iKeyCode, $("#txtCustNopoint").val(), $("#txtCustYespoint").val());
        return false;
    }
    else if (iKeyCode == 90) {
        //keycode for : z
        setFormulaWiseValue(iKeyCode, $("#txtZNoRate").val(), $("#txtZYesRate").val());
        return false;
    }
    else if (iKeyCode == 88) {
        //keycode for : x
        setFormulaWiseValue(iKeyCode, $("#txtXNoRate").val(), $("#txtXYesRate").val());
        return false;
    }
    else if (iKeyCode == 67) {
        //keycode for : c
        setFormulaWiseValue(iKeyCode, $("#txtCNoRate").val(), $("#txtCYesRate").val());
        return false;
    }
    else if (iKeyCode == 86) {
        //keycode for : v
        setFormulaWiseValue(iKeyCode, $("#txtVNoRate").val(), $("#txtVYesRate").val());
        return false;
    }
    else if (iKeyCode == 66) {
        //keycode for : b
        setFormulaWiseValue(iKeyCode, $("#txtBNoRate").val(), $("#txtBYesRate").val());
        return false;
    }
    else if (iKeyCode == 78) {
        //keycode for : n
        setFormulaWiseValue(iKeyCode, $("#txtNNoRate").val(), $("#txtNYesRate").val());
        return false;
    }
    else if (iKeyCode == 77) {
        //keycode for : m
        setFormulaWiseValue(iKeyCode, $("#txtMNoRate").val(), $("#txtMYesRate").val());
        return false;
    }
    else if (iKeyCode == 72) {
        //keycode for : h
        var vNoRate = $("#txtNoRate1").val();
        if (vNoRate != "") {
            //var vIncrementValue = parseFloat(vNoRate) + parseFloat(2);
            var vIncrementValue = parseFloat(vNoRate) + parseFloat($("#txtHRate").val());
            $("#txtYesRate1").val(vIncrementValue);
            var vNoRate2 = $("#txtNoRate2").val();
            var vYesRate2 = $("#txtYesRate2").val();
            if (vNoRate2 != "") {
                $("#txtNoRate2").val(vIncrementValue);
            }
            if (vYesRate2 != "") {
                $("#txtYesRate2").val(vIncrementValue);
            }
        }
        else { return false; }
    }
    else if (iKeyCode == 74) {
        //keycode for : j
        var vNoRate = $("#txtNoRate1").val();
        if (vNoRate != "") {
            //var vIncrementValue = parseFloat(vNoRate) + parseFloat(3);
            var vIncrementValue = parseFloat(vNoRate) + parseFloat($("#txtJRate").val());

            $("#txtYesRate1").val(vIncrementValue);
            var vNoRate2 = $("#txtNoRate2").val();
            var vYesRate2 = $("#txtYesRate2").val();
            if (vNoRate2 != "") {
                $("#txtNoRate2").val(vIncrementValue);
            }
            if (vYesRate2 != "") {
                $("#txtYesRate2").val(vIncrementValue);
            }
        }
        else { return false; }
    }
    else if (iKeyCode == 76) {
        //keycode for : l
        setFormulaWiseValue(iKeyCode, $("#txtLNoRate").val(), $("#txtLYesRate").val());
        return false;
    }
    else {
        if ((iKeyCode >= 48 && iKeyCode <= 57) || (iKeyCode >= 96 && iKeyCode <= 105)) {
            var vtempCount = 0
            var vCurrentNo = $("#rate-input").val();
            var vRatedifferent = $("#txtRatedifferent").val();
            $("#txtNoRate1").val($("#rate-input").val());
            var vFinalRate = parseFloat(vRatedifferent) + parseFloat(vCurrentNo) + parseFloat(vRatedifferent) * parseInt(vtempCount);
            if (!isNaN(vFinalRate)) {
                $("#txtYesRate1").val(vFinalRate)
            }
            if (vCurrentNo == "") {
                vtempCount = 0;
                $("#txtYesRate1").val('');
            }
            vcount = 1;
            return false;
        }
    }
}

function setFormulaWiseValue(KeyCode, vNopoint, vYesPoint) {
    var KeyCodeval = String.fromCharCode(KeyCode);
    var txtRateformula = $("#rate-input").val();
    var vFormulalength = txtRateformula.length;
    var vMinusValue = txtRateformula.split('-');
    var vPlusValue = txtRateformula.split('+');

    var isValideCode = $("#rate-input").val().split(KeyCodeval).length;
    if (isValideCode === 2) {
        return false;
    }

    var vNorate = $("#txtNoRate1").val();
    var vYesRate = $("#txtYesRate1").val();

    if (vNorate != "") {
        if (vMinusValue[1] == "") {
            $("#txtNoRate3").val(vNorate);
            $("#txtNoPoint3").val(vNopoint);
            $("#txtYesRate3").val(vNorate);
            $("#txtYesPoint3").val(vYesPoint);
            $("#rate-input").val(txtRateformula + KeyCodeval);
            return false;
        }
    }

    if (vYesRate != "") {
        if (vPlusValue[1] == "") {
            $("#txtNoRate2").val(vYesRate);
            $("#txtNoPoint2").val(vNopoint);
            $("#txtYesRate2").val(vYesRate);
            $("#txtYesPoint2").val(vYesPoint);
            $("#rate-input").val(txtRateformula + KeyCodeval);
            return false;
        }
    }
    var vIsMinus = vMinusValue[1] == undefined || vMinusValue[1] == "" ? true : false;
    var vIsPlus = vPlusValue[1] == undefined || vMinusValue[1] == "" ? true : false;
    if (vFormulalength <= 5 && vIsMinus && vIsPlus) {
        $("#txtNoRate1").val(vNorate);
        $("#txtNoPoint1").val(vNopoint);
        $("#txtYesRate1").val(vNorate);
        $("#txtYesPoint1").val(vYesPoint);
        $("#rate-input").val(txtRateformula + KeyCodeval);
        return false;
    }

}

function changeMarketStatusTo(status) {
    let statusString = "";
    switch (status) {
        case 2: statusString = "Inactive";
            break;
        case 3: statusString = "Suspended";
            break;
        case 4: statusString = "Closed";
            break;
    }
    if (confirm("Are you sure to change the market status to " + statusString + "?")) {
        setMarketStatus(status);
        return true;
    }
    else {
        return false;
    }
}

function setMarketStatus(status) {
    let data = {};
    closedMarket = false;
    if (status == 4)
        closedMarket = true;
    if (status == 1)
        data.isActive = 1;
    data.status = status;

    // switch(status){
    //     case 1: $("#active-radio").prop("checked",true);
    //         break;
    //     case 9: $("#ball-started-radio").prop("checked",true);
    //         break;
    // }
    updateMarket(data);
}

function updateMarket(data) {
    var status = data.status;
    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }
    let requestData = data;
    $.ajax({
        url: '/market/update/' + marketObj.centralMarketID,
        type: 'POST',
        data: requestData,
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: requestData => {
            if (requestData.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);
                return;
            }

            //alert('Market updated succesfully.');
            if (closedMarket) {
                $("#marketResultDiv").show();
            }
            else {
                $("#marketResultDiv").hide();
            }
            if (requestData.result && status != 1 && status != 9) {
                alert("Market result saved successfully!");
                $("#marketResult").val("");
                var data = [{
                    "appBetMarketID": null,
                    "appMarketID": marketObj.centralMarketID,
                    "appMarketStatus": $('input[name=rbtnstatus]:checked').val(),
                    "appMatchEventID": null,
                    "appRate": [],
                    "Fancytype": "6",
                    "appTotalMatched": null,
                }];

                // return false;
                $.ajax({
                    // url: 'http://178.239.168.142:1339/marketapi/' + marketObj.marketCode,
                    url: 'https://bfrates.com:8888/marketapi/' + marketObj.centralMarketID,
                    type: 'POST',
                    data: { data: JSON.stringify(data), "messageType": "fancy" },
                    success: res => {
                        console.log(res);
                    },
                    error: err => {
                        alert('Couldn\'t process the request.');
                    }
                });
            }
        },
        error: err => {
            console.log(err);

            if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while updating market status: ' + err.responseText);
        }
    });
}

function toggleBetAllow() {
    if (confirm("Are you sure to change the bet allow status?")) {
        form_error = $('#form-error');
        let data = {};
        data.isBetAllow = $("#ChkIsBetAllow").prop('checked') == true ? 1 : 0;
        
        token = window.localStorage.getItem('bhav-session-token');
        if (!token) {
            window.location.href = '/login';
            return;
        }
        $.ajax({
            url: '/market/update/' + marketObj.centralMarketID,
            type: 'POST',
            data: data,
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    return;
                }

                alert('Status updated succesfully.');
                return;

            },
            error: err => {
                console.log(err);

                if ('Forbidden' === err.responseText) {
                    form_error.prop('class', 'form-error');
                    form_error.html('Token expired. You need to login again.');
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while updating market status: ' + err.responseText);

                return;
            }
        });
    }
}

function toggleApplyCommission() {
    if (confirm("Are you sure to change the apply commission flag?")) {
        form_error = $('#form-error');
        let data = {};
        data.isCommissionOn = $("#ChkIsApplyCommission").prop('checked') == true ? 1 : 0;
        token = window.localStorage.getItem('bhav-session-token');
        if (!token) {
            window.location.href = '/login';
            return;
        }
        $.ajax({
            url: '/market/update/' + marketObj.centralMarketID,
            type: 'POST',
            data: data,
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    return;
                }

                alert('Status updated succesfully.');
                return;

            },
            error: err => {
                console.log(err);

                if ('Forbidden' === err.responseText) {
                    form_error.prop('class', 'form-error');
                    form_error.html('Token expired. You need to login again.');
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while updating market status: ' + err.responseText);

                return;
            }
        });
    }
}
$(document).ready(function () {

    form_error = $('#form-error');

    $(".allow-number").keypress(function (e) {
        if (e.keyCode < 48 || e.keyCode > 57) {
            e.preventDefault();
        }
    });

    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    $('.readonly-radio').attr('disabled', true);

    $("#saveMarketResult").click(function (e) {
        e.preventDefault();
        let result = $("#marketResult").val();
        let marketStatus = $("#rbtnClose").prop('checked');
        let data = {};
        if (result != "" && result != undefined) {
            if (marketStatus) {
                data.result = result;
                updateMarket(data);
            }
            else {
                alert('Market is not closed yet. Kindly close the market first and then you can set the results.');
            }
        }
        else {
            alert('Invalid value for the market result field!');
        }
    });

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }

    activeRadio = $('#active-radio');
    ballStartedRadio = $('#ball-started-radio');

    getMarketDetails().then(() => {
        $('#rate-title').html('Fancy Rate for ' + marketObj.marketName);
        $('#txtRatedifferent').val(marketObj.rateDifference);
        switch (marketObj.status) {
            case 1: $("#active-radio").prop('checked', true);
                break;
            case 2: $("#rbtnInactive").prop('checked', true);
                break;
            case 3: $("#rbtnSuspended").prop('checked', true);
                break;
            case 4: $("#rbtnClose").prop('checked', true);
                closedMarket = true;
                $("#marketResultDiv").show();
                break;
            case 9: $("#active-radio").prop('checked', true);
                break;
        }
        $('input:radio[name=rbtnstatus]').each(function () {
            if ($(this).val() == marketObj.betStatus) {
                $(this).prop('checked', true);
            }
        });
        if (marketObj.isBetAllow == 1)
            $('#ChkIsBetAllow').prop('checked', true);
        if (marketObj.isCommissionOn == 1)
            $('#ChkIsApplyCommission').prop('checked', true);


        var rateInput = $('#rate-input');
        rateInput.prop('disabled', '');
        rateInput.val('');
        rateInput.focus();
    });

    $('#rate-input').focus();

});

var getMarketIDfromUrl = () => {
    var ar = window.location.href.split('/');
    return parseInt(ar[ar.length - 1], 10);
};

var getMarketDetails = () => {
    var id = getMarketIDfromUrl();

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/market/get_details/' + id,
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();

                    return;
                }

                marketObj = data.market[0];
                resolve();

            },
            error: err => {
                console.log(err);

                if ('Forbidden' === err.responseText) {
                    form_error.prop('class', 'form-error');
                    form_error.html('Token expired. You need to login again.');
                    reject();

                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting market data: ' + err.responseText);
                reject();
            }
        });
    });
};

$('#rate-input').on('keypress', evt => {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
        return false;
    }
    else {
        return true;
    }
});

$('#rate-input').on('keyup', e => {

    // When enter is pressed.
    if (13 === e.keyCode) {
        if ('' === $(e.target).val().trim()) {
            return;
        }

        if (!e.shiftKey) {
            if (activeRadio.prop('checked')) {
                ballStartedRadio.prop('checked', true);
                setMarketStatus(9);
            } else {
                activeRadio.prop('checked', true);
                setMarketStatus(1);
            }
        }
        else {
            if (!activeRadio.prop('checked') && !ballStartedRadio.prop('checked')) {
                activeRadio.prop('checked', true);
                setMarketStatus(1);
            }
        }

        var rate = [];

        var count = 0;
        $('.card-body #betBoxes .rateInput').each(function (index) {

            if ($(this).val() != '') {
                var priority = $(this).data('priority');
                var appMarketStatus = 0;
                var htmlID = $(this).attr('id');
                var appIsBack = $(this).data('id') == 'no' ? false : true;
                var ratePrice = $(this).val();
                var point = $(this).parents('.box-row').find('.pointInput').val();
                var rateJson = {
                    "appBFBetRateID": count,
                    "appMatchID": marketObj.centralMarketID,
                    "appBetID": marketObj.centralMarketID,
                    "appMarketID_BF": marketObj.centralMarketID,
                    "appBetDetailID": marketObj.centralMarketID,
                    "appSelectionID_BF": marketObj.centralMarketID,
                    "appLPT": null,
                    "appRate": ratePrice,
                    "appIsBackBet": appIsBack,
                    "appBFVolume": 0,
                    "appCustomevolume": 0,
                    "appIsBestRate": true,
                    "appMatchCustomevolume": 0,
                    "appPriority": priority,
                    "appRunnerStatus": 0,
                    "appTotalMatched": 0,
                    "appPoint": parseInt(point),
                    "appRupees": 0,
                    "appIsBack": appIsBack
                };
                rate.push(rateJson);
            }
        });

        var rateJson = JSON.stringify(rate);
        var data = [{
            "appBetMarketID": null,
            "appMarketID": marketObj.centralMarketID,
            "appMarketStatus": $('input[name=rbtnstatus]:checked').val(),
            "appMatchEventID": null,
            "appRate": rateJson,
            "Fancytype": "6",
            "appTotalMatched": null,
        }];

        // return false;
        $.ajax({
            // url: 'http://178.239.168.142:1339/marketapi/' + marketObj.marketCode,
            url: 'https://bfrates.com:8888/marketapi/' + marketObj.centralMarketID,
            type: 'POST',
            data: { data: JSON.stringify(data), "messageType": "fancy" },
            success: response => {
                console.log(response);
                
                    if ($('input[name=rbtnstatus]:checked').val() == "9") {
                        setBallStarted()
                    }
                    if ($('input[name=rbtnstatus]:checked').val() == "1") {
                        setActiveStatus($('input#rate-input').val())
                    }
                    iStatus = $('input[name=rbtnstatus]:checked').val();
                    $.growl.notice({ title: "Success", message: response.msg });
            },
            error: err => {
                alert('Couldn\'t process the request.');
            }
        });

    } else {
        setformula(e);
    }
});

var setBallStarted = function () {
    $(".txtbox").val("");
    $("#txtNoPoint1").val("100");
    $("#txtYesPoint1").val("100");
    $("#rate-input").val($("#rate-input").val().match(/\d+/g));
    //$("#txtRateformula").val($("#txtRateformula").val().includes("+") == true ? $("#txtRateformula").val().split("+")[0] : $("#txtRateformula").val());
    $("#rbtndeActive").prop("checked", true);
    //$(".clsOpenClass").prop('disabled', false);
    //$(".txtbox").prop('disabled', false);
    setDefaultValue();
    $("#rate-input").select();
    $("#rate-input").focus();
}

var setDefaultValue = function () {
    if ($("#rate-input").val() != "") {
        if ($("#rate-input").val() > 0) {
            var vrateFormula = $("#rate-input").val();
            var vTotal = parseFloat(vrateFormula);
            var vRatedifferent = parseFloat($("#txtRatedifferent").val());
            if (!isNaN(vTotal)) {
                $("#txtNoRate1").val(vTotal);
                var vRate = parseFloat(vTotal) + vRatedifferent;
                $("#txtYesRate1").val(vRate);
            }
        }
    }
}

var setActiveStatus = function (vNoRate1) {

    var vMinRate = parseInt(vNoRate1) - parseInt($("#txtRateRang").val());
    var vMaxRate = parseInt(vNoRate1) + parseInt($("#txtRateRang").val());
    $("#spanRang").html(vMinRate + " - " + vMaxRate);
    $("#rbtnActive").prop("checked", true);
    //$(".clsOpenClass").prop('disabled', true);
    //$(".txtbox").prop('disabled', true);
    calculateSessionPosition(vMainBetID);
    $("#divMarektWiseProfitloss").scrollTop($("#divMarektWiseProfitloss").height() / 2)

    //$('#txtRateformula').blur();
    //$("#txtRateformula").val().includes("+") == true ? $("#txtRateformula").val().split("+")[0] : $("#txtRateformula").val();
    $('#rate-input').focus();
}


var rateOldValue = "";
$(document).on('keyup', '#txtRatedifferent', function (e) {
    var currentElementObj = this;
    var rateValue = $(currentElementObj).val();
    if (isNaN(rateValue) == true) {
        e.target.value = rateOldValue;
    }
    if (rateValue < 0 || rateValue > 99) {
        e.target.value = rateOldValue;
    }
    rateOldValue = e.target.value;
});
