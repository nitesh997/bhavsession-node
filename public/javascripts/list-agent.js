var form_error = false;
var token = false;
var selecteDeleteAgentId = 0;
var agentTable = null;
$(document).ready(function () {

    form_error = $('#form-error');

    token = window.localStorage.getItem('bhav-session-token');
    if (!token) {
        window.location.href = '/login';
        return;
    }  
    getAgentList();

});

var getAgentList = () => {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/agent/get_list',
            type: 'GET',
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', 'bearer ' + token);
            },
            success: data => {
                if (data.err_message) {
                    form_error.prop('class', 'form-error');
                    form_error.html(data.err_message);
                    reject();

                    return;
                }

                if (data.agents) {
                    form_error.html('');
                    if(agentTable != null)
                        agentTable.destroy();
                    agentTable = $('#agents-table').DataTable({                      
                        data: data.agents,
                        lengthMenu: [10,50, 100, 150, 200],
                        pageLength: 10,
                        columns: [
                            { "data": "name", "title": "Agent Name"},
                            { "data": "username" , "title": "User name"},
                            { "data": "secretkey" , "title": "Secret key"},
                            { "data": "agentcode" , "title": "Agent Code"},
                            { "title": "Action", 
                                'searchable': false,
                                'orderable': false,
                                'className': 'text-center',
                                "render": function (data, type, full, meta) {
                                    return "<button class='btn btn-success' onclick=\"edit('" + full.id + "')\" ><i class='md md-edit'></i></button> <button class='btn btn-danger' onclick=\"openDeletePopup('" + full.id + "')\" data-toggle='modal' data-target='#delete-modal'><i class='md md-delete'></i></button>";
                                }
                            }
                        ]
                    });

                    resolve();
                }
            },
            error: err => {
                if ('Forbidden' === err.responseText) {
                    alert('Please login again.');
                    window.location.href = '/login';
                    reject();
                    return;
                }

                form_error.prop('class', 'form-error');
                form_error.html('Error while getting match data: ' + err.responseText);
                reject();
            }
        });
    });
};

var edit = agentId => {
    location.href="/manage-agent/"+ agentId;
}
var openDeletePopup = agentId => {
    selecteDeleteAgentId  = agentId;
}
var deleteagent = () => {
    if(selecteDeleteAgentId < 1)
        return;
    var apiUrl = '/agent/delete/'+ selecteDeleteAgentId;
    $.ajax({
        url: apiUrl,
        type: 'POST',
        beforeSend: xhr => {
            xhr.setRequestHeader('Authorization', 'bearer ' + token);
        },
        success: data => {
            if (data.err_message) {
                form_error.prop('class', 'form-error');
                form_error.html(data.err_message);
                return;
            }
            form_error.html("");
            toastr.success('Agent deleted successfully.');
            $('#delete-modal').modal('hide');
            getAgentList();
        },
        error: err => {
            saveBtn.prop('disabled', false);

            if ('Forbidden' === err.responseText) {
                form_error.prop('class', 'form-error');
                form_error.html('Token expired. You need to login again.');

                return;
            }

            form_error.prop('class', 'form-error');
            form_error.html('Error while inserting agent data: ' + err.responseText);
        }
    });
}