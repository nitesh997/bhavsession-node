let tableVar = {
    "TBL_USER":"tbluser",
    "TBL_FANCY_AGENT":"fancy_agent",
    "TBL_AGENT":"tblagent",
    "TBL_CENTRAL_MARKET":"tblcentralmarket",
    "TBL_CLIENT":"tblclient",
    "TBL_FIXTURE":"tblfixture",
    "TBL_MARKET_CLIENT":"	tblmarketclient",
    "TBL_MARKET_RATE":"tblmarketrate",
    "TBL_RUNNER":"tblrunner",
    "TBL_SCORE_ADMIN":"tblscoreadmin",
    "TBL_SOURCE":"tblsource",
    "TBL_USER_AGENT":"user_agent",
	"TBL_REQUEST_LOG":" tblrequestlog"
};
module.exports = tableVar;