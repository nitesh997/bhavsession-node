const axios = require('axios');

var kc_auth = function(){
    this.IsValidToken = async function (url,access_token)
    {
        let result = undefined;
        await axios.post(url+"IsValidToken", {}, {
            headers: {"Authorization":"Bearer " + access_token}
        }).then(response => {
            console.log('Valid res-'+response)
            if(response.data.status != undefined)
            {
                result = {status:response.data.status};
            }else{
                result = {status:response.status};
            }
        })
        .catch(error => {
            if(error == undefined || error.response == undefined || error.response.status == 401)
            {
                result = {status:401};
            }
        });
        return result;
    },
    this.GetAccessToken = async function (url,secret_key)
    {
        let result = undefined;
        await axios.get(url+"GetToken", {params: {"secretkey":secret_key}}).then(response => {
            console.log()
            result = response.data;
        }).catch(error => {
            if(error == undefined || error.response == undefined || error.response.status == 401)
            {
                result = {status:401};
            }
        });
        return result;
    }
}
module.exports = kc_auth;