process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
var express = require("express");
var router = express.Router();
var axios = require("axios");
var testToken = require("../middlewares/test_token");
var jwt = require("jsonwebtoken");
var utils = require("../lib/utils");
var dateFormat = require("dateformat");
var auth = require("../lib/kc_auth");

const jwtSecret = process.env.JWT_SECRET;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
//var url = "mongodb://mongouser:Shashank123@127.0.0.1:27017/?authSource=admin&readPreference=primary&gssapiServiceName=mongodb&appname=MongoDB%20Compass&ssl=false";
var mongoObj = undefined;
MongoClient.connect(url, function (err, db) {
    if (err)
        console.log(err);
    mongoObj = db.db("centralserver");
});

var FormData = require('form-data');
const marketStatus = {
    "open": 1,
    "inactive": 2,
    "suspended": 3,
    "closed": 4,
    "settled": 5,
    "inplay": 6,
    "ballstart": 9,
    "resetsession": 10
};
// Models
var knex = require("../config");
var soap = require("soap");
var url = "http://185.44.76.182:786/ICentralizationService.svc?wsdl";

function storeErrorInDb(url, body, error) {
    let errorData = {
        "api": url,
        "request": body,
        "error": error,
        "createdDateTime": new Date()
    }
    mongoObj.collection("error_logs").insertOne(errorData, w = 0);
}

router.post("/add_new_fancy", testToken, async (req, res) => {
    res.json({status: true, centralID: 0, message: "Legacy Method"});
});

router.post("/add_rate", testToken, async (req, res) => {
    res.json({success: 1, message: "Legacy Method"});
});

router.post("/change_status", testToken, async (req, res) => {
    res.json({status: true, centralID: 0, message: "Legacy Method"});
});
router.post("/get_last_rate", testToken, async (req, res) => {
    var logData = {
        "url": req.url,
        "request": req.body,
        "createdDateTime": new Date()
    };
    var id = escape(req.body.marketCode);
    if (!id) {
        let resp = {
            status: false,
            err_message: "Invalid market ID"
        };
        res.json(resp);
        logData.response = resp;
        mongoObj.collection("last_rate_api_logs").insertOne(logData);
        return;
    }
    try {
        let result = await knex("tblmarketrate").where("marketCode", id).select('rateJson', knex.raw("(UNIX_TIMESTAMP() -  UNIX_TIMESTAMP(createdDateTime)) as diffSeconds"))
        var row = {};
        if (result.length > 0) {
            if (result[0].diffSeconds < 10) {
                row = result[0].rateJson;
                rateJson = JSON.parse(result[0].rateJson);
                rateJsonData = JSON.parse(rateJson.data);
                res.send(rateJsonData);
                logData.response = rateJsonData;
                mongoObj.collection("last_rate_api_logs").insertOne(logData);
                return;
            } else {
                rateJson = JSON.parse(result[0].rateJson);
                rateJsonData = JSON.parse(rateJson.data);
                row.data = rateJsonData;
                if (rateJson.messageType == "match_odds") {
                    appRate = [];
                    if (rateJsonData[0] != undefined && rateJsonData[0].appRate != undefined) {
                        row.data[0].appRate = appRate;
                    }
                    res.json(rateJsonData);
                    logData.response = rateJsonData;
                    mongoObj.collection("last_rate_api_logs").insertOne(logData);
                    return;
                } else {
                    res.send(rateJsonData);
                    logData.response = rateJsonData;
                    mongoObj.collection("last_rate_api_logs").insertOne(logData);
                    return;
                }
            }
        } else {
            logData.response = {};
            mongoObj.collection("last_rate_api_logs").insertOne(logData);
            res.json({});
        }
        return;
    } catch (e) {
        storeErrorInDb("get_last_rate", req.body, e);
        res.json({});
    }
});


router.post("/fancy_status", testToken, async (req, res) => {
    res.json({status: true});
});

router.post("/bet_status", testToken, async (req, res) => {
    res.json({status: true});
});

router.post("/import_fancy", testToken, async (req, res) => {
    res.json({success: 1, centralID: 0, message: "Legacy Method"});
});


router.post("/set_result", testToken, async (req, res) => {
    res.json({success: 1, message: "Legacy Method"});
});


router.post("/get_sport_list", testToken, async (req, res) => {
    let sources = await knex(utils.TBL_SOURCE).where({"sourceType": 2, isActive: 1}).select();
    let resp = [];
    try {
        if (sources[0].sourceID == 5) {
            let item1 = {};
            item1.EventType = {};
            item1.EventType.Id = 1;
            item1.EventType.Name = "Cricket";
            item1.MarketCount = "-";
            resp.push(item1);
            let item2 = {};
            item2.EventType = {};
            item2.EventType.Id = 2;
            item2.EventType.Name = "Tennis";
            item2.MarketCount = "-";
            resp.push(item2);
            let item3 = {};
            item3.EventType = {};
            item3.EventType.Id = 3;
            item3.EventType.Name = "Soccer";
            item3.MarketCount = "-";
            resp.push(item3);
            res.json(resp);
        } else if (sources[0].sourceID == 4) {
            let params = {
                "filter": {"marketBettingTypes": ["ODDS", "LINE"]}
            };
            let response = await axios.post('http://185.16.206.13:8080/api/listEventTypes', params, {headers: {"X-App": "test2"}});
            let body = response.data;
            if (body.result != undefined && body.result.length > 0) {
                let list = body.result;
                list.forEach(element => {
                    let item = {};
                    item.EventType = {};
                    item.EventType.Id = element.eventType.id;
                    item.EventType.Name = element.eventType.name;
                    item.MarketCount = element.marketCount;
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 3) {
            var args = {IsAustralian: "false"};
            soap.createClient(url, function (err, client) {
                console.log(err);
                client.GetlistEventTypes(args, function (err, result) {
                    res.send(result["GetlistEventTypesResult"]);
                });
            });
        } else if (sources[0].sourceID == 7) {
            let params = {"refID": "", "isaustralian": false};
            let response = await axios.post('http://185.44.76.182:788/Market/Site/listEventTypes', params,
                {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
            let body = response.data;
            if (body.appdata != undefined && body.appdata.length > 0) {
                let list = body.appdata;
                list.forEach(element => {
                    let item = {};
                    item.EventType = {};
                    item.EventType.Id = element.eventType.id;
                    item.EventType.Name = element.eventType.name;
                    item.MarketCount = element.marketCount;
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 9) {
            let item1 = {};
            item1.EventType = {};
            item1.EventType.Id = 4;
            item1.EventType.Name = "Cricket";
            item1.MarketCount = "-";
            resp.push(item1);
            let item2 = {};
            item2.EventType = {};
            item2.EventType.Id = 2;
            item2.EventType.Name = "Tennis";
            item2.MarketCount = "-";
            resp.push(item2);
            let item3 = {};
            item3.EventType = {};
            item3.EventType.Id = 1;
            item3.EventType.Name = "Soccer";
            item3.MarketCount = "-";
            resp.push(item3);
            res.json(resp);
        } else {
            res.json([]);
        }
    } catch (err) {
        storeErrorInDb("error_logs", "get_sport_list : " + err.message);
        res.json({success: 0, message: "Error"});
    }

});

router.post("/get_tournament_list", testToken, async (req, res) => {
    let sources = await knex(utils.TBL_SOURCE).where({"sourceType": 2, isActive: 1}).select();
    let resp = [];
    try {
        if (sources[0].sourceID == 5) {
            var sports = ['cricket', 'tennis', 'soccer'];
            let params = {
                "key": "YhNzyE7naPcAnD9OffbJTqAOEBC06dhaPSumuzma2rGdr7KsNdnIRBo9lAw5",
                "sport": sports[req.body.strEventTypeId - 1]
            };
            let response = await axios.get('https://www.bet247exch.com/game-list?key=' + params["key"] + '&sport=' + params["sport"]);
            let body = response.data;
            if (body.data.result != undefined && body.data.result.length > 0) {
                let list = body.data.result;
                list.forEach(element => {
                    let item = {};
                    item.Competition = {};
                    item.Competition.Id = element.series_id;
                    item.Competition.Name = element.series_name;
                    item.MarketCount = "-";
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 4) {
            let params = {
                "filter": {
                    "marketBettingTypes": ["ODDS", "LINE"],
                    "eventTypeIds": [
                        req.body.strEventTypeId
                    ]
                }
            };
            let response = await axios.post('http://185.16.206.13:8080/api/listCompetitions', params, {headers: {"X-App": "test2"}});
            let body = response.data;
            if (body.result != undefined && body.result.length > 0) {
                let list = body.result;
                list.forEach(element => {
                    let item = {};
                    item.Competition = {};
                    item.Competition.Id = element.competition.id;
                    item.Competition.Name = element.competition.name;
                    item.MarketCount = element.marketCount;
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 3) {
            var args = {strEventTypeId: req.body.strEventTypeId, IsAustralian: "false"};
            soap.createClient(url, function (err, client) {
                client.getlistCompititions(args, function (err, result) {
                    res.send(result["getlistCompititionsResult"]);
                });
            });
        } else if (sources[0].sourceID == 7) {
            let params = {"refID": req.body.strEventTypeId, "isaustralian": false};
            let response = await axios.post('http://185.44.76.182:788/Market/Site/EventTypes_listCompititions', params,
                {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
            let body = response.data;
            if (body.appdata != undefined && body.appdata.length > 0) {
                let list = body.appdata;
                list.forEach(element => {
                    let item = {};
                    item.Competition = {};
                    item.Competition.Id = element.competition.id;
                    item.Competition.Name = element.competition.name;
                    item.MarketCount = element.marketCount;
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 9) {
            var sports = [{
                id: 4,
                name: 'cricket'
            },
                {
                    id: 2,
                    name: 'tennis'
                },
                {
                    id: 1,
                    name: 'soccer'
                }];
            var sportIdx = sports.findIndex(x => x.id == req.body.strEventTypeId);
            var form = new FormData();
            form.append('Id', 0);
            form.append('type', 'series');
            form.append('sport', sports[sportIdx].name);
            axios.create({
                headers: form.getHeaders()
            }).post(sources[0].apiUrl + '/Api/listData', form).then(response => {
                let body = response.data;
                if (body != undefined) {
                    let list = body;
                    list.forEach(element => {
                        let item = {};
                        item.Competition = {};
                        item.Competition.Id = element.series_id;
                        item.Competition.Name = element.series_name;
                        item.MarketCount = "-";
                        resp.push(item);
                    });
                    res.json(resp);
                } else {
                    res.json([]);
                }
            }).catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
                console.log(error.message);
                res.json([]);
            });
        } else {
            res.json([]);
        }
    } catch (err) {
        storeErrorInDb("error_logs", "get_tournament_list : " + err.message);
        res.json({success: 0, message: "Error"});
    }

});

router.post("/get_match_list", testToken, async (req, res) => {
    let sources = await knex(utils.TBL_SOURCE).where({"sourceType": 2, isActive: 1}).select();
    let resp = [];
    try {
        if (sources[0].sourceID == 5) {
            var sports = ['cricket', 'tennis', 'soccer'];
            if (req.body.strEventTypeId == undefined || req.body.strEventTypeId == 0) {
                req.body.strEventTypeId = 1;
            }
            let params = {
                "key": "YhNzyE7naPcAnD9OffbJTqAOEBC06dhaPSumuzma2rGdr7KsNdnIRBo9lAw5",
                "series_id": req.body.strCompetitionId,
                "sport": sports[req.body.strEventTypeId - 1]
            };
            let response = await axios.get('https://www.bet247exch.com/game-list?key=' + params["key"] + '&series_id=' + params["series_id"] + '&sport=' + params["sport"]);

            let body = response.data;
            if (body.data.result != undefined && body.data.result.length > 0) {
                let list = body.data.result;
                list.forEach(element => {
                    let item = {};
                    item.Event = {};
                    item.Event.Id = element.match_id;
                    item.Event.Name = element.match_name;
                    item.Event.CountryCode = "";
                    item.Event.Timezone = "";
                    item.Event.Venue = "";
                    item.Event.OpenDate = "/Date(" + new Date(element.open_date) + ")/";
                    item.MarketCount = "-";
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 4) {
            let params = {};
            if (req.body.strCompetitionId != "" && req.body.strCompetitionId != null && req.body.strCompetitionId != undefined) {
                params = {
                    "filter": {
                        "marketBettingTypes": ["ODDS", "LINE"],
                        "competitionIds": [
                            req.body.strCompetitionId
                        ]
                    }
                };
            } else {
                params = {
                    "filter": {
                        "marketBettingTypes": ["ODDS", "LINE"],
                        "eventTypeIds": [
                            req.body.strEventTypeId
                        ]
                    }
                };
            }

            let response = await axios.post('http://185.16.206.13:8080/api/listEvents', params, {headers: {"X-App": "test2"}});
            let body = response.data;
            if (body.result != undefined && body.result.length > 0) {
                let list = body.result;
                list.forEach(element => {
                    let item = {};
                    item.Event = {};
                    item.Event.Id = element.event.id;
                    item.Event.Name = element.event.name;
                    item.Event.CountryCode = element.event.countryCode;
                    item.Event.Timezone = element.event.timezone;
                    item.Event.Venue = element.event.venue == undefined ? "" : element.event.venue;
                    item.Event.OpenDate = "/Date(" + new Date(element.event.openDate) / 1 + ")/";
                    item.MarketCount = element.marketCount;
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 3) {
            var args = {
                strCompetitionIds: req.body.strCompetitionId,
                IsAustralian: "false"
            };
            soap.createClient(url, function (err, client) {
                client.getlistEvents(args, function (err, result) {
                    res.send(result["getlistEventsResult"]);
                });
            });
        } else if (sources[0].sourceID == 7) {
            let tempUrl = "http://185.44.76.182:788/Market/Site/Compititions_listEvents";
            let params = {};
            if (req.body.strCompetitionId != "" && req.body.strCompetitionId != null && req.body.strCompetitionId != undefined) {
                params = {"refID": req.body.strCompetitionId, "isaustralian": false};
                tempUrl = "http://185.44.76.182:788/Market/Site/Compititions_listEvents";
            } else {
                params = {"refID": req.body.strEventTypeId, "isaustralian": false};
                tempUrl = "http://185.44.76.182:788/Market/Site/EventTypes_listEvents";
            }
            let response = await axios.post(tempUrl, params, {
                headers: {
                    "X-Application": "jay",
                    "X-Authentication": "jaynodeapi"
                }
            });
            let body = response.data;
            if (body.appdata != undefined && body.appdata.length > 0) {
                let list = body.appdata;
                list.forEach(element => {
                    let item = {};
                    item.Event = {};
                    item.Event.Id = element.event.id;
                    item.Event.Name = element.event.name;
                    item.Event.CountryCode = element.event.countryCode;
                    item.Event.Timezone = element.event.timezone;
                    item.Event.Venue = element.event.venue == undefined ? "" : element.event.venue;
                    item.Event.OpenDate = "/Date(" + new Date(element.event.openDate) / 1 + ")/";
                    item.MarketCount = element.marketCount;
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 9) {
            var sports = [{
                id: 4,
                name: 'cricket'
            },
                {
                    id: 2,
                    name: 'tennis'
                },
                {
                    id: 1,
                    name: 'soccer'
                }];
            if (req.body.strEventTypeId == undefined || req.body.strEventTypeId == 0) {
                req.body.strEventTypeId = 4;
            }
            var sportIdx = sports.findIndex(x => x.id == req.body.strEventTypeId);
            var form = new FormData();
            form.append('Id', req.body.strCompetitionId);
            form.append('type', 'match');
            form.append('sport', sports[sportIdx].name);
            axios.create({
                headers: form.getHeaders()
            }).post(sources[0].apiUrl + '/Api/listData', form).then(response => {
                let body = response.data;
                if (body != undefined) {
                    let list = body;
                    list.forEach(element => {
                        let item = {};
                        item.Event = {};
                        item.Event.Id = element.match_id;
                        item.Event.Name = element.match_name;
                        item.Event.CountryCode = element.countryCode;
                        item.Event.Timezone = element.timezone;
                        item.Event.Venue = "";
                        item.Event.OpenDate = "/Date(" + new Date(element.open_date) + ")/";
                        item.MarketCount = "-";
                        resp.push(item);
                    });
                    res.json(resp);
                } else {
                    res.json([]);
                }
            }).catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
                console.log(error.message);
                res.json([]);
            });
        } else {
            res.json([]);
        }
    } catch (err) {
        res.json({success: 0, message: "Error"});
    }
});

router.post("/get_market_list", testToken, async (req, res) => {
    let sources = await knex(utils.TBL_SOURCE).where({"sourceType": 2, isActive: 1}).select();
    let resp = [];
    try {
        if (sources[0].sourceID == 5) {
            var sports = ['cricket', 'tennis', 'soccer'];
            if (req.body.strEventTypeId == undefined || req.body.strEventTypeId == 0) {
                req.body.strEventTypeId = 1;
            }

            let params = {
                "key": "YhNzyE7naPcAnD9OffbJTqAOEBC06dhaPSumuzma2rGdr7KsNdnIRBo9lAw5",
                "match_id": req.body.strEventID,
                "sport": sports[req.body.strEventTypeId - 1]
            };
            console.log('url', 'https://www.bet247exch.com/game-list?key=' + params["key"] + '&match_id=' + params["match_id"] + '&sport=' + params["sport"]);
            let response = await axios.get('https://www.bet247exch.com/game-list?key=' + params["key"] + '&match_id=' + params["match_id"] + '&sport=' + params["sport"]);
            let body = response.data;
            if (body.data.result != undefined && body.data.result.length > 0) {
                let list = body.data.result;
                list.forEach(element => {
                    let item = {};
                    item.EventType = null;
                    item.MarketId = element.market_id;
                    item.MarketName = element.market_name;
                    item.Event = null;
                    item.Competition = null;
                    item.IsMarketDataDelayed = false;
                    item.Description = {};
                    item.Description.IsPersistenceEnabled = false;
                    item.Description.IsBspMarket = false;
                    item.Description.MarketTime = "/Date(" + new Date() / 1 + ")/";
                    item.Description.SuspendTime = "/Date(" + new Date() / 1 + ")/";
                    item.Description.SettleTime = null;
                    item.Description.BettingType = 0;
                    item.Description.IsTurnInPlayEnabled = true;
                    item.Description.MarketType = "";
                    item.Description.Regulator = "";
                    item.Description.MarketBaseRate = 0;
                    item.Description.IsDiscountAllowed = false;
                    item.Description.Wallet = "";
                    item.Description.Rules = "";
                    item.Description.RulesHasDate = false;
                    item.Description.Clarifications = null;
                    item.Runners = [];
                    let teams = JSON.parse(element.teams);
                    teams.runners.forEach(runner => {
                        let tempRunner = {};
                        tempRunner.SelectionId = runner.selectionId;
                        tempRunner.RunnerName = runner.runnerName;
                        tempRunner.Handicap = runner.handicap;
                        tempRunner.Metadata = null;
                        item.Runners.push(tempRunner);
                    });
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 4) {
            let params = {
                "filter": {
                    "marketBettingTypes": ["ODDS", "LINE"],
                    "eventIds": [
                        req.body.strEventID
                    ]
                },
                "maxResults": 500,
                "marketProjection": ["MARKET_DESCRIPTION", "RUNNER_DESCRIPTION", "RUNNER_METADATA", "MARKET_START_TIME"]
            };
            let response = await axios.post('http://185.16.206.13:8080/api/listMarketCatalogue', params, {headers: {"X-App": "test2"}});
            let body = response.data;

            if (body.result != undefined && body.result.length > 0) {
                let list = body.result;
                list.forEach(element => {
                    let item = {};
                    item.EventType = null;
                    item.MarketId = element.marketId;
                    item.MarketName = element.marketName;
                    item.Event = null;
                    item.Competition = null;
                    item.IsMarketDataDelayed = false;
                    item.Description = {};
                    item.Description.IsPersistenceEnabled = element.description.persistenceEnabled;
                    item.Description.IsBspMarket = element.description.bspMarket;
                    item.Description.MarketTime = "/Date(" + new Date(element.description.marketTime) / 1 + ")/";
                    item.Description.SuspendTime = "/Date(" + new Date(element.description.suspendTime) / 1 + ")/";
                    item.Description.SettleTime = null;
                    item.Description.BettingType = 0;
                    item.Description.IsTurnInPlayEnabled = element.description.turnInPlayEnabled;
                    item.Description.MarketType = element.description.marketType;
                    item.Description.Regulator = element.description.regulator;
                    item.Description.MarketBaseRate = element.description.marketBaseRate;
                    item.Description.IsDiscountAllowed = element.description.discountAllowed;
                    item.Description.Wallet = element.description.wallet;
                    item.Description.Rules = "";
                    item.Description.RulesHasDate = element.description.rulesHasDate;
                    item.Description.Clarifications = null;
                    item.Runners = [];
                    element.runners.forEach(runner => {
                        let tempRunner = {};
                        tempRunner.SelectionId = runner.selectionId;
                        tempRunner.RunnerName = runner.runnerName;
                        tempRunner.Handicap = runner.handicap;
                        tempRunner.Metadata = null;
                        item.Runners.push(tempRunner);
                    });
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 3) {
            var args = {strEventIds: req.body.strEventID, IsAustralian: "false"};
            soap.createClient(url, function (err, client) {
                client.getlistMarketCatalogue(args, function (err, result) {
                    res.send(result["getlistMarketCatalogueResult"]);
                });
            });
        } else if (sources[0].sourceID == 7) {
            let params = {"refID": req.body.strEventID, "isaustralian": false};
            let response = await axios.post('http://185.44.76.182:788/Market/Site/Events_listMarketandRunnerInfo', params,
                {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
            let body = response.data;
            if (body.appdata != undefined && body.appdata.length > 0) {
                let list = body.appdata;
                let keys = Object.keys(list);
                keys.forEach(function (key) {
                    let item = {};
                    let element = list[key];
                    item.EventType = null;
                    item.MarketId = element.marketId;
                    item.MarketName = element.marketName;
                    item.Event = null;
                    item.Competition = null;
                    item.IsMarketDataDelayed = false;
                    item.Description = {};
                    item.Description.IsPersistenceEnabled = element.description.persistenceEnabled;
                    item.Description.IsBspMarket = element.description.bspMarket;
                    item.Description.MarketTime = "/Date(" + new Date(element.description.marketTime) / 1 + ")/";
                    item.Description.SuspendTime = "/Date(" + new Date(element.description.suspendTime) / 1 + ")/";
                    item.Description.SettleTime = null;
                    item.Description.BettingType = 0;
                    item.Description.IsTurnInPlayEnabled = element.description.turnInPlayEnabled;
                    item.Description.MarketType = element.description.marketType;
                    item.Description.Regulator = element.description.regulator;
                    item.Description.MarketBaseRate = element.description.marketBaseRate;
                    item.Description.IsDiscountAllowed = element.description.discountAllowed;
                    item.Description.Wallet = element.description.wallet;
                    item.Description.Rules = "";
                    item.Description.RulesHasDate = element.description.rulesHasDate;
                    item.Description.Clarifications = null;
                    item.Runners = [];
                    element.runners.forEach(runner => {
                        let tempRunner = {};
                        tempRunner.SelectionId = runner.selectionId;
                        tempRunner.RunnerName = runner.runnerName;
                        tempRunner.Handicap = runner.handicap;
                        tempRunner.Metadata = null;
                        item.Runners.push(tempRunner);
                    });
                    resp.push(item);
                });
                res.json(resp);
            } else {
                res.json([]);
            }
        } else if (sources[0].sourceID == 9) {
            var sports = [{
                id: 4,
                name: 'cricket'
            },
                {
                    id: 2,
                    name: 'tennis'
                },
                {
                    id: 1,
                    name: 'soccer'
                }];
            if (req.body.strEventTypeId == undefined || req.body.strEventTypeId == 0) {
                req.body.strEventTypeId = 4;
            }
            var sportIdx = sports.findIndex(x => x.id == req.body.strEventTypeId);
            var form = new FormData();
            form.append('Id', req.body.strEventID);
            form.append('type', 'market');
            form.append('sport', sports[sportIdx].name);
            axios.create({
                headers: form.getHeaders()
            }).post('http://matchon.namo52.com/Api/listData', form).then(response => {
                let body = response.data;
                if (body != undefined) {
                    let list = body;
                    list.forEach(element => {
                        let item = {};
                        item.EventType = null;
                        item.MarketId = element.market_id;
                        item.MarketName = element.market_name;
                        item.Event = null;
                        item.Competition = null;
                        item.IsMarketDataDelayed = false;
                        item.Description = {};
                        item.Description.IsPersistenceEnabled = false;
                        item.Description.IsBspMarket = false;
                        item.Description.MarketTime = "/Date(" + new Date() / 1 + ")/";
                        item.Description.SuspendTime = "/Date(" + new Date() / 1 + ")/";
                        item.Description.SettleTime = null;
                        item.Description.BettingType = 0;
                        item.Description.IsTurnInPlayEnabled = true;
                        item.Description.MarketType = "";
                        item.Description.Regulator = "";
                        item.Description.MarketBaseRate = 0;
                        item.Description.IsDiscountAllowed = false;
                        item.Description.Wallet = "";
                        item.Description.Rules = "";
                        item.Description.RulesHasDate = false;
                        item.Description.Clarifications = null;
                        item.Runners = [];
                        resp.push(item);
                    });
                    res.json(resp);
                } else {
                    res.json([]);
                }
            }).catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
                console.log(error.message);
                res.json([]);
            });
        } else {
            res.json([]);
        }
    } catch (err) {
        console.log(err);
        storeErrorInDb("error_logs", "get_market_list : " + err.message);
        res.json({success: 0, message: "Error"});
    }

});
router.post("/verify_token", testToken, (req, res) => {
    res.send({status: true});
});
router.post("/import_market", testToken, async (req, res) => {
    try {
        var sport_id = req.body.sport_id;
        var tournament_id = req.body.tournament_id;
        var event_id = req.body.event_id;
        var market_id = req.body.market_id;
        if (market_id == null || market_id == "" || market_id == undefined) {
            storeErrorInDb("import_market", req.body, "Blank market Id");
            res.send({
                status: "error",
                message: "Please Pass market ID"
            });
            return;
        }
        let results = await knex(utils.TBL_CENTRAL_MARKET).where("sourceMarketID", market_id).select();
        if (results == null || results == [] || results == undefined || results.length == 0) {
            let sources = await knex(utils.TBL_SOURCE).where({"sourceType": 1, isActive: 1}).select();
            if (sources[0].sourceID == 6) {
                let params = {};
                let arr = [];
                arr[0] = market_id;
                params["marketIds"] = arr;
                let response = await axios.post('https://www.bet247exch.com/odds', params, {
                    headers: {
                        "key": "YhNzyE7naPcAnD9OffbJTqAOEBC06dhaPSumuzma2rGdr7KsNdnIRBo9lAw5",
                        "content-type": "application/json"
                    }
                });
                let logImportMarketData = {
                    "url": "https://www.bet247exch.com/odds",
                    "params": params,
                    "response": response.data,
                    "createdDateTime": new Date()
                };
                if (response == undefined || response.data == undefined) {
                    res.json({
                        error: "Record not found!"
                    });
                    logImportMarketData.importSuccess = 0;
                    // mongoObj.collection("import_market").insertOne(logImportMarketData);
                    return;
                } else {
                    logImportMarketData.importSuccess = 1;
                    // mongoObj.collection("import_market").insertOne(logImportMarketData);
                    response_parse = response.data.data.result[arr[0]];
                    if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                    } else {
                        var newCentralMarket = {
                            sourceMarketID: response_parse.marketId,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            // "marketName": market_name,
                            // "startTime": "",
                            // "type": "",
                            status: response_parse.status,
                            // "Result": "",
                            sourceID: 6,
                            isBetAllow: 1,
                            marketType: 1,
                            agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                            // "connectionCount": "",
                            // "numberOfRunners": ""
                        };

                        let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        res.json({
                            data: {
                                centralID: centralID
                            }
                        });
                    }
                }
            } else if (sources[0].sourceID == 2) {
                let params = {};
                let arr = [];
                arr[0] = market_id;
                params["marketIds"] = arr;
                let response = await axios.post('http://185.16.206.13:8080/api/listMarketBook', params, {headers: {"X-App": "test2"}});
                let logImportMarketData = {
                    "url": "http://185.16.206.13:8080/api/listMarketBook",
                    "params": params,
                    "response": response.data,
                    "createdDateTime": new Date()
                };
                if (response == undefined || response.data == undefined) {
                    res.json({
                        error: "Record not found!"
                    });
                    logImportMarketData.importSuccess = 0;
                    mongoObj.collection("import_market").insertOne(logImportMarketData);
                    return;
                } else {
                    logImportMarketData.importSuccess = 1;
                    mongoObj.collection("import_market").insertOne(logImportMarketData);
                    response_parse = response.data.result[0];
                    if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                    } else {
                        var newCentralMarket = {
                            sourceMarketID: response_parse.marketId,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            // "marketName": market_name,
                            // "startTime": "",
                            // "type": "",
                            status: response_parse.status,
                            // "Result": "",
                            isBetAllow: 1,
                            sourceID: 2,
                            marketType: 1,
                            agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                            // "connectionCount": "",
                            // "numberOfRunners": ""
                        };

                        let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        res.json({
                            data: {
                                centralID: centralID
                            }
                        });
                    }
                }
            } else if (sources[0].sourceID == 1) {
                let get_market_url = "http://rohitash.dream24.bet:3000/getmarket?id=" + market_id;
                let response = await axios.get(get_market_url);
                let logImportMarketData = {
                    "url": get_market_url,
                    "params": req.body,
                    "response": response.data,
                    "createdDateTime": new Date()
                };
                if (response.length == 0) {
                    res.json({
                        error: "Record not found!"
                    });
                    logImportMarketData.importSuccess = 0;
                    mongoObj.collection("import_market").insertOne(logImportMarketData);
                    return;
                } else {
                    logImportMarketData.importSuccess = 1;
                    mongoObj.collection("import_market").insertOne(logImportMarketData);
                    response_parse = response.data;
                    if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                    } else {
                        var newCentralMarket = {
                            sourceMarketID: response_parse.marketId,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            // "marketName": market_name,
                            // "startTime": "",
                            // "type": "",
                            status: response_parse.status,
                            // "Result": "",
                            sourceID: 1,
                            isBetAllow: 1,
                            marketType: 1,
                            agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                            // "connectionCount": "",
                            // "numberOfRunners": ""
                        };

                        let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        res.json({
                            data: {
                                centralID: centralID
                            }
                        });
                    }
                }
            } else if (sources[0].sourceID == 8) {
                let params = {"marketid": market_id, "isaustralian": false, "sitecode": 120, "createdby": 20};
                console.log(params);
                let response = await axios.post('http://185.44.76.182:788/Market/Site/GetCentralizationID', params,
                    {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
                let logImportMarketData = {
                    "url": "http://185.44.76.182:788/Market/Site/GetCentralizationID",
                    "params": req.body,
                    "response": response.data,
                    "createdDateTime": new Date()
                };
                console.log('response', response.data);
                if (response.length == 0) {
                    res.json({
                        error: "Record not found!"
                    });
                    logImportMarketData.importSuccess = 0;
                    mongoObj.collection("import_market").insertOne(logImportMarketData);
                    return;
                } else {
                    logImportMarketData.importSuccess = 1;
                    mongoObj.collection("import_market").insertOne(logImportMarketData);
                    response_parse = response.data.appdata;
                    if (response_parse.marketID == undefined || response_parse.marketID == null || response_parse.marketID == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                    } else {
                        let newCentralMarket = {
                            sourceMarketID: response_parse.bfMarketID,
                            tpSourceMarketID: response_parse.marketID,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            competitionName: response_parse.eventTypeName,
                            tournamentName: response_parse.competionsName,
                            matchName: response_parse.eventName,
                            marketName: response_parse.marketName,
                            startTime: response_parse.eventDate,
                            matchDate: response_parse.eventDate,
                            status: response_parse.status,
                            numberOfRunners: response_parse.noRunner,
                            sourceID: 8,
                            isBetAllow: 1,
                            marketType: 1,
                            bfMarketType: response_parse.betfairMarketType,
                            agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                        };
                        let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        let runners = [];
                        response_parse.runners.forEach(element => {
                            let runner = {
                                name: element.runner,
                                marketID: centralID,
                                tpRunnerID: element.selectionID
                            }
                            runners.push(runner);
                        });
                        await knex.batchInsert("tblrunner", runners);

                        res.json({
                            data: {
                                centralID: centralID
                            }
                        });
                    }
                }
            } else if (sources[0].sourceID == 10) {

                let response = await axios.get(sources[0].apiUrl + '/getMarkets?id=' + market_id);

                let logImportMarketData = {
                    "url": sources[0].apiUrl + "/getMarkets",
                    "params": market_id,
                    "response": response.data,
                    "createdDateTime": new Date()
                };
                if (response == undefined || response.data == undefined) {
                    res.json({
                        error: "Record not found!"
                    });
                    logImportMarketData.importSuccess = 0;
                    // mongoObj.collection("import_market").insertOne(logImportMarketData);
                    return;
                } else {
                    logImportMarketData.importSuccess = 1;
                    // mongoObj.collection("import_market").insertOne(logImportMarketData);
                    response_parse = response.data[market_id.toString()];
                    if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                    } else {
                        let newCentralMarket = {
                            sourceMarketID: response_parse.market_id,
                            tpSourceMarketID: response_parse.market_id,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            //competitionName: response_parse.eventTypeName,
                            //tournamentName: response_parse.competionsName,
                            //matchName: response_parse.eventName,
                            //marketName: response_parse.market_name,
                            //startTime: response_parse.marketStartTime,
                            //matchDate: response_parse.marketStartTime,
                            status: marketStatus[response_parse.status.toLowerCase()],
                            numberOfRunners: response_parse.numberOfRunners,
                            sourceID: 10,
                            isBetAllow: 1,
                            marketType: 1,
                            isInPlay: response_parse.inplay ? 1 : 0,
                            agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                        };
                        let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        let runners = [];
                        response_parse.runners.forEach(element => {
                            let runner = {
                                //name: element.runner,
                                marketID: centralID,
                                tpRunnerID: element.selectionId
                            }
                            runners.push(runner);
                        });
                        await knex.batchInsert("tblrunner", runners);

                        res.json({
                            data: {
                                centralID: centralID
                            }
                        });
                    }
                }
            }
        } else {
            res.json({
                data: {
                    centralID: results[0].centralMarketID
                }
            });
        }
    } catch (ex) {
        console.log(ex);
        storeErrorInDb("error_logs", req.body, "import_market : " + ex.message);
        res.json({status: 200, centralId: "", success: false});
    }

});

router.post("/get_status", testToken, async (req, res) => {
    var strCentralizedID = req.body.strCentralizedID;
    let logDataGetStatus = {
        "url": "get_status",
        "request": req.body,
        "createdDateTime": new Date()
    };
    if (strCentralizedID == null || strCentralizedID == "" || strCentralizedID == undefined) {
        let resp = {
            success: false,
            status: "error",
            message: "Please Pass Central ID"
        };
        logDataGetStatus.response = resp;
        // mongoObj.collection("get_status_logs").insertOne(logDataGetStatus);
        res.send(resp);
    } else {
        let results = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID", strCentralizedID).select();
        if (results == null || results == [] || results == undefined || results.length == 0) {
            res.send({
                success: false,
                status: "error",
                message: "Invalid Central ID"
            });
        } else {
            let isInPlay = true;
            let isBetAllow = true;
            if (results[0].marketType != 1) {
                let inPlayRow = await knex.raw("select isInPlay from tblcentralmarket where matchId = (select matchId from tblcentralmarket where centralMarketId = " + strCentralizedID + ") and marketName LIKE '%Match Odds%' LIMIT 1");
                if (inPlayRow[0][0] != undefined && inPlayRow[0][0].isInPlay != undefined) {
                    isInPlay = inPlayRow[0][0].isInPlay == 1 ? true : false;
                } else if (results[0].isInPlay == undefined || results[0].isInPlay == null || results[0].isInPlay == 0) {
                    isInPlay = false;
                }
            } else {
                if (results[0].isInPlay == undefined || results[0].isInPlay == null || results[0].isInPlay == 0) {
                    isInPlay = false;
                }
                if (results[0].isInPlay == 2) {
                    isInPlay = "";
                }
            }

            if (results[0].isBetAllow == undefined || results[0].isBetAllow == null || results[0].isBetAllow == 0) {
                isBetAllow = false;
            }
            let resp = {
                data: {
                    appCentralizedID: results[0].centralMarketID,
                    appMarketStatus: results[0].status,
                    appIsInPlay: isInPlay,
                    appIsSystemBetAllow: isBetAllow
                }
            };
            logDataGetStatus.response = resp;
            // mongoObj.collection("get_status_logs").insertOne(logDataGetStatus);
            res.json(resp);
        }
    }
});
router.post("/get_status_multiple_market", testToken, async (req, res) => {
    var strCentralizedID = req.body.strCentralizedID;
    if (strCentralizedID == null || strCentralizedID == "" || strCentralizedID == undefined) {
        let resp = {
            success: false,
            status: "error",
            message: "Please Pass Central ID"
        };
        res.send(resp);
    } else {
        let sql = "select centralMarketID as appCentralizedID,status as appMarketStatus,coalesce(isBetAllow,0) as appIsSystemBetAllow,CASE WHEN marketType != 1 THEN (select isInPlay from tblcentralmarket where matchId = tcm.matchID and marketName LIKE '%Match Odds%' LIMIT 1) ELSE tcm.isInPlay END as appIsInPlay from tblcentralmarket tcm where centralMarketID in (" + strCentralizedID + ")";
        let results = await knex.raw(sql);
        results = results[0];
        if (results == null || results == [] || results == undefined || results.length == 0) {
            res.send({
                success: false,
                status: "error",
                message: "Invalid Central ID"
            });
        } else {
            let resp = {
                data: results
            };
            res.json(resp);
        }
    }
});

router.post("/get_result", testToken, async (req, res) => {
    var strCentralizedID = req.body.strCentralizedID;
    let logDataGetResult = {
        "url": "get_result",
        "request_ip": req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        "request": req.body,
        "createdDateTime": new Date()
    };
    let resp = {};
    if (strCentralizedID == null || strCentralizedID == "" || strCentralizedID == undefined) {
        resp = {
            status: "error",
            message: "Please Pass Central ID"
        };
        logDataGetResult.response = resp;
        mongoObj.collection("get_result_logs").insertOne(logDataGetResult);
        res.send(resp);
        return;
    }
    let results = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID", strCentralizedID).select();
    if (results == null || results == [] || results == undefined || results.length == 0) {
        resp = {
            success: false,
            status: "error",
            message: "Invalid Central ID"
        }
        logDataGetResult.response = resp;
        mongoObj.collection("get_result_logs").insertOne(logDataGetResult);
        res.send(resp);
    } else {
        resp = {
            data: {
                result: results[0].Result
            }
        };
        logDataGetResult.response = resp;
        mongoObj.collection("get_result_logs").insertOne(logDataGetResult);
        res.json(resp);
    }
});

router.post("/get_result_multiple_market", testToken, async (req, res) => {
    var strCentralizedID = req.body.strCentralizedID;

    let resp = {};
    if (strCentralizedID == null || strCentralizedID == "" || strCentralizedID == undefined) {
        resp = {
            status: "error",
            message: "Please Pass Central ID"
        };
        res.send(resp);
        return;
    }
    let CentralizedIDArray = strCentralizedID.split(',');
    let results = await knex(utils.TBL_CENTRAL_MARKET).whereIn("centralMarketID", CentralizedIDArray).select("centralMarketID as centralId", "Result");
    if (results == null || results == [] || results == undefined || results.length == 0) {
        resp = {
            success: false,
            status: "error",
            message: "Invalid Central ID"
        }
        res.send(resp);
    } else {

        resp = {
            data: results
        };
        res.json(resp);
    }

});

router.post("/get_access_token", async function (req, res) {
    try {
        let ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
        ip = ip.split(":").pop();
        let agent_code = req.body.agentcode;
        let secret_key = req.body.secretkey;

        let whereConditions = {
            agentcode: agent_code,
            secretkey: secret_key
        };
        let data = {
            agentid: agent_code,
            datetime: new Date().getTime()
        };
        let result = await knex(utils.TBL_AGENT).where(whereConditions).select();
        if (result.length > 0) {
            // Username password match then generate token.
            jwt.sign(data, jwtSecret, (err, token) => {
                // Error while generating token.
                if (err) {
                    storeErrorInDb("get_access_token", req.body, err);
                    res.json({err_message: "Error while setting token."});
                    return;
                }
                res.json({token: token, agentcode: agent_code});
            });
        } else {
            storeErrorInDb("get_access_token", req.body, "Error while setting token.");
            res.json({err_message: "Error while setting token."});
            return;
        }
    } catch (ex) {
        console.log(ex);
        storeErrorInDb("get_access_token", req.body, ex);
        res.json({success: 0, message: "Error while updating market data."});
    }
});

router.post("/watch_market", async (req, res) => {
    try {
        let betfairID = req.body["centralMarketID"];
        let centralMarketResults = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID", betfairID).select();

        var apiresponse = {
            isValid: false,
            errors: [],
            data: null
        }

        if (centralMarketResults == null || centralMarketResults.length < 1)
            return res.json(apiresponse);

        let tpSourceMarketID = centralMarketResults[0].tpSourceMarketID;
        if (tpSourceMarketID == '' || tpSourceMarketID == null)
            return res.json(apiresponse);

        let params = {marketids: tpSourceMarketID.toString(), sitecode: 0};
        await axios
            .post("http://185.44.76.182:788/Market/Showtime", params, {
                headers: {
                    "X-Application": "jay",
                    "X-Authentication": "jaynodeapi"
                }
            })
            .then(response => {
                if (response != null && response != undefined && response.data != null && response.data.appissuccess == true) {
                    apiresponse.data = response.data.appdata;
                    apiresponse.isValid = true;
                    res.json(apiresponse);
                    return;
                }
                res.json(apiresponse);
            })
            .catch(error => {
                res.json(apiresponse);
            });
    } catch (err) {
        res.json(apiresponse);
    }
});

router.post("/get_fancy_list", testToken, async (req, res) => {
    try {
        let param = "";
        if(req.body.matchId !== "")
            param = req.body.matchId;
        if(req.body.strEventID !== undefined && req.body.strEventID !== "")
            param = req.body.strEventID;
        let params = {"eventids": param};
        let response = await axios.post('http://185.44.76.182:788/Market/Site/listManualMarket', params,
            {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
        res.json(response.data);
    } catch (err) {
        console.log(err);
        storeErrorInDb("error_logs", "get_fancy_list : " + err.message);
        res.json({success: 0, message: "Error"});
    }
});

router.post("/import_fancy_market", testToken, async (req, res) => {
    try {
        var sport_id = req.body.sport_id;
        var tournament_id = req.body.tournament_id;
        var event_id = req.body.match_id;
        var market_ids = req.body.market_ids;
        if (market_ids == null || market_ids == "" || market_ids == undefined) {
            storeErrorInDb("import_fancy", req.body, "Blank market Ids");
            res.send({
                status: "error",
                message: "Please Pass market IDs"
            });
            return;
        }
        let params = {"eventids": event_id};
        let response = await axios.post('http://185.44.76.182:788/Market/Site/listManualMarket', params,
            {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
        let logImportMarketData = {
            "url": "http://185.44.76.182:788/Market/Site/listManualMarket",
            "params": req.body,
            "response": response.data,
            "createdDateTime": new Date()
        };
        var marketIds = req.body.market_ids.split(",");
        var datalist = [];
        for (let market_id of marketIds) {
            let results = await knex(utils.TBL_CENTRAL_MARKET).where("sourceMarketID", market_id).select();
            if (results == null || results == [] || results == undefined || results.length == 0) {
                var marketData = response.data.appdata.filter(function (el) {
                    return el.marketID == market_id;
                });

                if (marketData == null || marketData == undefined || marketData.length < 1)
                    continue;

                marketData = marketData[0];
                let newCentralMarket = {
                    sourceMarketID: marketData.marketID,
                    tpSourceMarketID: marketData.marketID,
                    competitionID: sport_id,
                    tournamentID: tournament_id,
                    matchID: event_id,
                    competitionName: marketData.eventTypeName,
                    tournamentName: marketData.competionsName,
                    matchName: marketData.matchName,
                    marketName: marketData.marketName,
                    startTime: marketData.matchDate,
                    matchDate: marketData.matchDate,
                    status: marketData.marketStatus,
                    numberOfRunners: marketData.noRunner,
                    sourceID: 8,
                    isBetAllow: 1,
                    marketType: marketData.marketType,
                    bfMarketType: marketData.marketTypeName,
                    agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                };
                let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                let centralID = resultAddData[0];
                let runners = [];
                marketData.runner.forEach(element => {
                    let runner = {
                        name: element.runnerName,
                        marketID: centralID,
                        tpRunnerID: element.selectionID
                    }
                    runners.push(runner);
                });
                await knex.batchInsert("tblrunner", runners);
                datalist.push({
                    centralId: centralID,
                    marketId: market_id
                });
            } else {
                datalist.push({
                    centralId: results[0].centralMarketID,
                    marketId: market_id
                });
            }
        }
        res.json({
            data: datalist
        });

    } catch (ex) {
        console.log(ex);
        storeErrorInDb("error_logs", req.body, "import_fancy : " + ex.message);
        res.json({status: 200, data: [], success: false});
    }
});


router.post("/import_matchodds_bs", testToken, async (req, res) => {
    try {
        let authObj = new auth();
        var tournament_id = req.body.tournament_id;
        var event_id = req.body.event_id;
        var market_id = req.body.market_id;
        var sport_id = req.body.sport_id;
        var agent_ids = JSON.parse(req.body.agentIDs);
        var agentMarketImportparams = {};
        let message = "";
        let status = "success";
        if (market_id == null || market_id == "" || market_id == undefined) {
            storeErrorInDb("import_market", req.body, "Blank market Id");
            res.send({
                status: "error",
                message: "Please Pass market ID"
            });
            return;
        }
        if (agent_ids == undefined || agent_ids == "" || agent_ids.length == 0) {
            res.send({
                status: "error",
                message: "Please Select Agent"
            });
            return;
        }
        let results = await knex(utils.TBL_CENTRAL_MARKET).select().where("sourceMarketID", market_id);

        let centralID = 0;
        if (results == null || results == [] || results == undefined || results.length == 0) {
            let sources = await
                knex(utils.TBL_SOURCE).where({"sourceType": 1, isActive: 1}).select();
            if (sources[0].sourceID == 2) {
                let params = {};
                let arr = [];
                arr[0] = marketID;
                params["marketIds"] = arr;
                let response = await
                    axios.post('http://185.16.206.13:8080/api/listMarketBook', params, {headers: {"X-App": "test2"}});
                if (response == undefined || response.data == undefined) {
                    res.json({
                        error: "Record not found!"
                    });
                    return;
                } else {
                    response_parse = response.data.result[0];
                    if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                    } else {
                        var newCentralMarket = {
                            sourceMarketID: response_parse.marketId,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            // "marketName": market_name,
                            // "startTime": "",
                            // "type": "",
                            status: response_parse.status,
                            // "Result": "",
                            sourceID: 2,
                            isBetAllow: 1,
                            marketType: 1
                            // "connectionCount": "",
                            // "numberOfRunners": ""
                        };

                        let resultAddData = await
                            knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        res.json({
                            data: {
                                centralID: centralID
                            }
                        });
                    }
                }
            } else if (sources[0].sourceID == 1) {
                var get_market_url =
                    "http://rohitash.dream24.bet:3000/getmarket?id=" + marketID;
                let response = await
                    axios.get(get_market_url);
                let logImportMarketData = {
                    "url": get_market_url,
                    "params": req.body,
                    "response": response.data,
                    "createdDateTime": new Date()
                };
                if (response.length == 0) {
                    res.json({
                        data: {
                            centralID: ""
                        }
                    });
                } else {
                    response_parse = response.data;
                    if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
                        res.json({
                            data: {
                                centralID: ""
                            }
                        });
                        return;
                    } else {
                        var newCentralMarket = {
                            sourceMarketID: response_parse.marketId,
                            competitionID: eventTypeID,
                            competitionName: eventTypeName,
                            tournamentID: competitionID,
                            tournamentName: competition,
                            matchID: eventID,
                            matchName: eventName,
                            marketName: marketName,
                            matchDate: openDate,
                            betStatus: betStatus,
                            isBetAllow: isBetAllow,
                            marketType: 1,
                            description: rules,
                            isInplay: isInplay,
                            status: response_parse.status,
                            sourceID: 1
                        };
                        let resultAddData = await
                            knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        centralID = resultAddData[0];
                    }
                }
            } else if (sources[0].sourceID == 8) {
                let params = {"marketid": market_id, "isaustralian": false, "sitecode": 120, "createdby": 20};
                let response = await
                    axios.post('http://185.44.76.182:788/Market/Site/GetCentralizationID', params,
                        {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
                if (response.length == 0) {
                    res.json({
                        status: "error",
                        message: "Market is not valid"
                    });
                    return;
                } else {
                    response_parse = response.data.appdata;
                    if (response_parse.marketID == undefined || response_parse.marketID == null || response_parse.marketID == "") {
                        res.json({
                            status: "error",
                            message: "This market cannot be imported."
                        });
                        return;
                    } else {
                        let newCentralMarket = {
                            sourceMarketID: response_parse.bfMarketID,
                            tpSourceMarketID: response_parse.marketID,
                            competitionID: sport_id,
                            tournamentID: tournament_id,
                            matchID: event_id,
                            competitionName: response_parse.eventTypeName,
                            tournamentName: response_parse.competionsName,
                            matchName: response_parse.eventName,
                            marketName: response_parse.marketName,
                            startTime: response_parse.eventDate,
                            matchDate: response_parse.eventDate,
                            status: response_parse.status,
                            numberOfRunners: response_parse.noRunner,
                            bfMarketType: response_parse.betfairMarketType,
                            sourceID: 8,
                            isBetAllow: 1,
                            marketType: 1,
                            agentcode: res.locals.agentcode ? res.locals.agentcode : ''
                        };
                        let resultAddData = await
                            knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
                        let centralID = resultAddData[0];
                        let runners = [];
                        let importArr = [];
                        response_parse.runners.forEach(element => {
                                let runner = {
                                    name: element.runner,
                                    marketID: centralID,
                                    tpRunnerID: element.selectionID
                                }
                                importArr.push({
                                    RunnerName: element.runner,
                                    bfRunnerId: element.selectionID
                                });
                                runners.push(runner);
                            }
                        )
                        await
                            knex.batchInsert("tblrunner", runners);
                        agentMarketImportparams = {
                            bfSportId: sport_id,
                            SportName: response_parse.eventTypeName,
                            SportDescrption: "",
                            bfTournamentId: tournament_id,
                            TournamentName: response_parse.competionsName,
                            TournamentDescrption: "",
                            bfMatchId: event_id,
                            MatchName: response_parse.eventName,
                            MatchDescrption: "",
                            bfMarketId: response_parse.bfMarketID,
                            MarketName: response_parse.marketName,
                            MarketDescrption: "",
                            Result: "",
                            IsSettled: false,
                            IsFancy: false,
                            BettingType: "0",
                            Clarifications: null,
                            IsBspMarket: false,
                            IsDiscountAllowed: false,
                            IsPersistenceEnabled: true,
                            IsTurnInPlayEnabled: true,
                            MarketBaseRate: 5.0,
                            MarketTime: response_parse.eventDate,
                            MarketType: response_parse.betfairMarketType,
                            Regulator: "",
                            Rules: "",
                            RulesHasDate: false,
                            SettleTime: null,
                            SuspendTime: null,
                            OpenDate: response_parse.eventDate,
                            Wallet: "",
                            CentralId: centralID,
                            runners: importArr
                        };
                    }
                }
            }
        } else {
            centralID = results[0].centralMarketID;
            let marketData = results[0];
            let runners = await
                knex(utils.TBL_RUNNER).where({"marketID": centralID}).select('name as RunnerName', 'tpRunnerID as bfRunnerId');
            agentMarketImportparams = {
                bfSportId: marketData.competitionID,
                SportName: marketData.competitionName,
                SportDescrption: "",
                bfTournamentId: marketData.tournamentID,
                TournamentName: marketData.tournamentName,
                TournamentDescrption: "",
                bfMatchId: marketData.matchID,
                MatchName: marketData.matchName,
                MatchDescrption: "",
                MarketTime: marketData.matchDate,
                CentralId: centralID,
                bfMarketId: marketData.sourceMarketID,
                MarketName: marketData.marketName,
                MarketDescrption: "",
                Result: "",
                IsSettled: false,
                IsFancy: false,
                BettingType: "0",
                Clarifications: null,
                IsBspMarket: false,
                centralizationID: centralID,
                IsDiscountAllowed: false,
                IsPersistenceEnabled: true,
                IsTurnInPlayEnabled: false,
                MarketBaseRate: 0.0,
                MarketType: marketData.bfMarketType,
                Regulator: "",
                Rules: "",
                RulesHasDate: false,
                SettleTime: null,
                SuspendTime: marketData.matchDate,
                OpenDate: marketData.matchDate,
                Wallet: "",
                runners: runners
            };
        }

        let apiUrlResults = await knex(utils.TBL_AGENT).distinct("import_api_url", "bs_secret_key", "bs_access_token", "username").whereIn("id", agent_ids);
        if (apiUrlResults.length) {
            for (let row of apiUrlResults) {
                let secret_key = row.bs_secret_key;
                let access_token = row.bs_access_token;
                let isValidAccessToken = false;
                if (access_token !== undefined && access_token.length > 0) {
                    let result1 = await authObj.IsValidToken(
                        row.import_api_url,
                        access_token
                    );
                    if (result1 != undefined && result1.status != undefined && result1.status == 200) {
                        isValidAccessToken = true;
                    }
                }

                if (!isValidAccessToken) {
                    var tokenResponse = await authObj.GetAccessToken(
                        row.import_api_url,
                        secret_key
                    );
                    if (tokenResponse.status != 200) {
                    } else {
                        access_token = tokenResponse.result.token;
                        await knex(utils.TBL_AGENT).where("import_api_url", row.import_api_url).update({bs_access_token: access_token});
                        isValidAccessToken = true;
                    }
                }
                //Call api to import market to Website

                if (isValidAccessToken) {
                    try {
                        let importResponse = await axios
                            .post(row.import_api_url + "SaveImportMarketData", agentMarketImportparams, {
                                headers: {Authorization: "Bearer " + access_token}
                            })
                        message += row.username + " : " + importResponse.data.Message + "<br/>";
                    } catch (e) {
                        status = "error";
                        message += "Cannot import to " + row.username + "<br/>";
                    }
                } else {
                    status = "error";
                    message += "Cannot import to " + row.username + "<br/>";
                }
            }
        }
        res.json({
            status: status,
            message: message
        });
    } catch (ex) {
        storeErrorInDb("import_matchodds_bs", req.body, ex.message);
        res.json({status: "failure", message: ex.message, centralId: "", success: false});
    }
});

router.post("/import_fancy_bs", testToken, async (req, res) => {
    try {
        var sport_id = req.body.sport_id;
        var tournament_id = req.body.tournament_id;
        var event_id = req.body.event_id;
        var market_id = req.body.market_id;
        var sportName = req.body.sportName;
        var tournamentName = req.body.tournamentName;
        var agent_ids = JSON.parse(req.body.agentIDs);
        var agentMarketImportparams = {};
        let message = "", status = "success";
        if (market_id == null || market_id == "" || market_id == undefined) {
            storeErrorInDb("import_fancy", req.body, "Blank market Id");
            res.send({
                status: "error",
                message: "Please Pass market ID"
            });
            return;
        }
        if (agent_ids == undefined || agent_ids == "" || agent_ids.length == 0) {
            res.send({
                status: "error",
                message: "Please Select Agent"
            });
            return;
        }
        let params = {"eventids": event_id};
        let response = await axios.post('http://185.44.76.182:788/Market/Site/listManualMarket', params,
            {headers: {"X-Application": "jay", "X-Authentication": "jaynodeapi"}});
        var datalist = [];

        let results = await knex(utils.TBL_CENTRAL_MARKET).where("sourceMarketID", market_id).select();
        if (results == null || results == [] || results == undefined || results.length == 0) {
            var marketData = response.data.appdata.filter(function (el) {
                return el.marketID == market_id;
            });

            if (marketData == null || marketData == undefined || marketData.length < 1)
                res.json({status: 200, data: [], success: false});

            marketData = marketData[0];
            let newCentralMarket = {
                sourceMarketID: marketData.marketID,
                tpSourceMarketID: marketData.marketID,
                competitionID: sport_id,
                tournamentID: tournament_id,
                matchID: event_id,
                competitionName: sportName,
                tournamentName: tournamentName,
                matchName: marketData.matchName,
                marketName: marketData.marketName,
                startTime: marketData.matchDate,
                matchDate: marketData.matchDate,
                status: marketData.marketStatus,
                numberOfRunners: marketData.runner.length,
                bfMarketType: marketData.marketTypeName,
                sourceID: 8,
                isBetAllow: 1,
                marketType: marketData.marketType,
                agentcode: res.locals.agentcode ? res.locals.agentcode : ''
            };
            let resultAddData = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket);
            let centralID = resultAddData[0];
            let runners = [];
            let importArr = [];
            marketData.runner.forEach(element => {
                let runner = {
                    name: element.runnerName,
                    marketID: centralID,
                    tpRunnerID: element.selectionID
                }
                importArr.push({
                    RunnerName: element.runnerName,
                    bfRunnerId: element.selectionID
                });
                runners.push(runner);
            });
            await knex.batchInsert("tblrunner", runners);
            agentMarketImportparams = {
                bfSportId: sport_id,
                SportName: sportName,
                SportDescrption: "",
                bfTournamentId: tournament_id,
                TournamentName: tournamentName,
                TournamentDescrption: "",
                bfMatchId: event_id,
                MatchName: marketData.eventName,
                MatchDescrption: "",
                bfMarketId: marketData.marketID,
                MarketName: marketData.marketName,
                MarketDescrption: "",
                Result: "",
                IsSettled: false,
                IsFancy: true,
                BettingType: "0",
                Clarifications: null,
                IsBspMarket: false,
                IsDiscountAllowed: false,
                IsPersistenceEnabled: true,
                IsTurnInPlayEnabled: true,
                MarketBaseRate: 5.0,
                MarketTime: marketData.eventDate,
                MarketType: marketData.marketTypeName,
                Regulator: "",
                Rules: "",
                RulesHasDate: false,
                SettleTime: null,
                SuspendTime: null,
                OpenDate: marketData.eventDate,
                Wallet: "",
                CentralId: centralID,
                runners: importArr
            };
            datalist.push({
                centralId: centralID,
                marketId: market_id
            });
        } else {
            let marketData = results[0];
            let centralID = results[0].centralMarketID;
            let runners = await
                knex(utils.TBL_RUNNER).where({"marketID": centralID}).select('name as RunnerName', 'tpRunnerID as bfRunnerId');
            agentMarketImportparams = {
                bfSportId: marketData.competitionID,
                SportName: marketData.competitionName,
                SportDescrption: "",
                bfTournamentId: marketData.tournamentID,
                TournamentName: marketData.tournamentName,
                TournamentDescrption: "",
                bfMatchId: marketData.matchID,
                MatchName: marketData.matchName,
                MatchDescrption: "",
                MarketTime: marketData.matchDate,
                CentralId: centralID,
                bfMarketId: marketData.sourceMarketID,
                MarketName: marketData.marketName,
                MarketDescrption: "",
                Result: "",
                IsSettled: false,
                IsFancy: true,
                BettingType: "0",
                Clarifications: null,
                IsBspMarket: false,
                centralizationID: centralID,
                IsDiscountAllowed: false,
                IsPersistenceEnabled: true,
                IsTurnInPlayEnabled: false,
                MarketBaseRate: 0.0,
                MarketType: marketData.bfMarketType,
                Regulator: "",
                Rules: "",
                RulesHasDate: false,
                SettleTime: null,
                SuspendTime: marketData.matchDate,
                OpenDate: marketData.matchDate,
                Wallet: "",
                runners: runners
            };
            datalist.push({
                centralId: results[0].centralMarketID,
                marketId: market_id
            });
        }
        let apiUrlResults = await knex(utils.TBL_AGENT).distinct("import_api_url", "bs_secret_key", "bs_access_token", "username").whereIn("id", agent_ids);
        if (apiUrlResults.length) {
            let authObj = new auth();
            for (let row of apiUrlResults) {
                let secret_key = row.bs_secret_key;
                let access_token = row.bs_access_token;
                let isValidAccessToken = false;
                if (access_token !== undefined && access_token.length > 0) {
                    let result1 = await authObj.IsValidToken(
                        row.import_api_url,
                        access_token
                    );
                    if (result1 != undefined && result1.status != undefined && result1.status == 200) {
                        isValidAccessToken = true;
                    }
                }

                if (!isValidAccessToken) {
                    var tokenResponse = await authObj.GetAccessToken(
                        row.import_api_url,
                        secret_key
                    );
                    if (tokenResponse.status != 200) {
                    } else {
                        access_token = tokenResponse.result.token;
                        await knex(utils.TBL_AGENT).where("import_api_url", row.import_api_url).update({bs_access_token: access_token});
                        isValidAccessToken = true;
                    }
                }
                //Call api to import market to Website

                if (isValidAccessToken) {
                    try {
                        let importResponse = await axios
                            .post(row.import_api_url + "SaveImportMarketData", agentMarketImportparams, {
                                headers: {Authorization: "Bearer " + access_token}
                            });
                        message += row.username + " : " + importResponse.data.Message + "<br/>";
                    } catch (e) {
                        status = "error";
                        message += "Cannot import to " + row.username + "<br/>";
                    }
                } else {
                    status = "error";
                    message += "Cannot import to " + row.username + "<br/>";
                }
            }
        }
        res.json({
            status: status,
            message: message
        });

    } catch (ex) {
        storeErrorInDb("error_logs", req.body, "import_fancy : " + ex.message);
        res.json({status: 200, data: [], success: false});
    }
});

router.post("/showMarket", testToken, async (req, res) => {
    try {
        let authObj = new auth();
        let status = "success", message = "";
        var centralMarketID = req.body.centralMarketID;
        var agent_ids = JSON.parse(req.body.agentIDs);
        if (agent_ids == undefined || agent_ids == "" || agent_ids.length == 0) {
            res.send({
                status: "error",
                message: "Please Select Agent"
            });
            return;
        }
        let apiUrlResults = await knex(utils.TBL_AGENT).distinct("import_api_url", "bs_secret_key", "bs_access_token", "username").whereIn("id", agent_ids);

        if (apiUrlResults.length) {
            for (let row of apiUrlResults) {
                let secret_key = row.bs_secret_key;
                let access_token = row.bs_access_token;
                let isValidAccessToken = false;
                if (access_token !== undefined && access_token.length > 0) {
                    let result1 = await authObj.IsValidToken(
                        row.import_api_url,
                        access_token
                    );
                    if (result1 != undefined && result1.status != undefined && result1.status == 200) {
                        isValidAccessToken = true;
                    }
                }

                if (!isValidAccessToken) {
                    var tokenResponse = await authObj.GetAccessToken(
                        row.import_api_url,
                        secret_key
                    );
                    if (tokenResponse.status != 200) {
                    } else {
                        access_token = tokenResponse.result.token;
                        await knex(utils.TBL_AGENT).where("import_api_url", row.import_api_url).update({bs_access_token: access_token});
                        isValidAccessToken = true;
                    }
                }
                //Call api to import market to Website

                if (isValidAccessToken) {
                    try {
                        agentMarketImportparams = {
                            "CentralID": centralMarketID,
                            "IsActive": 1
                        };
                        let importResponse = await axios
                            .post(row.import_api_url + "isactivedeactive", agentMarketImportparams, {
                                headers: {Authorization: "Bearer " + access_token}
                            });
                        message += row.username + " : " + importResponse.data.messages + "<br/>";
                    } catch (e) {
                        status = "error";
                        message += "Cannot update " + row.username + "<br/>";
                    }
                } else {
                    status = "error";
                    message += "Cannot update " + row.username + "<br/>";
                }
            }
        }

        res.json({
            status: status,
            message: message
        });

    } catch (ex) {
        storeErrorInDb("error_logs", req.body, "import_fancy : " + ex.message);
        res.json({status: 200, data: [], success: false});
    }
});

router.post("/hideMarket", testToken, async (req, res) => {
    try {
        let authObj = new auth();
        var centralMarketID = req.body.centralMarketID;
        var agent_ids = JSON.parse(req.body.agentIDs);
        if (agent_ids == undefined || agent_ids == "" || agent_ids.length == 0) {
            res.send({
                status: "error",
                message: "Please Select Agent"
            });
            return;
        }
        let status = "success",message="";
        let apiUrlResults = await knex(utils.TBL_AGENT).distinct("import_api_url", "bs_secret_key", "bs_access_token", "username").whereIn("id", agent_ids);
        if (apiUrlResults.length) {
            for (let row of apiUrlResults) {
                let secret_key = row.bs_secret_key;
                let access_token = row.bs_access_token;
                let isValidAccessToken = false;
                if (access_token !== undefined && access_token.length > 0) {
                    let result1 = await authObj.IsValidToken(
                        row.import_api_url,
                        access_token
                    );
                    if (result1 != undefined && result1.status != undefined && result1.status == 200) {
                        isValidAccessToken = true;
                    }
                }

                if (!isValidAccessToken) {
                    var tokenResponse = await authObj.GetAccessToken(
                        row.import_api_url,
                        secret_key
                    );
                    if (tokenResponse.status != 200) {
                    } else {
                        access_token = tokenResponse.result.token;
                        await knex(utils.TBL_AGENT).where("import_api_url", row.import_api_url).update({bs_access_token: access_token});
                        isValidAccessToken = true;
                    }
                }
                //Call api to import market to Website

                if (isValidAccessToken) {
                    try {
                        agentMarketImportparams = {
                            "CentralID": centralMarketID,
                            "IsActive": 0
                        };
                        let importResponse = await axios
                            .post(row.import_api_url + "isactivedeactive", agentMarketImportparams, {
                                headers: {Authorization: "Bearer " + access_token}
                            });
                        message = row.username + " : " + importResponse.data.messages+"<br/>";
                    } catch (e) {
                        status = "error";
                        message += "Cannot update to " + row.username + "<br/>";
                    }
                } else {
                    status = "error";
                    message += "Cannot update to " + row.username + "<br/>";
                }
            }
        }

        res.json({
            status: status,
            message: message
        });

    } catch (ex) {
        console.log(ex.message);
        storeErrorInDb("error_logs", req.body, "import_fancy : " + ex.message);
        res.json({status: 200, data: [], success: false});
    }
});
router.post("/closeMarket", testToken, async (req, res) => {
    try {
        var centralMarketID = req.body.centralMarketID;
        await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketId", centralMarketID).update({"status":4});
        res.json({
            status: "success",
            message: "Market Closed Successfully"
        });

    } catch (ex) {
        console.log(ex.message);
        storeErrorInDb("error_logs", req.body, "import_fancy : " + ex.message);
        res.json({status: 200, data: [], success: false});
    }
});
module.exports = router;