// Dependencies.
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(bodyParser.json());

const axios = require('axios');
var utils = require('../lib/utils');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const sequilze = require('../sequlizeDB');
// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

// [ : Post method :]
router.post('/get_status_ByIDS', verifyToken, (req, res) => {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(403);
      return;
    }

    var string = req.body.sourceMarketID;
    if(req.body.sourceMarketID == undefined || req.body.sourceMarketID == ""){
      res.json({err_message: 'Please Add IDS in sourceMarketID values'})
    }
    else{
      let Ids = string.split(",");
    common_model.getRowWhereINJoin('c.sourceMarketID, m.marketCode, m.rateJson','tblcentralmarket c','tblmarketrate m','m.marketCode = c.centralMarketID','m.isActive = 1 AND m.isDelete = 0 And','sourceMarketID',Ids)
    .then(result => {
      res.json({market: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error occurred while getting market Status.'})
    });
  }
  });
});


module.exports = router;