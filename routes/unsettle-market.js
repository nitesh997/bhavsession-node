// Dependencies.
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var https = require('https');
app.use(bodyParser.json());
const axios = require('axios');
var utils = require('../lib/utils');
var auth = require('../lib/kc_auth');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const sequilze = require('../sequlizeDB');
// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

router.get('/get_list', verifyToken, function(req, res) {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    common_model.getRowWhereOrderBy(utils.TBL_CENTRAL_MARKET,'*',{'result': null, 'status': 4},'centralMarketID','desc')
    .then(result => {
      res.json({market: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error occurred while getting market data.'})
    });

  });
});

module.exports = router;