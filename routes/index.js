// Dependencies.
var express = require('express');
var path = require('path');
var jwt = require('jsonwebtoken');
var utils = require('../lib/utils');
var md5 = require('md5');

var router = express.Router();

// Models.
var commonModel = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

/* Send Login file */
router.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/login.html'));
});

/* Send Login file */
router.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/login.html'));
});

/* Send admin-side file */
router.get('/add-market', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/add-market.html'));
});

/* Send admin-side file */
router.get('/list-market', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/list-market.html'));
});

router.get('/fancy-rate/:id', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/fancy-rate.html'));
});

router.get('/logout', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/logout.html'));
});

router.get('/bookmaker', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/bookmaker.html'));
});

router.get('/manual-odds', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/manual-odds.html'));
});

router.get('/add-bookmaker', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/add-bookmaker.html'));
});

router.get('/add-manual-odds', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/add-manual-odds.html'));
});

router.get('/bookmaker-rate/:id', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/bookmaker-rate.html'));
});

router.get('/manual-odds-rate/:id', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/manual-odds-rate.html'));
});

router.get('/odd-even', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/list-odd-even.html'));
});

router.get('/add-odd-even', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/add-odd-even.html'));
});

router.get('/unsettled-market', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/unsettled-market.html'));
});
router.get('/market-status', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/market-status.html'));
});

router.get('/import-market', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/import-market.html'));
});

router.get('/agents', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/agents.html'));
});


router.get('/manage-agent/:id', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/add-agent.html'));
});

router.get('/reports', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/request-log-report.html'));
});

// Dashbord page marketrates 
router.get('/market_rates', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/market_rates.html'));
});


// Dashbord page marketrates 
router.get('/get-rates', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/get-rates.html'));
});

/* Login */
router.post('/login', (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  // Check body has username password.
  if ('undefined' === typeof username || 'undefined' === typeof password) {
    res.json({ err_message: 'Invalid username or password.' });
    return;
  }

  username = username.trim();
  password = md5(password.trim());
  console.log(password)
  var pass= password;
  // Get user from userModel.
  commonModel.getActiveData(utils.TBL_USER, '*', { 'username': username, 'password': password })
    .then((results) => {
      // Username or password doesn't match then send invalid message.
      if (0 === results.length) {
        res.json({ err_message: 'Invalid username or password.' });

        return;
      }

      let user = {
        userID: results[0].userID
      }

      // Username password match then generate token.
      jwt.sign(user, jwtSecret, { expiresIn: 60 * 60 * 24 }, (err, token) => {
        // Error while generating token.
        if (err) {
          console.log(err);
          res.json({ err_message: 'Error while setting token.' });

          return;
        }

        // Send token.
        res.json({ token });
      });

    })
    .catch(err => {
      console.log(err);
      // Error while getting data.
      res.json({ err_message: 'Error while getting data.' });
    });

});


module.exports = router;