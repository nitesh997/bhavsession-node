var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const tables = require('./lib/tablevar');

require('dotenv').config();

var indexRouter = require('./routes/index');
var marketRouter = require('./routes/market');
var bookmakerRouter = require('./routes/bookmaker');
var manualOddsRouter = require('./routes/manual-odds');
var comonAPIsRouter = require('./routes/common-api');
var api = require('./routes/api');
var oddEvenRouter = require('./routes/odd-even');
var unsettlemarketRouter = require('./routes/unsettle-market');
var marketStatusRouter = require('./routes/market-status');

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/market', marketRouter);
app.use('/bookmaker', bookmakerRouter);
app.use('/manual-odds', manualOddsRouter);
app.use('/common', comonAPIsRouter);
app.use('/api', api);
app.use('/odd-even', oddEvenRouter);
app.use('/unsettle-market', unsettlemarketRouter);
app.use('/market-status', marketStatusRouter);
module.exports = app;