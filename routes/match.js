// Dependencies.
var express = require('express');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();

// Middlewares.
var verifyToken = require('../middlewares/verify_token');

// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

/* Add market. */
router.get('/get_active_list', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    // Get match list.
    return common_model.getActiveData('tblmatch')
    .then(result => {

      res.json({matches: result});

      return;

    })
    .catch(err => {
      console.log(err);
      // Error while getting match data.
      res.json({err_message: 'Error while getting match data.'});

      return;
    });
  });

});

/* Add market. */
router.get('/get_full_list', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    // Get match list.
    return common_model.getAllData('tblmatch')
    .then((results, fields) => {

      console.log(fields);
      res.json({matches: results});

      return;

    })
    .catch(err => {
      console.log(err);
      // Error while getting match data.
      res.json({err_message: 'Error while getting match data.'});

      return;
    });
  });

});


module.exports = router;
