// Dependencies.
var express = require('express');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var utils = require('../lib/utils');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const axios = require('axios');
var auth = require('../lib/kc_auth');
// Models.
var common_model = require('../models/common_model');
var knex = require("../config");
// Globals.
const jwtSecret = process.env.JWT_SECRET;

//request log report
    router.post('/requestlog', verifyToken, async (req, res) => {
        jwt.verify(req.token, jwtSecret, async (err, authData) => {
            if (err) {
                res.sendStatus(403);
                return;
            }

        let where = '1 = 1',record = 0;
        if(req.body.search != undefined && req.body.search != ''){
            where += ' AND (agent_username LIKE "%'+req.body.search+'" OR api_name LIKE "%'+req.body.search+'")';
        }
        if(req.body.agent != undefined && req.body.agent != ''){
            where += ' AND agent_username ="'+req.body.agent+'"';
        }
        if(req.body.toDate != undefined && req.body.toDate != ''){
            where += ' AND Date(datetime) <= "'+req.body.toDate+'"';
        }
        if(req.body.fromDate != undefined && req.body.fromDate != ''){
            where += ' AND Date(datetime) >= "'+req.body.fromDate+'"';
        }
        let result = await knex(utils.TBL_REQUEST_LOG).whereRaw(where).
            select('agent_username','api_name',knex.raw('count(*) as apiCount')).
            groupBy('api_name','agent_username').
            then(function(result){
                let resultCount = knex(utils.TBL_REQUEST_LOG).whereRaw(where).select(knex.raw('count(*) as apiCount')).groupBy('api_name','agent_username');
                if(resultCount.length > 0){
                    record = resultCount[0].apiCount
                }
                res.json({data: result,recordsTotal:record,recordsFiltered:record});
            }).catch(err => {
                console.log(err);
                res.json({ err_message: 'Error while adding market data.' });
            });
    }).catch(err => {
        console.log(err);
        res.json({err_message: 'Error occurred while getting agents data.'});
        });
    });

    router.post('/requestlog_agent_list', verifyToken, async (req, res) => {
        jwt.verify(req.token, jwtSecret, async (err, authData) => {
        if (err) {
            console.log(err);
            res.sendStatus(403);
            return;
        }
        let result = await knex(utils.TBL_REQUEST_LOG).select(knex.raw('distinct(agent_username)'));
        res.json({data: result});
        }).catch(err => {
            console.log(err);
        res.json({err_message: 'Error occurred while getting agents data.'})
        });
    });

module.exports = router;