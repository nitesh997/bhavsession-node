const signalR = require("@microsoft/signalr");
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');
const knex = require('./config');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
//var url = "mongodb://mongouser:Shashank%40123@localhost:27017/?authSource=admin&readPreference=primary&gssapiServiceName=mongodb&appname=MongoDB%20Compass&ssl=false";
var mongoObj = undefined;
MongoClient.connect(url, function(err, db) {
    if (err) 
        console.log(err);
    mongoObj = db.db("centralserver");
});
// Body parser.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// Headers for cors.
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
var timer;
let markets="";

let connection = new signalR.HubConnectionBuilder()
    .withUrl("http://185.193.36.234:1215/hubs")
    .build();
connection.onclose(function(err){
    console.log(err);
    markets = "";
    start();
});

function start() {
    try {
        connection.start().then(function () {
            fetchData();
        }).catch(function (err) {
            return console.error(err.toString());
        });;
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};

connection.on("ReceiveMessage", function (message) {
    try{
        (async () => {
            for (let i = 0; i < message.length; i++) {
                let market = message[i];
                console.log(market);
                let results = await knex("tblcentralmarket").where("tpSourceMarketID",market["appMarketID"]).select('centralMarketID','matchID').orderBy('centralMarketID',"desc");
                if(results.length > 0)
                {
                    console.log(results[0]);
                    let newJson = formatJson(market,results[0].centralMarketID,results[0].matchID);
                    let status = market["appMarketStatus"];
                    let statusData = {
                        "status":status,
						"isInPlay": market["appIsInPlay"] == true ? 1 : 0
                    };
                    //await knex("tblcentralmarket").where("centralMarketID",results[0].centralMarketID).update(statusData);
                    //axios.post('https://bfrates.com:8888/marketapi/'+results[0].centralMarketID,{data:newJson,messageType:"match_odds"});
                }
            }
        })();
    } catch(e) {
        console.log(e);
        if(results == undefined)
            results = {};
        let errorData = {
            "api":"source script for kc",
            "error":e,
            "params":results,
            "message":e.message
          }
          mongoObj.collection("error_logs").insertOne(errorData,w=0);
    }
});
connection.start().then(function () {
    fetchData();
}).catch(function (err) {
    return console.error(err.toString());
});

// Models
async function fetchData()
{
    try{
        let results = await knex("tblcentralmarket").select(knex.raw("GROUP_CONCAT(DISTINCT tpSourceMarketID SEPARATOR ',') as 'values'")).where("marketType",1).where("sourceID",8).whereNot("status",4);
        if(!results || results.length == 0 || results[0].values == null)
        {
            if(timer != undefined)
                clearTimeout(timer);
            timer = setTimeout(function() {
                fetchData();
            }, (600));
            return;
        }else{
            let data = results[0].values;
            if(markets != data)
            {
                markets = data;
                connection.invoke("ConnectMultiMarketRate", data).catch(function (err) {
                    return console.error(err.toString());
                }); 
            }
            if(timer != undefined)
                clearTimeout(timer);
            timer = setTimeout(function() {
                fetchData();
            }, (600));
        }
        return;
    }catch(err){
        console.log(err);
        let errorData = {
            "api":"source script for bss",
            "error":err,
            "message":err.message
          }
          mongoObj.collection("error_logs").insertOne(errorData,w=0);
        fetchData();
        return;
    }
}
function formatJson(bfResponseObj,centralID,eventID)
{
    var requestedJson = {};
    
    requestedJson.appBetMarketID = bfResponseObj.marketId;
    //From Db : handicap,match_odds
    requestedJson.appMarketType = 0;
    requestedJson.appIsInPlay = bfResponseObj.appIsInPlay == true ? 1 : 0;
    requestedJson.appIsSystemBetAllow = true;
    requestedJson.appMarketID = centralID;
    requestedJson.appMarketStatus = bfResponseObj.appMarketStatus;
    requestedJson.appMatchEventID = eventID;

    requestedJson.appTotalMatched = bfResponseObj.appTotalMatched;
    requestedJson.appGetRecordTime = (new Date());
    var appRateArr = bfResponseObj.appRate;
    
    requestedJson.appRate = appRateArr;
    var requestedJsonArr = [];
    requestedJsonArr.push(requestedJson);
    return JSON.stringify(requestedJsonArr);

}