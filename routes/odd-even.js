// Dependencies.
var express = require('express');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var utils = require('../lib/utils');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const axios = require('axios');
var auth = require('../lib/kc_auth');
// Models.
var common_model = require('../models/common_model');
var market_model = require('../models/market_model');
const sequilze = require('../sequlizeDB');
// Globals.
const jwtSecret = process.env.JWT_SECRET;

/* Add market. */
router.post('/add', verifyToken, async (req, res) => {
  try {
    var authObj = new auth();
    var authData = jwt.verify(req.token, jwtSecret);
    let dataKeys = [
      'matchID', 'matchName', 'matchDate', 'tournamentID', 'tournamentName', 'competitionID', 'competitionName',
      'marketName', 'betStatus', 'maxStack', 'isFancyCode', 'isCommissionOn', 'rateDifference',
      'autoBallStartDuration', 'description', 'isActive', 'status'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ('undefined' === typeof value) {
        res.json({ err_message: 'Invalid market data.' });
        return;
      }
      data[dataKeys[i]] = value;
    }
    data.createdBy = authData.userID;
    data.marketType = utils.MARKET_TYPE_ODDEVEN;

    var result = await common_model.addData(utils.TBL_CENTRAL_MARKET, data);
    let centralID = result.insertId;
    if (data["autoBallStartDuration"] == "") { data["autoBallStartDuration"] = 0; }

    // Add market data.
    let results = await sequilze.query(`select distinct import_api_url,bs_secret_key,bs_access_token from tblagent where id in (select agent from user_agent where user = ${authData.userID})`, {type: sequilze.QueryTypes.SELECT});
    if(results.length) {
      for(let row of results) {
        let secret_key = row.bs_secret_key;
        let access_token = row.bs_access_token;
        let isValidAccessToken = false;
        if(access_token != undefined && access_token.length > 0)
        {
          var result = await authObj.IsValidToken(row.import_api_url,access_token);
          if(result.status == 200)
          {
            isValidAccessToken = true;
          }
        }
        if (!isValidAccessToken) {
          var tokenResponse = await authObj.GetAccessToken(row.import_api_url,secret_key);
          if (tokenResponse == undefined || tokenResponse.status != 200) {
            console.log('Error while adding market data.');
          } else {
            access_token = tokenResponse.result.token;
            await common_model.updateData(utils.TBL_AGENT, { bs_access_token: access_token }, { import_api_url: row.import_api_url });
          }
        }
          
          if(isValidAccessToken)
          {
            // call add new fancy api
            let params = {
              "marketID": data["matchID"],
              "marketName": data["marketName"],
              "eventID": data["matchID"],
              "eventName": data["matchName"],
              "competitionID": data["tournamentID"],
              "competitionName": data["tournamentName"],
              "eventTypeID": data["competitionID"],
              "eventTypeName": data["competitionName"],
              "betStatus": 0,
              "marketRate": 0,
              "isBetAllow": true,
              "isActive": false,
              "isFancy": true,
              "fancyType": 31,
              "status": 2,
              "centralizationID": centralID,
              "rateDifferent": data["rateDifference"],
              "maxStack": data["maxStack"],
              "isApplyCommision": true,
              "rules": data["description"],
              "ballStartSec": data["autoBallStartDuration"],
              "isInplay": true,
              "isCheckVolume": true,
              "afterClose": "2019-11-15T19:39:21.970Z",
              "afterSuspend": "2019-11-15T19:39:21.970Z",
              "afterBetAllowFalse": "2019-11-15T19:39:21.970Z",
              "siteCode": "101",
              "siteGroup": "",
              "runners": [
                {
                  "selectionID_BF": "",
                  "title": "Bet"
                }
              ]
            };
            axios.post(row.import_api_url + 'AddNewFancy', params, {
              headers: { "Authorization": "Bearer " + access_token }
            }).then(response => {
              console.log('response.. inserted successfully',response);
            }).catch(error => {
              console.log('error while inserting.. ', error);
              // res.json({ err_message: error });
            });
          }
        } 
      }
      res.json({ status: 200 });
    }
   catch (err) {
    res.json({ err_message: 'Error while adding market data.', "err":err.message });
  }
});
/* Add market rate json. */
router.post('/add_rate_json', verifyToken, function (req, res) {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let dataKeys = [
      'marketCode', 'rateJson'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ('undefined' === typeof value) {
        res.json({ err_message: 'Invalid market data.' });
        return;
      }

      data[dataKeys[i]] = value;
    }
    data.createdBy = authData.userID;

    // Add market data.
    return common_model.addData('tblmarketrate', data)
      .then(result => res.json({ result }))
      .catch(err => {
        console.log(err);
        res.json({ err_message: 'Error while adding market data.' });
      });
  });

});
/* Update market. */
router.post('/update/:id', verifyToken, function (req, res) {

  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let marketID = escape(req.params.id);
    if (typeof marketID === 'undefined') {
      res.json({ err_message: 'Invalid market data.' });
      return;
    }
    marketID = parseInt(marketID.trim(), 10);

    let dataKeys = [
      'matchID', 'matchName', 'matchDate', 'tournamentID', 'tournamentName', 'competitionID', 'competitionName',
      'marketCode', 'marketName', 'betStatus', 'maxStack', 'isFancyCode', 'isCommissionOn', 'rateDifference',
      'autoBallStartDuration', 'description', 'status', 'isActive', 'isDelete', 'isBetAllowed', 'result'
    ];
    let data = {
      centralMarketID: marketID
    };

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if (typeof value !== 'undefined') {
        data[dataKeys[i]] = value.trim();
      }
    }

    let length = 0;
    for (let i in data) {
      length++;
      break;
    }

    if (!length) {
      res.json({ err_message: 'Invalid market data.' });
      return;
    }

    common_model.updateData(utils.TBL_CENTRAL_MARKET, data, { 'centralMarketID': data.centralMarketID })
      .then(result => {
        res.json({ result });
      })
      .catch(err => {
        console.log(err);
        res.json({ err_message: 'Error while updating market data.' })
      });

  });

});

router.get('/get_list', verifyToken, function (req, res) {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      // console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    common_model.getRowWhereOrderBy(utils.TBL_CENTRAL_MARKET, '*', { 'marketType': utils.MARKET_TYPE_ODDEVEN }, 'centralMarketID', 'desc').then(result => {
      res.json({ market: result });
      return;
    }).catch(err => {
      console.log(err);
      res.json({ err_message: 'Error occurred while getting market data.' })
    });

  });
});

router.get('/get_details/:id', verifyToken, (req, res) => {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(403);
      return;
    }

    var id = escape(req.params.id);
    if (!id) {
      res.send({ err_message: 'Invalid market ID' });
      return;
    }

    id = parseInt(id.trim(), 10);
    if ('' === id) {
      res.send({ err_message: 'Invalid market ID' });
      return;
    }

    common_model.getAllData(utils.TBL_CENTRAL_MARKET, '*', { centralMarketID: id }).then(result => {
      res.json({ market: result });
      return;
    }).catch(err => {
      console.log(err);
      res.json({ err_message: 'Error while getting market data.' });
    });
  });
});

router.get('/get_runners/:id', verifyToken, async (req, res) => {
  jwt.verify(req.token, jwtSecret, async (err, authData) => {
    if (err) {
      res.sendStatus(403);
      return;
    }

    var id = escape(req.params.id);
    if (!id) {
      res.send({ err_message: 'Invalid market ID' });
      return;
    }

    id = parseInt(id.trim(), 10);
    if ('' === id) {
      res.send({ err_message: 'Invalid market ID' });
      return;
    }

    try {
      const market = await common_model.getAllData(utils.TBL_CENTRAL_MARKET, '*', { centralMarketID: id });
      const runners = await market_model.getRunners(id);
      res.json({
        market,
        runners
      })
    } catch (e) {
      res.json({ err_message: 'Error while getting market data.' });
    }
  });
});

module.exports = router;
