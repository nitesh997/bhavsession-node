// Dependencies.
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var https = require('https');
app.use(bodyParser.json());
const axios = require('axios');
var utils = require('../lib/utils');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const sequilze = require('../sequlizeDB');
// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

var crypto = require('crypto');

router.get('/get_list', verifyToken, function(req, res) {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    common_model.getRowWhereOrderBy(utils.TBL_AGENT,'*',{'isdelete': 0 } ,'name','desc')
    .then(result => {
      res.json({agents: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error occurred while getting agents data.'})
    });

  });
});

router.post('/add_agent', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let dataKeys = [
      'name', 'ip1', 'ip2', 'ip3','ip4','agentcode','import_api_url','username','bs_secret_key','isAutoImportMatchOdds'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ('undefined' === typeof value) {
        res.json({err_message: 'Invalid agent data.'});
        return;
      }

      data[dataKeys[i]] = value;
    }
    data.secretkey = crypto.createHash('sha256').update(data.username).digest('hex');
    data.bs_access_token = "";
    data.isDelete = 0;
    // Add agent data.
    return common_model.addData(utils.TBL_AGENT, data)
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while adding agent data.'});
    });
  });

});

router.post('/update/:id', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let agentId = escape(req.params.id);
    if (typeof agentId === 'undefined') {
      res.json({err_message: 'Invalid agent data.'});

      return;
    }
    agentId = parseInt(agentId.trim(), 10);

    let dataKeys = [
      'name', 'ip1', 'ip2', 'ip3','ip4','agentcode','import_api_url','username','bs_secret_key','isAutoImportMatchOdds'
    ];
    let data = {
      id: agentId
    };

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if (typeof value !== 'undefined') {
        data[dataKeys[i]] = value.trim();
      }
    }

    let length = 0;
    for (let i in data) {
      length ++;
      break;
    }

    if (!length) {
      res.json({err_message: 'Invalid agent data.'});
      return;
    }
    data.secretkey = crypto.createHash('sha256').update(data.username).digest('hex');
    common_model.updateData(utils.TBL_AGENT, data, {'id': data.id})
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while updating agent data.'})
    });

  });

});

router.post('/delete/:id', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let agentId = escape(req.params.id);
    if (typeof agentId === 'undefined') {
      res.json({err_message: 'Invalid agent data.'});

      return;
    }
    agentId = parseInt(agentId.trim(), 10);

    let data = {
      id: agentId
    };

    data.isDelete = 1;
    common_model.updateData(utils.TBL_AGENT, data, {'id': data.id})
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while delete agent data.'})
    });

  });

});

router.get('/get_details/:id', verifyToken, (req, res) => {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(403);
      return;
    }

    var id = escape(req.params.id);
    if (!id) {
      res.send({err_message: 'Invalid agent ID'});
      return;
    }

    id = parseInt(id.trim(), 10);
    if ('' === id) {
      res.send({err_message: 'Invalid agent ID'});
      return;
    }

    common_model.getAllDataWithoutisDelete(utils.TBL_AGENT, '*', {id: id}).then(result => {
      res.json({agent: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error while getting agent data.'});
    });
  });
});
module.exports = router;