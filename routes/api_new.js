var express = require("express");
var router = express.Router();
var axios = require("axios");
var testToken = require("../middlewares/test_token");
var jwt = require("jsonwebtoken");
var utils = require("../lib/utils");
var auth = require("../lib/kc_auth");
const jwtSecret = process.env.JWT_SECRET;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
//var url = "mongodb://naimesh:abc123@localhost:27017/";
// var url = "mongodb://mongouser:Shashank%40123@localhost:27017/?authSource=admin&readPreference=primary&gssapiServiceName=mongodb&appname=MongoDB%20Compass&ssl=false";
var mongoObj = undefined;
MongoClient.connect(url, function(err, db) {
    if (err) 
        console.log(err);
    mongoObj = db.db("centralserver");
});

// Models
var knex = require("../config");
var common_model = require("../models/common_model");
var soap = require("soap");
var url = "http://89.34.99.39:786/ICentralizationService.svc?wsdl";

router.post("/get_last_rate", testToken,async (req, res) => {
  var logData = {
    "url":req.url,
    "request":req.body,
    "createdDateTime":new Date()
  };
  var id = escape(req.body.marketCode);
  if (!id) {
    let resp = {
      status:false,
      err_message: "Invalid market ID"
    };
    res.json(resp);
    logData.response = resp;
    mongoObj.collection("last_rate_api_logs").insertOne(logData);
    return;
  }
  try{
    let result = await knex("tblmarketrate").where("marketCode",id).select('rateJson',knex.raw("(UNIX_TIMESTAMP() -  UNIX_TIMESTAMP(createdDateTime)) as diffSeconds"))
    var row = {};
    if (result.length > 0) {
      if (result[0].diffSeconds < 10) {
        row = result[0].rateJson;
        rateJson = JSON.parse(result[0].rateJson);
        rateJsonData = JSON.parse(rateJson.data);
        res.send(rateJsonData);
        logData.response = rateJsonData;
        mongoObj.collection("last_rate_api_logs").insertOne(logData);
        return;
      } else {
        rateJson = JSON.parse(result[0].rateJson);
        rateJsonData = JSON.parse(rateJson.data);
        row.data = rateJsonData;
        if (rateJson.messageType == "match_odds") {
          appRate = {};
          if (rateJsonData[0] != undefined && rateJsonData[0].appRate != undefined) {
            row.data[0].appRate = appRate;
          }
          res.json(rateJsonData);
          logData.response = rateJsonData;
          mongoObj.collection("last_rate_api_logs").insertOne(logData);
          return;
        } else {
          res.send(rateJsonData);
          logData.response = rateJsonData;
          mongoObj.collection("last_rate_api_logs").insertOne(logData);
          return;
        }
      }
    }else{
      logData.response = {};
      mongoObj.collection("last_rate_api_logs").insertOne(logData);
      res.json({});
    }
    return;
  }catch(e){
    res.json({});
  }
});

router.post("/add_new_fancy", testToken, async (req, res) => {
  try {
    var logData = {
      "url":"http://localhost:3000/api/add_new_fancy",
      "request":req.body,
      "createdDateTime":new Date()
    };
    var authObj = new auth();
    let dataKeys = ["matchID","matchName","matchDate","tournamentID","tournamentName","competitionID","competitionName",
      "marketName","betStatus","maxStack","isFancyCode","isCommissionOn","rateDifference","autoBallStartDuration",
      "description","isActive","status","betStatus","isBetAllow","isActive","isFancy","fancyType","marketRate",
      "isApplyCommision","isInplay","isCheckVolume","afterClose","afterSuspend","afterBetAllowFalse"];
    let data = {};
    data["status"] = 2;
    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ("undefined" === typeof value && value) {
        let errResp = {status:false, err_message: "Invalid market data." };
        res.json(errResp);
        logData.response = errResp;
        mongoObj.collection("add_fancy_log").insertOne(logData);
        return;
      }
      data[dataKeys[i]] = value;
    }
    data.matchDate = req.body["opendate"];
    data.createdBy = 0;
    data.marketType = utils.MARKET_TYPE_FANCY;
    let isCheckVolume = 0;
    
    if (req.body.isCheckVolume != undefined && req.body.isCheckVolume != "") {
      isCheckVolume = req.body.isCheckVolume;
      data["isCheckVolume"] = isCheckVolume;
    }
    if (req.body.afterClose != undefined && req.body.afterClose != "") {
      afterClose = req.body.afterClose;
    }
    if (req.body.afterSuspend != undefined && req.body.afterSuspend != "") {
      afterSuspend = req.body.afterSuspend;
    }
    if (req.body.afterBetAllow != undefined && req.body.afterBetAllow != "") {
      afterBetAllow = req.body.afterBetAllowFalse;
    }
    let centralID = await knex(utils.TBL_CENTRAL_MARKET).insert(data).returning('centralMarketID');
    
    if (data["autoBallStartDuration"] == "") {
      data["autoBallStartDuration"] = 0;
    }
    if (data["description"] == undefined || data["description"] == "")
      data["description"] = "";
    let strVals = data["description"].match(/(?<=":")([^:]+?)(?="(?=,|}|]))/g);
    if (strVals && strVals.length) {
      strVals.forEach(strVal => {
        let newVal = strVal.replace(/("|“|”)/g, '\\"');
        data["description"] = data["description"].replace(strVal, newVal);
      });
    }
    
    let runners = req.body.runners;
    if (req.body.fancyType != 6) {
      if (runners && runners.length) {
        runners.forEach(async singleRunner => {
          let newData = {
            tpRunnerID: singleRunner.selectionID_BF,
            marketID: centralID,
            name: singleRunner.title
          };
          await knex(utils.TBL_RUNNER).insert(newData);
        });
      }
    }
    let results = await knex(utils.TBL_AGENT).distinct("import_api_url","bs_secret_key","bs_access_token").where("id","in",knex("user_agent").where("user",1).select("agent"));
    if (results.length) {
      for (let row of results) {
        let secret_key = row.bs_secret_key;
        let access_token = row.bs_access_token;
        let isValidAccessToken = false;
        if (access_token != undefined && access_token.length > 0) {
          var result = await authObj.IsValidToken(
            row.import_api_url,
            access_token
          );
          var logDataAuth = {
            "url":row.import_api_url,
            "request":access_token,
            "createdDateTime":new Date(),
            "response":result
          };
          mongoObj.collection("add_fancy_log").insertOne(logDataAuth);
          if (result.status == 200) {
            isValidAccessToken = true;
          }
        }

        if (!isValidAccessToken) {
          var tokenResponse = await authObj.GetAccessToken(
            row.import_api_url,
            secret_key
          );
          var logDataGetToken = {
            "url":row.import_api_url,
            "request":secret_key,
            "createdDateTime":new Date(),
            "response":tokenResponse
          };
          mongoObj.collection("add_fancy_log").insertOne(logDataGetToken);
          if (tokenResponse.status != 200) {
            isValidAccessToken = false;
          } else {
            access_token = tokenResponse.result.token;
            await knex(utils.TBL_AGENT).where("import_api_url",row.import_api_url).update({ bs_access_token: access_token });
            isValidAccessToken = true; // after updating/getting access token need to set isValidAccessToken true so that insertion is done
          }
        }
        //Call api to import market to Website

        if (isValidAccessToken) {
          let params = {
            marketID: data["matchID"],
            marketName: data["marketName"],
            eventID: data["matchID"],
            eventName: data["matchName"],
            competitionID: data["tournamentID"],
            competitionName: data["tournamentName"],
            eventTypeID: data["competitionID"],
            eventTypeName: data["competitionName"],
            opendate:data["matchDate"],
            betStatus: data["betStatus"],
            marketRate: data["marketRate"],
            isBetAllow: data["isBetAllow"],
            isActive: data["isActive"],
            isFancy: data["isFancy"],
            fancyType: data["fancyType"],
            status: data["status"],
            centralizationID: centralID,
            rateDifferent: data["rateDifference"],
            maxStack: data["maxStack"],
            isApplyCommision: data["isApplyCommision"],
            rules: data["description"],
            ballStartSec: data["autoBallStartDuration"],
            isInplay: data["isInPlay"],
            isCheckVolume: data["isCheckVolume"],
            afterClose: data["afterClose"],
            afterSuspend: data["afterSuspend"],
            afterBetAllowFalse: data["afterBetAllowFalse"],
            siteCode: "101",
            siteGroup: ""
          };
          params["runners"] = [];
          if (req.body.fancyType == 6) {
            params["runners"] = [
              {
                selectionID_BF: "",
                title: "Bet"
              }
            ];
          } else {
            params["runners"] = req.body.runners;
            
            if (params["runners"] && params["runners"].length) {
              params["runners"] = [];
              let runnersList = await knex(utils.TBL_RUNNER).where("marketID",centralID).select("id","name","tpRunnerID");
              
              for (let row in runnersList) {
                let runner = {};
                runner.selectionID_BF = runnersList[row].id;
                runner.title = runnersList[row].name;
                runner.selectionID_BF_Ref = runnersList[row].tpRunnerID;
                params["runners"].push(runner);
              }
            }
          }
          await axios
            .post(row.import_api_url + "AddNewFancy", params, {
              headers: { Authorization: "Bearer " + access_token }
            })
            .then(response => {
              var logDataAddFancy = {
                "url":row.import_api_url + "AddNewFancy",
                "request":params,
                "auth":access_token,
                "createdDateTime":new Date(),
                "response":response
              };
              mongoObj.collection("add_fancy_log").insertOne(logDataAddFancy);
            })
            .catch(error => {
              console.log("error while executing query....", error);
            });
        }
      }
      res.json({ status: 200, success: 1,centralID:centralID,runners:params["runners"] });
      return;
    }
    res.json({status:false, err_message: "Error while adding market data." });
  } catch (err) {
    res.json({ status:false, centralID: 0, message: "Error Adding Market Data" });
  }
});

router.post("/fancy_status", testToken, async (req, res) => {
  try {
    var authObj = new auth();
    let centralID = req.body["centralId"];
    let isActive = req.body["isActive"];
    await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",centralID).update({ isActive: isActive });
    
    let results = await knex(utils.TBL_AGENT).distinct("import_api_url","bs_secret_key","bs_access_token").where("id","in",knex("user_agent").where("user",1).select("agent"));
    if (results.length) {
      for (let row of results) {
        let secret_key = row.bs_secret_key;
        let access_token = row.bs_access_token;
        let isValidAccessToken = false;
        if (access_token != undefined && access_token.length > 0) {
          var result = await authObj.IsValidToken(
            row.import_api_url,
            access_token
          );
          var logDataAuth = {
            "url":row.import_api_url,
            "request":access_token,
            "createdDateTime":new Date(),
            "response":result
          };
          mongoObj.collection("fancy_status_logs").insertOne(logDataAuth);
          if (result.status == 200) {
            isValidAccessToken = true;
          }
        }

        if (!isValidAccessToken) {
          var tokenResponse = await authObj.GetAccessToken(
            row.import_api_url,
            secret_key
          );
          var logDataGetToken = {
            "url":row.import_api_url,
            "request":secret_key,
            "createdDateTime":new Date(),
            "response":tokenResponse
          };
          mongoObj.collection("fancy_status_logs").insertOne(logDataGetToken);
          if (tokenResponse.status != 200) {
            isValidAccessToken = false;
          } else {
            access_token = tokenResponse.result.token;
            await knex(utils.TBL_AGENT).where("import_api_url",row.import_api_url).update({ bs_access_token: access_token });
            isValidAccessToken = true; // after updating/getting access token need to set isValidAccessToken true so that insertion is done
          }
        }
        //Call api to import market to Website

        if (isValidAccessToken) {
          let params = {
            CentralId: centralID,
            IsActive: isActive
          };
          await axios
            .post(row.import_api_url + "isactivedeactive", params, {
              headers: { Authorization: "Bearer " + access_token }
            })
            .then(response => {
              var logDataFancyStatus = {
                "url":row.import_api_url + "isactivedeactive",
                "request":params,
                "auth":access_token,
                "createdDateTime":new Date(),
                "response":response
              };
              mongoObj.collection("fancy_status_logs").insertOne(logDataAddFancy);
            })
            .catch(error => {
              console.log("error while executing query....", error);
            });
        }
      }
      res.json({ status: true});
      return;
    }
    res.json({ status: false,err_message : "Cannot Send requests"});
  } catch (err) {
    res.json({ status: false,err_message : "Cannot Send requests"});
  }
});

router.post("/bet_status", testToken, async (req, res) => {
  try {
    var authObj = new auth();
    let centralID = req.body["centralId"];
    let isActive = req.body["isBetAllow"];
    await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",centralID).update({ isBetAllow: isActive });
    let results = await knex(utils.TBL_AGENT).distinct("import_api_url","bs_secret_key","bs_access_token").where("id","in",knex("user_agent").where("user",1).select("agent"));
    
    if (results.length) {
      for (let row of results) {
        let secret_key = row.bs_secret_key;
        let access_token = row.bs_access_token;
        let isValidAccessToken = false;
        if (access_token != undefined && access_token.length > 0) {
          var result = await authObj.IsValidToken(
            row.import_api_url,
            access_token
          );
          var logDataAuth = {
            "url":row.import_api_url,
            "request":access_token,
            "createdDateTime":new Date(),
            "response":result
          };
          mongoObj.collection("bet_status_log").insertOne(logDataAuth);
          if (result.status == 200) {
            isValidAccessToken = true;
          }
        }

        if (!isValidAccessToken) {
          var tokenResponse = await authObj.GetAccessToken(
            row.import_api_url,
            secret_key
          );
          var logDataGetToken = {
            "url":row.import_api_url,
            "request":secret_key,
            "createdDateTime":new Date(),
            "response":tokenResponse
          };
          mongoObj.collection("bet_status_log").insertOne(logDataGetToken);
          if (tokenResponse.status != 200) {
            isValidAccessToken = false;
          } else {
            access_token = tokenResponse.result.token;
            await knex(utils.TBL_AGENT).where("import_api_url",row.import_api_url).update({ bs_access_token: access_token });
            isValidAccessToken = true; // after updating/getting access token need to set isValidAccessToken true so that insertion is done
          }
        }
        //Call api to import market to Website

        if (isValidAccessToken) {
          let params = {
            CentralId: centralID,
            IsActive: isActive
          };
          await axios
            .post(row.import_api_url + "isBetAllow", params, {
              headers: { Authorization: "Bearer " + access_token }
            })
            .then(response => {
              var logDataAddFancy = {
                "url":row.import_api_url + "isBetAllow",
                "request":params,
                "auth":access_token,
                "createdDateTime":new Date(),
                "response":response
              };
              mongoObj.collection("bet_status_log").insertOne(logDataAddFancy);
            })
            .catch(error => {
              console.log("error while executing query....", error);
            });
        }
      }
    }
    res.json({ success: true});
  } catch (err) {
    console.log("error in add_new_fency api... ", err);
    res.json({success:false,"err_message":"Cannot change the status"});
  }
});

router.post("/import_fancy", testToken, async (req, res) => {
  try {
    var authObj = new auth();
    let centralID = req.body.centralID;
    let markets = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",centralID).select('centralMarketID','matchID','marketName','matchDate','matchName','tournamentID','tournamentName','competitionID','competitionName','betStatus','marketRate','isBetAllow','isActive','isFancy','fancyType','status','rateDifference','maxStack','isApplyCommision','description','autoBallStartDuration','isInPlay','isCheckVolume','afterClose','afterSuspend','afterBetAllowFalse');
    let results = await knex(utils.TBL_AGENT).distinct("import_api_url","bs_secret_key","bs_access_token").where("id","in",knex("user_agent").where("user",1).select("agent"));
    
    if (markets.length && results.length) {
      let data = markets[0];
      for (let row of results) {
        let secret_key = row.bs_secret_key;
        let access_token = row.bs_access_token;
        let isValidAccessToken = false;
        if (access_token != undefined && access_token.length > 0) {
          var result = await authObj.IsValidToken(
            row.import_api_url,
            access_token
          );
          var logDataAuth = {
            "url":row.import_api_url,
            "request":access_token,
            "createdDateTime":new Date(),
            "response":result
          };
          mongoObj.collection("import_fancy_log").insertOne(logDataAuth);
          if (result.status == 200) {
            isValidAccessToken = true;
          }
        }

        if (!isValidAccessToken) {
          var tokenResponse = await authObj.GetAccessToken(
            row.import_api_url,
            secret_key
          );
          if (tokenResponse.status != 200) {
            isValidAccessToken = false;
          } else {
            access_token = tokenResponse.result.token;
            await knex(utils.TBL_AGENT).where("import_api_url",row.import_api_url).update({ bs_access_token: access_token });
            isValidAccessToken = true;
          }
        }
        //Call api to import market to Website

        if (isValidAccessToken) {
          let params = {
            marketID: data["matchID"],
            marketName: data["marketName"],
            eventID: data["matchID"],
            eventName: data["matchName"],
            competitionID: data["tournamentID"],
            competitionName: data["tournamentName"],
            eventTypeID: data["competitionID"],
            eventTypeName: data["competitionName"],
            opendate: data["matchDate"],
            betStatus: data["betStatus"],
            marketRate: data["marketRate"],
            isBetAllow: data["isBetAllow"],
            isActive: data["isActive"],
            isFancy: data["isFancy"],
            fancyType: data["fancyType"],
            status: data["status"],
            centralizationID: centralID,
            rateDifferent: data["rateDifference"],
            maxStack: data["maxStack"],
            isApplyCommision: data["isApplyCommision"],
            rules: data["description"],
            ballStartSec: data["autoBallStartDuration"],
            isInplay: data["isInPlay"],
            isCheckVolume: data["isCheckVolume"],
            afterClose: data["afterClose"],
            afterSuspend: data["afterSuspend"],
            afterBetAllowFalse: data["afterBetAllow"],
            siteCode: "101",
            siteGroup: ""
          };
          params["runners"] = [];
          if (req.body.fancyType == 6) {
            params["runners"] = [
              {
                selectionID_BF: "",
                title: "Bet"
              }
            ];
          } else {
            params["runners"] = [];
            let runnersList = await knex(utils.TBL_RUNNER).where("marketID",centralID).select("id","name","tpRunnerID");
            for (let row in runnersList) {
              let runner = {};
              runner.selectionID_BF = runnersList[row].id;
              runner.title = runnersList[row].name;
              runner.selectionID_BF_Ref = runnersList[row].tpRunnerID;
              params["runners"].push(runner);
            }
          }
          await axios
            .post(row.import_api_url + "AddNewFancy", params, {
              headers: { Authorization: "Bearer " + access_token }
            })
            .then(response => {
              var logDataAddFancy = {
                "url":row.import_api_url + "AddNewFancy",
                "request":params,
                "auth":access_token,
                "createdDateTime":new Date(),
                "response":response
              };
              mongoObj.collection("import_fancy_log").insertOne(logDataAddFancy);
            })
            .catch(error => {
              console.log("error while executing query....", error);
            });
        }
      }
      res.json({ status: 200, success: 1,centralID:centralID,runners:params["runners"] });
      return;
    }
    res.json({success:0,message:"Error importing fancy"});
  } catch (err) {
    res.json({ success: 0, centralID: 0, message: "Error Adding Market Data" });
  }
});

router.post("/change_status", testToken, async (req, res) => {
  try {
    let centralID = req.body["centralID"];
    let status = req.body["status"];
    await knex.where("centralMarketID",centralID).update({status:status});
    let data = [
      {
        appBetMarketID: null,
        appMarketID: centralID,
        appMarketStatus: status,
        appMatchEventID: null,
        appRate: "[]",
        appTotalMatched: null
      }
    ];
    let logDataChangeStatus = {
      "url":"change_status",
      "request":req.body,
      "createdDateTime":new Date()
    };
    mongoObj.collection("change_status_log").insertOne(logDataChangeStatus);
    let params = { data: JSON.stringify(data), messageType: "fancy" };
    axios.post(process.env.CENTRAL_URL + centralID, params)
          .then(response => {
            res.json({ success: 1, message: "Status Updated." });
          })
          .catch(error => {
            res.json({
              success: 0,
              message: "Error while updating market data."
            });
          });
  } catch (err) {
    res.json({ success: 0, centralID: 0, message: "Error Adding Market Data" });
  }
});

router.post("/set_result", testToken, async (req, res) => {
  try {
    let logDataSetResult = {
      "url":"set_result",
      "request":req.body,
      "createdDateTime":new Date()
    };
    mongoObj.collection("set_result_log").insertOne(logDataSetResult);
    let centralID = req.body["centralID"];
    let result = req.body["result"];
    await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",centralID).update({Result:result});
    res.json({ success: 1, message: "Result Updated Successfully" });
  } catch (err) {
    res.json({ success: 0, message: "Error" });
  }
});

router.post("/add_rate", testToken, async (req, res) => {
  try {
    let logAddRate = {
      "url":"add_rate",
      "request":req.body,
      "createdDateTime":new Date()
    };
    mongoObj.collection("add_rate_log").insertOne(logAddRate);
    let centralID = req.body["centralID"];
    let rate = req.body["rate"];
    await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",centralID).update({"status":1});
    let params = { data: rate, messageType: "fancy" };
    axios
      .post(process.env.CENTRAL_URL + centralID, params)
      .then(response => {
        res.json({ success: 1, message: "Rate Added." });
      })
      .catch(error => {
        res.json({ success: 0, message: "Error while updating market data." });
      });
  } catch (err) {
    res.json({ success: 0, message: "Error" });
  }
});

router.post("/get_sport_list", testToken, (req, res) => {
  var arr = [];
  var args = { IsAustralian: "false" };
  soap.createClient(url, function(err, client) {
    client.GetlistEventTypes(args, function(err, result) {
      res.send(result["GetlistEventTypesResult"]);
    });
  });
});

router.post("/get_tournament_list", testToken, (req, res) => {
  var arr = [];
  var args = { strEventTypeId: req.body.strEventTypeId, IsAustralian: "false" };
  soap.createClient(url, function(err, client) {
    client.getlistCompititions(args, function(err, result) {
      res.send(result["getlistCompititionsResult"]);
    });
  });
});

router.post("/get_match_list", testToken, (req, res) => {
  var arr = [];
  var args = {
    strCompetitionIds: req.body.strCompetitionId,
    IsAustralian: "false"
  };
  soap.createClient(url, function(err, client) {
    client.getlistEvents(args, function(err, result) {
      res.send(result["getlistEventsResult"]);
    });
  });
});

router.post("/get_market_list", testToken, (req, res) => {
  var arr = [];
  var args = { strEventIds: req.body.strEventID, IsAustralian: "false" };
  soap.createClient(url, function(err, client) {
    client.getlistMarketCatalogue(args, function(err, result) {
      res.send(result["getlistMarketCatalogueResult"]);
    });
  });
});
router.post("/verify_token", testToken, (req, res) => {
  res.send({ status: true });
});
router.post("/import_market", testToken, async (req, res) => {
  var sport_id = req.body.sport_id;
  var tournament_id = req.body.tournament_id;
  var event_id = req.body.event_id;
  var market_id = req.body.market_id;
  if (market_id == null || market_id == "" || market_id == undefined) {
    res.send({
      status: "error",
      message: "Please Pass market ID"
    });
    return;
  }
  let results = await knex(utils.TBL_CENTRAL_MARKET).where("sourceMarketID",market_id).select();
  if (results == null || results == [] || results == undefined || results.length == 0) {
    let get_market_url = "http://rohitash.dream24.bet:3000/getmarket?id=" + market_id;
        let response = axios.get(get_market_url);
        let logImportMarketData = {
          "url":get_market_url,
          "params":req.body,
          "response":response,
          "createdDateTime":new Date()
        };
          if (response.length == 0) {
            res.json({
              error: "Record not found!"
            });
            logImportMarketData.importSuccess = 0;
            mongoObj.collection("import_market").insertOne(logImportMarketData);
            return;
          } else {
            logImportMarketData.importSuccess = 1;
            mongoObj.collection("import_market").insertOne(logImportMarketData);
            response_parse = response.data;
            if (response_parse.marketId == undefined || response_parse.marketId == null ||response_parse.marketId == "") {
              res.json({
                data: {
                  centralID: ""
                }
              });
            } else {
              var newCentralMarket = {
                sourceMarketID: response_parse.marketId,
                competitionID: sport_id,
                tournamentID: tournament_id,
                matchID: event_id,
                // "marketName": market_name,
                // "startTime": "",
                // "type": "",
                status: response_parse.status,
                // "Result": "",
                sourceID: 1,
                isBetAllow: 1,
                marketType: 1
                // "connectionCount": "",
                // "numberOfRunners": ""
              };
              let centralID = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket).returning("centralMarketID");
              res.json({
                data: {
                  centralID: centralID
                }
              });
            }
          }
      } else {
        res.json({
          data: {
            centralID: results[0].centralMarketID
          }
        });
      }
});

router.post("/import_matchodds_bs",testToken, async (req, res) => {
  let authObj = new auth();
  let eventTypeID = req.body.eventTypeID;
  let eventTypeName = req.body.eventTypeName;
  let competitionID = req.body.competitionID;
  let competition = req.body.competition;
  let eventID = req.body.eventID;
  let eventName = req.body.eventName;
  let openDate = req.body.openDate;
  let marketID = req.body.marketID;
  let marketName = req.body.marketName;
  let betStatus = req.body.betStatus;
  let marketRate = req.body.marketRate;
  let isBetAllow = req.body.isBetAllow;
  let isFancy = req.body.isFancy;
  let fancyType = req.body.fancyType;
  let isActive = req.body.isActive;
  let isAustralian = req.body.isAustralian;
  let isUnmatchedAllow = req.body.isUnmatchedAllow;
  let isApplyCommision = req.body.isApplyCommision;
  let rules = req.body.rules;
  let ballStartSec =
    req.body.ballStartSec == undefined ? 0 : req.body.ballStartSec;
  let isInplay = req.body.isInplay;
  let isManualInplay = req.body.isManualInplay;
  if (marketID == null || marketID == "" || marketID == undefined) {
    res.send({
      success:false,
      status: "error",
      message: "Please Pass market ID"
    });
    return;
  }
  let results = await knex(utils.TBL_CENTRAL_MARKET).select().where("sourceMarketID",marketID);
  
  let centralID = 0;
  if (results == null || results == [] || results == undefined || results.length == 0) {
    var get_market_url =
      "http://rohitash.dream24.bet:3000/getmarket?id=" + marketID;
    let response = await axios.get(get_market_url);
    let logImportMarketData = {
      "url":get_market_url,
      "params":req.body,
      "response":response,
      "createdDateTime":new Date()
    };
    if (response.length == 0) {
      logImportMarketData.importSuccess = 0;
      mongoObj.collection("import_market").insertOne(logImportMarketData);
      res.json({
        data: {
          centralID: ""
        }
      });
    } else {
      logImportMarketData.importSuccess = 1;
      mongoObj.collection("import_market").insertOne(logImportMarketData);
      response_parse = response.data;
      if (response_parse.marketId == undefined || response_parse.marketId == null || response_parse.marketId == "") {
        res.json({
          data: {
            centralID: ""
          }
        });
      } else {
        var newCentralMarket = {
          sourceMarketID: response_parse.marketId,
          competitionID: eventTypeID,
          competitionName: eventTypeName,
          tournamentID: competitionID,
          tournamentName: competition,
          matchID: eventID,
          matchName: eventName,
          marketName: marketName,
          matchDate: openDate,
          betStatus: betStatus,
          isBetAllow: isBetAllow,
          marketType: 1,
          description: rules,
          isInplay: isInplay,
          status: response_parse.status,
          sourceID: 1
        };
        centralID = await knex(utils.TBL_CENTRAL_MARKET).insert(newCentralMarket).returning("centralMarketID");
      }
    }
  } else {
    centralID = results[0].centralMarketID;
  }
  let apiUrlResults = await knex(utils.TBL_AGENT).distinct("import_api_url","bs_secret_key","bs_access_token").where("id","in",knex("user_agent").where("user",1).select("agent"));
  if (apiUrlResults.length) {
    for (let row of apiUrlResults) {
      let secret_key = row.bs_secret_key;
      let access_token = row.bs_access_token;
      let isValidAccessToken = false;
      if (access_token != undefined && access_token.length > 0) {
        let result1 = await authObj.IsValidToken(
          row.import_api_url,
          access_token
        );
        if (result1.status == 200) {
          isValidAccessToken = true;
        }
      }

      if (!isValidAccessToken) {
        var tokenResponse = await authObj.GetAccessToken(
          row.import_api_url,
          secret_key
        );
        if (tokenResponse.status != 200) {
        } else {
          access_token = tokenResponse.result.token;
          await knex(utils.TBL_AGENT).where("import_api_url",row.import_api_url).update({ bs_access_token: access_token });
          isValidAccessToken = true;
        }
      }
      //Call api to import market to Website

      if (isValidAccessToken) {
        let params = {
          eventTypeID: eventTypeID,
          eventTypeName: eventTypeName,
          competitionID: competitionID,
          competition: competition,
          eventID: eventID,
          eventName: eventName,
          openDate: openDate,
          marketID: marketID,
          marketName: marketName,
          centralizationID: centralID,
          betStatus: betStatus,
          marketRate: marketRate,
          isBetAllow: isBetAllow,
          isFancy: isFancy,
          fancyType: fancyType,
          isActive: isActive,
          isAustralian: isAustralian,
          isUnmatchedAllow: isUnmatchedAllow,
          isApplyCommision: isApplyCommision,
          rules: rules,
          ballStartSec: ballStartSec,
          isInplay: isInplay,
          isManualInplay: isManualInplay,
          runners: req.body.runners
        };
        axios
          .post(row.import_api_url + "AddNewMatchodds", params, {
            headers: { Authorization: "Bearer " + access_token }
          })
          .then(response => {
            var logDataAddFancy = {
              "url":row.import_api_url + "AddNewMatchodds",
              "request":params,
              "auth":access_token,
              "createdDateTime":new Date(),
              "response":response
            };
            mongoObj.collection("import_market_bs").insertOne(logDataAddFancy);
          })
          .catch(error => {
            //console.log(error);
            // res.json({success: 0,message: 'Error while updating market data.'})
          });
      }
    }
  }
  res.json({
    data: {
      centralID: centralID
    }
  });
});
router.post("/get_status",testToken ,async (req, res) => {
  var strCentralizedID = req.body.strCentralizedID;
  let logDataGetStatus = {
    "url":row.import_api_url + "get_status",
    "request":req.body,
    "createdDateTime":new Date()
  };
  if (strCentralizedID == null || strCentralizedID == "" || strCentralizedID == undefined) {
    let resp = {
      success:false,
      status: "error",
      message: "Please Pass Central ID"
    };
    logDataGetStatus.response = resp;
    mongoObj.collection("get_status_logs").insertOne(logDataGetStatus);
    res.send(resp);
  }else{
    let results = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",strCentralizedID).select();
      if (results == null || results == [] || results == undefined || results.length == 0) {
        res.send({
          success:false,
          status: "error",
          message: "Invalid Central ID"
        });
      } else {
        let isInPlay = true;
        let isBetAllow = true;
        if (results[0].isInPlay == undefined || results[0].isInPlay == null || results[0].isInPlay == 0) {
          isInPlay = false;
        }
        if (results[0].isBetAllow == undefined || results[0].isBetAllow == null || results[0].isBetAllow == 0) {
          isBetAllow = false;
        }
        let resp = {
          data: {
            appCentralizedID: results[0].centralMarketID,
            appMarketStatus: results[0].status,
            appIsInPlay: isInPlay,
            appIsSystemBetAllow: isBetAllow
          }
        };
        logDataGetStatus.response = resp;
        mongoObj.collection("get_status_logs").insertOne(logDataGetStatus);
        res.json(resp);
      }
  }
});
router.post("/get_result", testToken, async (req, res) => {
  var strCentralizedID = req.body.strCentralizedID;
  let logDataGetResult = {
    "url":row.import_api_url + "get_result",
    "request":req.body,
    "createdDateTime":new Date()
  };
  if (strCentralizedID == null || strCentralizedID == "" || strCentralizedID == undefined) {
    let resp = {
      status: "error",
      message: "Please Pass Central ID"
    };
    logDataGetResult.response = resp;
    mongoObj.collection("get_result_logs").insertOne(logDataGetResult);
    res.send(resp);
  }
  let results = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID",strCentralizedID).select();
  if (results == null || results == [] || results == undefined || results.length == 0) {
    let resp = {
      success:false,
      status: "error",
      message: "Invalid Central ID"
    }
    logDataGetResult.response = resp;
    mongoObj.collection("get_result_logs").insertOne(logDataGetResult);
    res.send(resp);
  } else {
    let resp = {
      data: {
        result: results[0].Result
      }
    };
    logDataGetResult.response = resp;
    mongoObj.collection("get_result_logs").insertOne(logDataGetResult);
    res.json(resp);
  }
});
router.post("/get_access_token", function(req, res) {
  let ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
  ip = ip.split(":").pop();
  let agent_code = req.body.agentcode;
  let secret_key = req.body.secretkey;

  let whereConditions = {
    agentcode: agent_code,
    secretkey: secret_key
  };
  let data = {
    agentid: agent_code,
    datetime: new Date().getTime()
  };
  common_model
    .getRowWhereOrAccessToken("tblagent", "*", whereConditions)
    .then(result => {
      if (result.length > 0) {
        // Username password match then generate token.
        jwt.sign(data, jwtSecret, (err, token) => {
          // Error while generating token.
          if (err) {          
            res.json({ err_message: "Error while setting token." });
            return;
          }
          res.json({ token: token, agentcode: agent_code });
        });
      } else {
        res.json({ err_message: "Error while setting token." });
        return;
      }
    })
    .catch(err => {
      console.log(err);
      res.json({ err_message: "Error while setting access agent." });
    });
});
module.exports = router;