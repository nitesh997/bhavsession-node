// Dependencies.
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var https = require('https');
app.use(bodyParser.json());
const axios = require('axios');
var utils = require('../lib/utils');
var auth = require('../lib/kc_auth');
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
const sequilze = require('../sequlizeDB');
// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

/* Add market. */
router.post('/add', verifyToken, async (req, res) => {
  try{
    var authObj = new auth();
    var authData = jwt.verify(req.token, jwtSecret);
    let dataKeys = [
      'matchID', 'matchName', 'matchDate', 'tournamentID', 'tournamentName', 'competitionID',
      'competitionName', 'marketName', 'betStatus', 'maxStack', 'isCommissionOn',
      'rateDifference', 'autoBallStartDuration', 'description', 'status', 'isActive',
      'marketType', 'afterBetAllowFalse', 'afterSuspend', 'afterClose'
    ];
    let data = {};
    let runnerList = req.body.runnerList;

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      data[dataKeys[i]] = value;
    }
    data.createdBy = authData.userID;
    data.marketType = utils.MARKET_TYPE_BOOKMAKER;
    var resultAddMarket = await common_model.addData(utils.TBL_CENTRAL_MARKET, data);
    for(let i in runnerList){
      let runnerObject = runnerList[i];
      runnerObject['marketID'] = resultAddMarket.insertId;
    }
    var response = resultAddMarket;
    var result = await common_model.addDataBatch('tblrunner',runnerList);
    var resultAllAgents = await common_model.getRowWhereJoin3('ua.agent',{'tm.createdBy':authData.userID,'tm.centralMarketID':resultAddMarket.insertId},
    'tblcentralmarket tm','user_agent ua','tm.createdBy=ua.user','tblagent ta','ta.id=ua.agent');
    let agents=[];
    for(let i in resultAllAgents){
      let agent = {};
      let created_on = new Date().toISOString();
      agent['fancy']=resultAddMarket.insertId;
      agent['agent']=resultAllAgents[i].agent;
      agent['isDeleted']=0;
      agents.push(agent);
    }
    var result = await common_model.addDataBatch('fancy_agent',agents);
    let results = await sequilze.query(`select distinct import_api_url,bs_secret_key,bs_access_token from tblagent where id in (select agent from user_agent where user = ${authData.userID})`, {type: sequilze.QueryTypes.SELECT});
    if(results.length) {
        for(let row of results) {
        try{
          let secret_key = row.bs_secret_key;
          let access_token = row.bs_access_token;
          let isValidAccessToken = false;
        if(access_token != undefined && access_token.length > 0)
        {
          var result = await authObj.IsValidToken(row.import_api_url,access_token);
          if(result.status == 200)
          {
            isValidAccessToken = true;
          }
        }
          if (!isValidAccessToken) {
            var tokenResponse = await authObj.GetAccessToken(row.import_api_url,secret_key);
            if (tokenResponse.status != 200) {
              console.log('Error while adding market data.');
            } else {
              access_token = tokenResponse.result.token;
              await common_model.updateData(utils.TBL_AGENT, { bs_access_token: access_token }, { import_api_url: row.import_api_url });
            }
        }
            
            if(isValidAccessToken)
            {
            // call add new fancy api
            let params = {
              "marketID": data["matchID"],
              "marketName": data["marketName"],
              "eventID": data["matchID"],
              "eventName": data["matchName"],
              "competitionID": data["tournamentID"],
              "competitionName": data["tournamentName"],
              "eventTypeID": data["competitionID"],
              "eventTypeName": data["competitionName"],
              "betStatus": 0,
              "marketRate": 0,
              "isBetAllow": true,
              "isActive": false,
              "isFancy": true,
              "fancyType": 7,
              "status": 2,
              "centralizationID": resultAddMarket.insertId,
              "rateDifferent": data["rateDifference"],
              "maxStack": data["maxStack"],
              "isApplyCommision": true,
              "rules": data["description"],
              "ballStartSec": data["autoBallStartDuration"],
              "isInplay": true,
              "isCheckVolume": true,
              "afterClose": "2019-11-15T19:39:21.970Z",
              "afterSuspend": "2019-11-15T19:39:21.970Z",
              "afterBetAllowFalse": "2019-11-15T19:39:21.970Z",
              "siteCode": "101",
              "siteGroup": ""
            };
            params["runners"] = [];
            var runnersList = await common_model.getAllDataWithoutisDelete(utils.TBL_RUNNER, "id,name", { "marketID": resultAddMarket.insertId });
            for (let row in runnersList) {
              let runner = {};
              runner.selectionID_BF = runnersList[row].id;
              runner.title = runnersList[row].name;
              params["runners"].push(runner);
            }
            console.log(params);
            axios.post(row.import_api_url + 'AddNewFancy', params, {
              headers: { "Authorization": "Bearer " + access_token }
            }).then(response => {
              console.log('response.. inserted successfully');
            }).catch(error => {
              console.log('error while inserting.. ', error);
              // res.json({ err_message: error });
            });
            }
          } catch(err) {
            console.log(err);
            res.json({err_message: 'Error while adding market data.'});
          }
        }
     
       
      }
      res.json({ status: 200 });
    } catch(err) {
    console.log(err);
    res.json({err_message: 'Error while adding market data.'});
  }
});

/* Add market rate json. */
router.post('/add_rate_json', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let dataKeys = [
      'marketCode', 'rateJson'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if ('undefined' === typeof value) {
        res.json({err_message: 'Invalid market data.'});
        return;
      }

      data[dataKeys[i]] = value;
    }
    data.createdBy = authData.userID;

    // Add market data.
    return common_model.addData('tblmarketrate', data)
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while adding market data.'});
    });
  });

});


/* Update market. */
router.post('/update/:id', verifyToken, function(req, res) {
  
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    let marketID = escape(req.params.id);
    if (typeof marketID === 'undefined') {
      res.json({err_message: 'Invalid market data.'});

      return;
    }
    marketID = parseInt(marketID.trim(), 10);

    let dataKeys = [
      'matchID', 'matchName', 'matchDate', 'tournamentID', 'tournamentName', 'competitionID', 'competitionName',
      'marketCode', 'marketName', 'betStatus', 'maxStack', 'isFancyCode', 'isCommissionOn', 'rateDifference',
      'autoBallStartDuration', 'description', 'status', 'isActive', 'isDelete', 'Result'
    ];
    let data = {};

    for (let i in dataKeys) {
      let value = req.body[dataKeys[i]];
      if (typeof value !== 'undefined') {
        data[dataKeys[i]] = value.trim();
      }
    }

    let length = 0;
    for (let i in data) {
      length ++;
      break;
    }

    if (!length) {
      res.json({err_message: 'Invalid market data.'});
      return;
    }

    common_model.updateData(utils.TBL_CENTRAL_MARKET, data, {'centralMarketID': marketID})
    .then(result => res.json({result}))
    .catch(err => {
      console.log(err);
      res.json({err_message: 'Error while updating market data.'})
    });

  });

});

router.get('/get_list', verifyToken, function(req, res) {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      // Send forbidden if token is wrong/doesn't exist.
      res.sendStatus(403);
      return;
    }

    common_model.getRowWhereOrderBy(utils.TBL_CENTRAL_MARKET,'*',{'marketType':utils.MARKET_TYPE_BOOKMAKER},'centralMarketID','desc')
    .then(result => {
      res.json({market: result});
      return;
    }).catch(err => {
      console.log(err);
      res.json({err_message: 'Error occurred while getting market data.'})
    });

  });
});

router.get('/get_details/:id', verifyToken, (req, res) => {
  jwt.verify(req.token, jwtSecret, (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(403);
      return;
    }

    var id = escape(req.params.id);
    if (!id) {
      res.send({err_message: 'Invalid market ID'});
      return;
    }

    id = parseInt(id.trim(), 10);
    if ('' === id) {
      res.send({err_message: 'Invalid market ID'});
      return;
    }
    
    common_model.getRowWhere(utils.TBL_CENTRAL_MARKET, '*', {centralMarketID: id}).then(result => {
      var result1 = result[0];
      common_model.getRowWhereJoin('tr.id as runner_id, tr.name as runner_name',{'tm.centralMarketID':id},
      'tblcentralmarket tm','tblrunner tr','tm.centralMarketID=tr.marketID').then(result2 => {
        result1.runnerList=result2;
        res.json({market: result1});
        return;
      }).catch(err2 => {
        console.log(err2);
        throw err2;
      });
    }).catch(err1 => {
      console.log(err1);
      res.json({err_message: 'Error while getting market data.'});
    });
  });
});

let addResponsesToDb = function(responses,marketID){
  for(let i in responses){
    let response = responses[i];
    common_model.updateData('fancy_agent',{'api_response_details':response.body,
      'api_response_code':response.status},{'fancy':marketID,'agent':response.agent})
    .then(result => {
      console.log('API response updated for market:'+marketID);
    }).catch(err2 => {
      console.log(err2);
      console.log(err2.stack);
    });
  }
};

module.exports = router;