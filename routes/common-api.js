// Dependencies.
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
app.use(bodyParser.json());
var axios = require("axios");
//var url = "http://localhost:3000/api/";
var url = "http://localhost:3000/api/";
// Middlewares.
var verifyToken = require('../middlewares/verify_token');
var testToken = require('../middlewares/test_token');


// Models.
var common_model = require('../models/common_model');

// Globals.
const jwtSecret = process.env.JWT_SECRET;

router.get('/testToken', testToken, function(req, res) {
  res.send({message: 'Token tested successfully!'});
});

router.post('/getAccessToken', function(req, res) {

  let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  let domain_name = req.get('origin');
  let agent_code = req.body.agentcode;
  let secret_key = req.body.secretkey;

  let whereConditions = {
    'domain_name': domain_name,
    'agentcode': agent_code,
    'secretkey': secret_key
  };

  let or_where_conditions = [
    {
      'ip1': ip
    },
    {
      'ip2': ip
    },
    {
      'ip3': ip
    },
    {
      'ip4': ip
    }
  ];

  let data = {
    'agentid': agent_code,
    'datetime': new Date().getTime()
  }

  common_model.getRowWhereOrAccessToken('tblagent','*',whereConditions,or_where_conditions)
  .then(result => {
    // Username password match then generate token.
    jwt.sign(data, jwtSecret, (err, token) => {
      // Error while generating token.
      if (err) {
        console.log(err);
        res.json({err_message: 'Error while setting token.'});
        return;
      }
      // Send token.
      res.json({token});
    });
  })
  .catch(err => {
    console.log(err);
    res.json({err_message: 'Error while getting access agent.'})
  });
});

router.get('/updateBetStatus/:id/:betStatus', verifyToken, function(req, res) {
    jwt.verify(req.token, jwtSecret, (err, authData) => {
      if (err) {
        console.log(err);
        res.sendStatus(403);
        return;
      }
      var marketID = req.params.id;
      if (!marketID || '' === marketID) {
        res.send({err_message: 'Invalid market ID'});
        return;
      }
  
      marketID = parseInt(marketID.trim(), 10);
      let betStatus = req.params.betStatus;
      let allowedValues = ['Active','Inactive'];
      if (!betStatus || '' === betStatus|| allowedValues.indexOf(betStatus)==-1) {
        res.send({err_message: 'Invalid bet status'});
        return;
      }
  
      let data = {
        'betStatus': betStatus,
        'updatedDateTime': new Date().toISOString()
      };
  
      common_model.updateData('tblmarket', data, {'marketID': marketID})
      .then(result => {
        res.json({result});
      })
      .catch(err => {
        console.log(err);
        res.json({err_message: 'Error while updating market data.'})
      });
    }); 
  });
  
  router.get('/updateBetCommission/:id/:isCommissionOn', verifyToken, function(req, res) {
    jwt.verify(req.token, jwtSecret, (err, authData) => {
      if (err) {
        console.log(err);
        res.sendStatus(403);
        return;
      }
      var marketID = req.params.id;
      if (!marketID || '' === marketID) {
        res.send({err_message: 'Invalid market ID'});
        return;
      }
  
      marketID = parseInt(marketID.trim(), 10);
      let isCommissionOn = parseInt(req.params.isCommissionOn);
      let allowedValues = [0,1];
      if (allowedValues.indexOf(isCommissionOn)==-1) {
        res.send({err_message: 'Invalid flag'});
        return;
      }
  
      let data = {
        'isCommissionOn': isCommissionOn,
        'updatedDateTime': new Date().toISOString()
      };
  
      common_model.updateData('tblmarket', data, {'marketID': marketID})
      .then(result => {
        res.json({result});
      })
      .catch(err => {
        console.log(err);
        res.json({err_message: 'Error while updating market data.'})
      });
    }); 
  });
  
  router.get('/updateMaxStack/:id/:maxStack', verifyToken, function(req, res) {
    jwt.verify(req.token, jwtSecret, (err, authData) => {
      if (err) {
        console.log(err);
        res.sendStatus(403);
        return;
      }
      var marketID = req.params.id;
      if (!marketID || '' === marketID) {
        res.send({err_message: 'Invalid market ID'});
        return;
      }
  
      marketID = parseInt(marketID.trim(), 10);
      let maxStack = parseInt(req.params.maxStack);
      if (!maxStack || '' === maxStack) {
        res.send({err_message: 'Invalid value for max stack.'});
        return;
      }
  
      let data = {
        'maxStack': maxStack,
        'updatedDateTime': new Date().toISOString()
      };
  
      common_model.updateData('tblmarket', data, {'marketID': marketID})
      .then(result => {
        res.json({result});
      })
      .catch(err => {
        console.log(err);
        res.json({err_message: 'Error while updating market data.'})
      });
    }); 
  });
  
  router.get('/updateAutoBallStartDuration/:id/:autoBallStartDuration', verifyToken, function(req, res) {
    jwt.verify(req.token, jwtSecret, (err, authData) => {
      if (err) {
        console.log(err);
        res.sendStatus(403);
        return;
      }
      var marketID = req.params.id;
      if (!marketID || '' === marketID) {
        res.send({err_message: 'Invalid market ID'});
        return;
      }
  
      marketID = parseInt(marketID.trim(), 10);
      let autoBallStartDuration = parseInt(req.params.autoBallStartDuration);
      if (!autoBallStartDuration || '' === autoBallStartDuration) {
        res.send({err_message: 'Invalid value for autoball start duration.'});
        return;
      }
  
      let data = {
        'autoBallStartDuration': autoBallStartDuration,
        'updatedDateTime': new Date().toISOString()
      };
  
      common_model.updateData('tblmarket', data, {'marketID': marketID})
      .then(result => {
        res.json({result});
      })
      .catch(err => {
        console.log(err);
        res.json({err_message: 'Error while updating market data.'})
      });
    }); 
  });

  router.get('/updateStatus/:id/:status', verifyToken, function(req, res) {
    jwt.verify(req.token, jwtSecret, (err, authData) => {
      if (err) {
        console.log(err);
        res.sendStatus(403);
        return;
      }
      var marketID = req.params.id;
      if (!marketID || '' === marketID) {
        res.send({err_message: 'Invalid market ID'});
        return;
      }
  
      marketID = parseInt(marketID.trim(), 10);
      let status = parseInt(req.params.status);
      let allowedValues = [0,1,2,3];
      if (allowedValues.indexOf(status)==-1) {
        res.send({err_message: 'Invalid flag'});
        return;
      }
  
      let data = {
        'status': status,
        'updatedDateTime': new Date().toISOString()
      };
  
      common_model.updateData('tblmarket', data, {'marketID': marketID})
      .then(result => {
        res.json({result});
      })
      .catch(err => {
        console.log(err);
        res.json({err_message: 'Error while updating market data.'})
      });
    }); 
  });

  
  router.post('/get_sport_list', async (req, res) => {
    let params = { "refID": "", "isaustralian": false,"requestFrom":"self" };
    let response = await axios.post(url + 'get_sport_list', params);
    let body = response.data;
    res.send(body);
  });
  
  router.post('/get_tournament_list',async  (req, res) => {
    let params = { "strEventTypeId": req.body.strEventTypeId,"requestFrom":"self"};
    let response = await axios.post(url + 'get_tournament_list', params);
    let body = response.data;
    res.send(body);
  });
  
  router.post('/get_match_list',async  (req, res) => {
    let params = { "strCompetitionId": req.body.strCompetitionId ,"requestFrom":"self"};
    let response = await axios.post(url + 'get_match_list', params);
    let body = response.data;
    res.send(body);
  });
  
  router.post('/get_market_list',async  (req, res) => {
    let params = { "strEventID": req.body.strEventID,"requestFrom":"self"};
    let response = await axios.post(url + 'get_market_list', params);
    let body = response.data;
    res.send(body);
  });
module.exports = router;