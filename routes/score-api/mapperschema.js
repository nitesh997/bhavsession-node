var moment = require('moment');

module.exports = {
    news: {
        description: 'description',
        updated: 'updated',
        title: 'title',
        description_text: 'description_text',
        news_url: 'link'
    },
    recentMatches: {
        match_id: 'key',
        match_name: 'name',
        short_name: 'short_name',
        title: 'title',
        related_name: 'related_name',
        team1: { team_id: () => 1, name: 'teams.a.name', team_key: 'teams.a.key', season_team_key: 'teams.a.match.season_team_key' },
        team2: { team_id: () => 2, name: 'teams.b.name', team_key: 'teams.b.key', season_team_key: 'teams.b.match.season_team_key' },
        match_date: (iteratee, source, destination) => {
            if (iteratee.start_date.iso) {
                return moment(iteratee.start_date.iso).format("DD-MM-YYYY hh:mm A");
            }
            return null;
        },
        toss: 'toss.str',
        match_status: 'status',
        status: 'msgs.completed',
        status_str: 'msgs.result',
        winner_team: (iteratee, source, destination) => {
            if (iteratee.winner_team == 'a') {
                return { name: iteratee.teams.a.name, team_key: iteratee.teams.a.key, season_team_key: iteratee.teams.a.match.season_team_key };
            }
            else if (iteratee.winner_team == 'b') {
                return { name: iteratee.teams.b.name, team_key: iteratee.teams.b.key, season_team_key: iteratee.teams.b.match.season_team_key };
            }
            return null;
        },
        venue: 'venue',
        format: 'format',
        season: { season_key: 'season.key', season_name: 'season.name' }
    },
    recentMatcheWithScore: {
        match_id: 'key',
        match_name: 'name',
        short_name: 'short_name',
        title: 'title',
        day: "day",
        related_name: 'related_name',
        team1: { team_id: () => 1, name: 'teams.a.name', team_key: 'teams.a.key', season_team_key: 'teams.a.match.season_team_key' },
        team2: { team_id: () => 2, name: 'teams.b.name', team_key: 'teams.b.key', season_team_key: 'teams.b.match.season_team_key' },
        match_date: (iteratee, source, destination) => {
            if (iteratee.start_date.iso) {
                return moment(iteratee.start_date.iso).format("DD-MM-YYYY hh:mm A");
            }
            return null;
        },
        toss: 'toss.str',
        match_status: 'status',
        status: 'msgs.completed',
        status_str: 'msgs.result',
        innings: (iteratee, source, destination) => {
            var battingOrder = iteratee.batting_order.reverse();
            var inningsScore = iteratee.innings;
            var matchinnings = [];
            battingOrder.forEach(element => {
                var innings = element[0] + "_" + element[1];
                var score = inningsScore[innings];
                var matchScore = {
                    run_rate: score["run_rate"],
                    score: score["runs"],
                    decl: score["decl"],
                    wickets: score["wickets"],
                    overs: score["overs"],
                    run_str: score["run_str"],
                    team_id: 1,
                    team: 'team1',
                    inning: element[1]
                }
                if (element[0] == 'b') {
                    matchScore.team_id = 2;
                    matchScore.team = "team2";
                }
                matchinnings.push(matchScore);
            });
            return matchinnings;
        },
        winner_team: (iteratee, source, destination) => {
            if (iteratee.winner_team == 'a') {
                return { name: iteratee.teams.a.name, team_key: iteratee.teams.a.key, season_team_key: iteratee.teams.a.match.season_team_key };
            }
            else if (iteratee.winner_team == 'b') {
                return { name: iteratee.teams.b.name, team_key: iteratee.teams.b.key, season_team_key: iteratee.teams.b.match.season_team_key };
            }
            return null;
        },
        venue: 'venue',
        format: 'format',
        season: { season_key: 'season.key', season_name: 'season.name' }
    },
    matchPlayingXI: {
        match_id: 'key',
        match_name: 'name',
        short_name: 'short_name',
        title: 'title',
        related_name: 'related_name',
        team1: {
            team_id: () => 1,
            name: 'teams.a.name',
            team_key: 'teams.a.key',
            players: (iteratee, source, destination) => {
                var players = [];
                var playersDetails = iteratee.teams.a.match.playing_xi;
                if (playersDetails == null || playersDetails == undefined)
                    return players;

                playersDetails.forEach(playerKey => {
                    var playerWholeDetails = iteratee.players[playerKey];
                    var playerObj = {
                        name: playerWholeDetails.name,
                        fullname: playerWholeDetails.fullname,
                        captain: iteratee.teams.a.match.captain == playerKey,
                        match_keeper: iteratee.teams.a.match.keeper == playerKey,
                        keeper: playerWholeDetails.identified_roles.keeper,
                        bowler: playerWholeDetails.identified_roles.bowler,
                        batsman: playerWholeDetails.identified_roles.batsman,
                        season_role: playerWholeDetails.seasonal_role
                    };
                    players.push(playerObj);
                });
                return players;
            }
        },
        team2: {
            team_id: () => 2,
            name: 'teams.b.name',
            team_key: 'teams.b.key',
            players: (iteratee, source, destination) => {
                var players = [];
                var playersDetails = iteratee.teams.b.match.playing_xi;
                if (playersDetails == null || playersDetails == undefined)
                    return players;
                playersDetails.forEach(playerKey => {
                    var playerWholeDetails = iteratee.players[playerKey];
                    var playerObj = {
                        name: playerWholeDetails.name,
                        fullname: playerWholeDetails.fullname,
                        captain: iteratee.teams.b.match.captain == playerKey,
                        match_keeper: iteratee.teams.b.match.keeper == playerKey,
                        keeper: playerWholeDetails.identified_roles.keeper,
                        bowler: playerWholeDetails.identified_roles.bowler,
                        batsman: playerWholeDetails.identified_roles.batsman,
                        season_role: playerWholeDetails.seasonal_role
                    };
                    players.push(playerObj);
                });
                return players;
            }
        }
    },
    matchscorecard: {
        match_id: 'key',
        match_name: 'name',
        short_name: 'short_name',
        title: 'title',
        related_name: 'related_name',
        venue: 'venue',
        format: 'format',
        season: { season_key: 'season.key', season_name: 'season.name' },
        match_date: (iteratee, source, destination) => {
            if (iteratee.start_date.iso) {
                return moment(iteratee.start_date.iso).format("DD-MM-YYYY hh:mm A");
            }
            return null;
        },
        team1: {
            team_id: () => 1,
            name: 'teams.a.name',
            team_key: 'teams.a.key'
        },
        team2: {
            team_id: () => 2,
            name: 'teams.b.name',
            team_key: 'teams.b.key'
        },
        toss: 'toss.str',
        match_status: 'status',
        status: 'msgs.completed',
        status_str: 'msgs.result',
        innings: (iteratee, source, destination) => {
            var battingOrder = iteratee.batting_order;
            battingOrder.reverse();
            var inningsScore = iteratee.innings;
            var matchinnings = [];
            battingOrder.forEach(element => {
                var innings = element[0] + "_" + element[1];
                var score = inningsScore[innings];
                var matchScore = {
                    run_rate: score["run_rate"],
                    score: score["runs"],
                    decl: score["decl"],
                    wickets: score["wickets"],
                    overs: score["overs"],
                    run_str: score["run_str"],
                    team_id: 1,
                    team: 'team1',
                    inning: element[1],
					key: innings
                }
                if (element[0] == 'b') {
                    matchScore.team_id = 2;
                    matchScore.team = "team2";
                }
                matchinnings.push(matchScore);
            });
            return matchinnings;
        },
        winner_team: (iteratee, source, destination) => {
            if (iteratee.winner_team == 'a') {
                return { name: iteratee.teams.a.name, team_key: iteratee.teams.a.key, season_team_key: iteratee.teams.a.match.season_team_key };
            }
            else if (iteratee.winner_team == 'b') {
                return { name: iteratee.teams.b.name, team_key: iteratee.teams.b.key, season_team_key: iteratee.teams.b.match.season_team_key };
            }
            return null;
        },
        partnership: (iteratee, source, destination) => {
            //already reverse batting order in above innings object
            var battingOrder = iteratee.batting_order;
            if (battingOrder.length < 1) {
                return null;
            }

            var recentBallDetails = iteratee.now.recent_overs[0];
            var recentOver = recentBallDetails[0];
            var recentBall = recentBallDetails[1][0];

            var bollDetails = iteratee.balls[recentBall];
            var striker = bollDetails.striker;

            var inningNo = battingOrder[0][1];
            var teamno = battingOrder[0][0];
            var inning = teamno + "_" + inningNo;
            var partnerships = iteratee.innings[inning].partnerships;
            var lastpar = partnerships[partnerships.length - 1];
            var player1Details = iteratee.players[lastpar.player_a];
            var player2Details = iteratee.players[lastpar.player_b];

            var batting1Details = player1Details.match.innings[inningNo]["batting"];
            var batting2Details = player2Details.match.innings[inningNo]["batting"];

            var partnership = {
                runs: lastpar.runs,
                balls: lastpar.balls,
                run_rate: lastpar.run_rate,
                dismissed: lastpar.dismissed,
                player_1: {
                    player_name: player1Details.name,
                    player_fullname: player1Details.fullname,
                    partnership_runs: lastpar.player_a_runs,
                    partnership_balls: lastpar.player_a_balls,
                    is_striker: striker == lastpar.player_a,
                    dismissed: batting1Details.dismissed,
                    runs: batting1Details.runs,
                    balls: batting1Details.balls,
                    strike_rate: batting1Details.strike_rate,
                    fours: batting1Details.fours,
                    sixes: batting1Details.sixes,
                },
                player_2: {
                    player_name: player2Details.name,
                    player_fullname: player2Details.fullname,
                    partnership_runs: lastpar.player_b_runs,
                    partnership_balls: lastpar.player_b_balls,
                    is_striker: striker == lastpar.player_b,
                    dismissed: batting2Details.dismissed,
                    runs: batting2Details.runs,
                    balls: batting2Details.balls,
                    strike_rate: batting2Details.strike_rate,
                    fours: batting2Details.fours,
                    sixes: batting2Details.sixes
                }
            }
            return partnership;
        },
        bowler: (iteratee, source, destination) => {
            // "recent_overs": [
            //     [
            //         65,
            //         [
            //             "b8f7615b-973a-40aa-9185-4658833acac0",
            //             "0912b7ce-b14f-4e78-8ede-b30226740972"
            //         ]
            //     ],
            var recentBallDetails = iteratee.now.recent_overs[0];
            var recentOver = recentBallDetails[0];
            var recentBall = recentBallDetails[1][0];

            var bollDetails = iteratee.balls[recentBall];
            var striker = bollDetails.striker;
            var playerWholeDetails = iteratee.players[bollDetails.bowler.key];

            var bowlingDetails = playerWholeDetails.match.innings[bollDetails.innings]["bowling"];
            var bowlerDetails = {
                player_name: playerWholeDetails.name,
                player_fullname: playerWholeDetails.fullname,
                economy: bowlingDetails.economy,
                overs: bowlingDetails.overs,
                runs: bowlingDetails.runs,
                wickets: bowlingDetails.wickets,
                maiden_overs: bowlingDetails.maiden_overs
            }
            return bowlerDetails;
        },
        commentary: []
    },
    fullscorecard: {
        match_id: 'key',
        match_name: 'name',
        short_name: 'short_name',
        title: 'title',
        related_name: 'related_name',
        venue: 'venue',
        format: 'format',
        season: { season_key: 'season.key', season_name: 'season.name' },
        team1: {
            team_id: () => 1,
            name: 'teams.a.name',
            team_key: 'teams.a.key'
        },
        team2: {
            team_id: () => 2,
            name: 'teams.b.name',
            team_key: 'teams.b.key'
        },
        innings: (iteratee, source, destination) => {
            var battingOrder = iteratee.batting_order.reverse();
            var inningsScore = iteratee.innings;
            var matchinnings = [];
            battingOrder.forEach(element => {
                var inningNo = element[1];
                var teamA = element[0];
                var innings = teamA + "_" + inningNo;
                var teamB = teamA == 'b' ? 'a' : 'b';
                var teamBInning = teamB + "_" + inningNo;
                var score = inningsScore[innings];
                var matchScore = {
                    team_id: 1,
                    team: 'team1',
                    inning: inningNo,
                    run_rate: score["run_rate"],
                    score: score["runs"],
                    decl: score["decl"],
                    wickets: score["wickets"],
                    overs: score["overs"],
                    run_str: score["run_str"],
                    balls: score["balls"],
                    fours: score["fours"],
                    sixes: score["sixes"],
                    wickets: score["wickets"],
                    extras: score["extras"],
                    wide: score["wide"],
                    noball: score["noball"],
                    legbye: score["legbye"],
                    bye: score["bye"],
                    penalty: score["penalty"],
                    player_battings: [],
                    player_blowing: []
                }
                if (element[0] == 'b') {
                    matchScore.team_id = 2;
                    matchScore.team = "team2";
                }

                var playerBattingOrder = score["batting_order"];
                playerBattingOrder.forEach(batsmankey => {
                    var playerWholeDetails = iteratee.players[batsmankey];
                    var battingDetails = playerWholeDetails.match.innings[matchScore.inning]["batting"];
                    var batmanDetails = {
                        player_name: playerWholeDetails.name,
                        player_fullname: playerWholeDetails.fullname,
                        dismissed: battingDetails.dismissed,
                        runs: battingDetails.runs,
                        balls: battingDetails.balls,
                        fours: battingDetails.fours,
                        sixes: battingDetails.sixes,
                        strike_rate: battingDetails.strike_rate,
                        wicket_details: {}
                    }

                    if (batmanDetails.dismissed) {
                        var ballOfDismissed = battingDetails.ball_of_dismissed;
                        var bowler_details = iteratee.players[ballOfDismissed.bowler.key];
                        var wicket_type = {
                            catch: "0",
                            bowled: "1",
                            runout: "2",
                            lbw: "3",
                            stumbed: "4",
                            retirehut: "5",
                            hitwicket: "6"
                        };
                        var wicketDetails = {
                            bowler_name: bowler_details.name,
                            bowler_fullname: bowler_details.fullname,
                            wicket_type: ballOfDismissed.wicket_type,
                            wickey_typeid: wicket_type[ballOfDismissed.wicket_type],
                            fielder_name: "",
                            fielder_fullname: "",
                            dismissed_over: (battingDetails.dismissed_at.over - 1) + "." + battingDetails.dismissed_at.ball,
                            team_score: battingDetails.dismissed_at.team_runs + "-" + battingDetails.dismissed_at.wicket_index,
                            wicket_number: battingDetails.dismissed_at.wicket_index
                        };
                        var filderKey = ballOfDismissed.fielder["key"];
                        if (filderKey && filderKey != "") {
                            var filderDetails = iteratee.players[filderKey];
                            wicketDetails.fielder_name = filderDetails.name;
                            wicketDetails.fielder_fullname = filderDetails.fullname;
                        }
                        batmanDetails.wicket_details = wicketDetails;
                    }
                    matchScore.player_battings.push(batmanDetails);
                });

                var battingTeamDetails = inningsScore[teamBInning];
                var playerBowlingOrder = battingTeamDetails["bowling_order"];
                playerBowlingOrder.forEach(bowlerKey => {
                    var playerWholeDetails = iteratee.players[bowlerKey];
                    var bowlingDetails = playerWholeDetails.match.innings[matchScore.inning]["bowling"];
                    var bowlerDetails = {
                        player_name: playerWholeDetails.name,
                        player_fullname: playerWholeDetails.fullname,
                        dots: bowlingDetails.dots,
                        runs: bowlingDetails.runs,
                        fours: bowlingDetails.fours,
                        sixes: bowlingDetails.sixes,
                        balls: bowlingDetails.balls,
                        maiden_overs: bowlingDetails.maiden_overs,
                        wickets: bowlingDetails.wickets,
                        extras: bowlingDetails.extras,
                        overs: bowlingDetails.overs,
                        economy: bowlingDetails.economy
                    }
                    matchScore.player_blowing.push(bowlerDetails);
                });
                matchinnings.push(matchScore);
            });
            return matchinnings;
        }
    }
}