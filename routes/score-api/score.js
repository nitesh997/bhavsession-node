var express = require('express'),
    router = express.Router();
var CricketAPIServices = require('./services');
var morphism = require('morphism');
var schema = require('./mapperschema');
var knex = require("../../config");
var utils = require("../../lib/utils");
var axios = require("axios");

router.get('/schedule/', function (req, res) {
    CricketAPIServices.scheduleResponse(null).then((response) => {
        res.json(response);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/schedule/:month/', function (req, res) {
    CricketAPIServices.scheduleResponse(req.params['month']).then((response) => {
        res.json(response);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/recent-matches/', function (req, res) {
    CricketAPIServices.recentMatchesResponse().then((response) => {
        res.json(response);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/match/:matchId/', function (req, res) {
    CricketAPIServices.getMatchResponse(req.params['matchId']).then((response) => {
        res.json(response);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/getnews/', function (req, res) {
    CricketAPIServices.getNewsResponse().then((response) => {
        const newsData = response;
        var news = [];
        if (newsData != undefined && newsData.news != undefined && newsData.news.length > 0) {
            var news = morphism.morphism(schema.news, newsData.news);
            news = news;
        }
        res.json(news);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/dashboard/', function (req, res) {

    Promise.all([CricketAPIServices.recentMatchesResponse(), CricketAPIServices.getNewsResponse()]).then(async data => {
        const recentMatches = data[0];
        const newsData = data[1];
        var response = {
            matches: {
                live: [],
                upcoming: [],
                recent: []
            },
            news: []
        }
        if (recentMatches != undefined && recentMatches.cards != undefined && recentMatches.cards.length > 0) {
            var matches = morphism.morphism(schema.recentMatches, recentMatches.cards);
            var scoreofMatches = matches.filter(x => x.match_status == "started" || x.match_status == "completed");
            var matchesWithScore = [];
            var promises = [];
            scoreofMatches.forEach((match) => {
                promises.push(CricketAPIServices.getMatchResponse(match.match_id));
            });
            await Promise.all(promises).then(data => {
                data.forEach((match) => {
                    if (match != undefined && match.card != undefined) {
                        matchesWithScore.push(match.card);
                    }
                });
            });
            var convertedMatches = morphism.morphism(schema.recentMatcheWithScore, matchesWithScore);
            response.matches.live = convertedMatches.filter(x => x.match_status == "started");
            response.matches.upcoming = matches.filter(x => x.match_status == "notstarted");
            response.matches.recent = convertedMatches.filter(x => x.match_status == "completed");
        }
        if (newsData != undefined && newsData.news != undefined && newsData.news.length > 0) {
            var news = morphism.morphism(schema.news, newsData.news);
            response.news = news;
        }
        res.json(response);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/matches/', function (req, res) {

    CricketAPIServices.scheduleResponse().then(async data => {
        var response = {
            live: {
                t20: [],
                odi: [],
                test: []
            },
            upcoming: {
                t20: [],
                odi: [],
                test: []
            },
            recent: {
                t20: [],
                odi: [],
                test: []
            }
        }
        if (data == undefined) {
            return res.json(response);
        }
        var allmatches = [];
        var nextMonth = data.next_month, previousMonth = data.prev_month;
        var months = data.months;
        months.forEach((month) => {
            var days = month.days;
            days.forEach((day) => {
                var matches = day.matches;
                allmatches.push.apply(allmatches, matches);
            });
        });

        var matches = morphism.morphism(schema.recentMatches, allmatches);
        var scoreofMatches = matches.filter(x => x.match_status == "started" || x.match_status == "completed");
        var matchesWithScore = [];
        var promises = [];
        scoreofMatches.forEach((match) => {
            promises.push(CricketAPIServices.getMatchResponse(match.match_id));
        });
        await Promise.all(promises).then(data => {
            data.forEach((match) => {
                if (match != undefined && match.card != undefined) {
                    matchesWithScore.push(match.card);
                }
            });
        });
        var convertedMatches = morphism.morphism(schema.recentMatcheWithScore, matchesWithScore);
        var liveMatchs = convertedMatches.filter(x => x.match_status == "started");
        var upcomingMatchs = matches.filter(x => x.match_status == "notstarted");
        var recentMatchs = convertedMatches.filter(x => x.match_status == "completed");

        response.live.t20 = liveMatchs.filter(x => x.format == "t20");
        response.live.odi = liveMatchs.filter(x => x.format == "one-day");
        response.live.test = liveMatchs.filter(x => x.format == "test");

        response.upcoming.t20 = upcomingMatchs.filter(x => x.format == "t20");
        response.upcoming.odi = upcomingMatchs.filter(x => x.format == "one-day");
        response.upcoming.test = upcomingMatchs.filter(x => x.format == "test");

        response.recent.t20 = recentMatchs.filter(x => x.format == "t20");
        response.recent.odi = recentMatchs.filter(x => x.format == "one-day");
        response.recent.test = recentMatchs.filter(x => x.format == "test");

        res.json(response);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/getteams/:matchId/', function (req, res) {
    CricketAPIServices.getMatchResponse(req.params['matchId']).then((response) => {
        var match = {};
        if (response != undefined && response.card != undefined) {
            match = morphism.morphism(schema.matchPlayingXI, response.card);
        }
        res.json(match);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/getfullscorecard/:matchId/', function (req, res) {
    CricketAPIServices.getMatchResponse(req.params['matchId']).then((response) => {
        var match = {};
        if (response != undefined && response.card != undefined) {
            match = morphism.morphism(schema.fullscorecard, response.card);
        }
        res.json(match);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/getscore/:matchId/', function (req, res) {
    CricketAPIServices.getMatchResponse(req.params['matchId']).then((response) => {
        var match = {};
        if (response != undefined && response.card != undefined) {
            match = morphism.morphism(schema.matchscorecard, response.card);
        }
        res.json(match);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});

router.get('/getlivematches/', function (req, res) {
    CricketAPIServices.recentMatchesResponse().then(async (response) => {
        const recentMatches = response;
        var liveMatchs = [];
        if (recentMatches != undefined && recentMatches.cards != undefined && recentMatches.cards.length > 0) {
            var matches = morphism.morphism(schema.recentMatches, recentMatches.cards);
            var scoreofMatches = matches.filter(x => x.match_status == "started");
            var matchesWithScore = [];
            var promises = [];
            scoreofMatches.forEach((match) => {
                promises.push(CricketAPIServices.getMatchResponse(match.match_id));
            });
            await Promise.all(promises).then(data => {
                data.forEach((match) => {
                    if (match != undefined && match.card != undefined) {
                        matchesWithScore.push(match.card);
                    }
                });
            });
            var convertedMatches = morphism.morphism(schema.recentMatcheWithScore, matchesWithScore);
            liveMatchs = convertedMatches.filter(x => x.match_status == "started");
        }
        res.json(liveMatchs);
    }).catch(function (err) {
        console.error('Oops we have an error', err);
    })
});


router.get('/getcentraldetails/:matchId/', async function (req, res) {
    var matchKey = req.params['matchId'];
    var apiresponse = {
        isValid: false,
        errors: [],
        data: null
    }
    let results = await knex(utils.TBL_FIXTURE).distinct("betfairID").where("type", 1).where("fixtureID", matchKey).select("betfairID");
    if (results == null || results.length < 1)
        return res.json(apiresponse);
    var betfairID = results[0].betfairID;
    let centralMarketResults = await knex(utils.TBL_CENTRAL_MARKET).where("centralMarketID", betfairID).select();
    var centralMarketDetails = centralMarketResults[0];
    var market_id = centralMarketDetails.sourceMarketID;

    var diff = 0.0;
    if(centralMarketDetails.matchDate != undefined)
        diff = Date.parse(centralMarketDetails.matchDate) - new Date();

    var market_data = {
        matchInfo :[
            {
                "appMatchID": centralMarketDetails.matchID,
                "appMatch": centralMarketDetails.matchName,
                "appDate": centralMarketDetails.matchDate,
                "appEventID_BF": centralMarketDetails.tournamentID,
                "appInPlay": centralMarketDetails.isInPlay == 1,
                "appRemTime": diff > 0 ? diff : 0.0
            }
        ],
        "marketInfo": [
            {
                "appBetID": parseInt(betfairID),
                "appBetName": "Match Odds",
                "appMatchID": centralMarketDetails.matchID,
                "appMarketID_BF": parseInt(betfairID),
                "appFancyType": centralMarketDetails.fancyType,
                "appCentralizationID": betfairID,
                "appMarketStatus": parseInt(centralMarketDetails.status),
                "appIsInplay": centralMarketDetails.isInPlay == 1
            }
        ],
        "runnerInfo": []
    }

    let runnersInfo = await knex(utils.TBL_RUNNER).where("marketID", betfairID).select();
    if(runnersInfo != null){
        runnersInfo.forEach(element => {
            var runnerInfo = {
                "appBetDetailID": element.tpRunnerID,
                "appBetTitle": element.name,
                "appSelectionID_BF": element.tpRunnerID,
                "appBetID": parseInt(betfairID)
            }
            market_data.runnerInfo.push(runnerInfo);
        });
    }
    apiresponse.data = market_data;
    apiresponse.isValid = true;   
    res.json(apiresponse);
});

router.get('/getcommentry/:matchId/:overKey/', function (req, res) {
    CricketAPIServices.getMatchBallByBallResponse(req.params['matchId'],req.params['overKey']).then(async (response) => {
        const matchOverDetails = response; console.log(matchOverDetails);
    var overComments = {};
    if (matchOverDetails != undefined && matchOverDetails.balls != undefined && matchOverDetails.over.balls.length > 0) {
       // var overComments = morphism.morphism(schema.overCommentry, matchOverDetails.data);
       overComments.prev_over = matchOverDetails.pre_over != undefined ? matchOverDetails.pre_over : '';
       overComments.next_over = matchOverDetails.next_over != undefined ? matchOverDetails.next_over : '';
       overComments.comment = [];
        for(key in matchOverDetails.over.balls){
            overComments.comment.push({
                [key] : matchOverDetails.balls[matchOverDetails.over.balls[key]].comment
            });
        }
    }
    res.json(overComments);
}).catch(function (err) {
    console.error('Oops we have an error', err);
})
});
module.exports = router;