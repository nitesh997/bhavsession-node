require('dotenv').config();

function envFor(name, defaultValue = null){
    if (process.env[`SC_${name}`] != undefined) {
        return process.env[`SC_${name}`]
    }
    return defaultValue
}


class ScoreConfig {	
    constructor() {         
        this.backend = {
            host: 'https://rest.cricketapi.com',
            spiderHost: 'https://rest.cricketapi.com',
            path: '/',
            };
        
        this.auth = {
            app_id: envFor("APP_ID"), // get it from .env
            access_key: envFor("ACCESS_KEY"),
            secret_key: envFor("SECRET_KEY"),
            device_id: envFor("DEVICE_ID")
        }
    }
  }

module.exports = new ScoreConfig();     