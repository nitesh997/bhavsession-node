const con = require('../db');

// marketModel is used to access its methods eg. marketModel.getMarketList()
marketModel = {

    /**
     * Function to get market list.
     */
    getMarketList: () => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select('m.*, mt.matchName, DATE_FORMAT(mt.matchDateTime, "%d/%m/%Y") matchDateTime', false).from('tblmarket m')
                .join('tblmatch mt', 'mt.matchID=m.matchID', 'inner')
                .where({ 'm.isDelete': 0 })
                .get((err, response) => {
                    if (err) {
                        reject(err);
                        return;
                    }

                    resolve(response);
                });

        });
    },

    getRunners: (id) => {
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select('*', false).from('tblrunner')
                .where({ 'marketId': id })
                .get((err, response) => {
                    if (err) {
                        reject(err);
                        return;
                    }

                    resolve(response);
                });
        });
    }
}

module.exports = marketModel;
