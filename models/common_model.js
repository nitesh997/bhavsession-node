const con = require('../db');

// userModel is used to access its methods eg. userModel.getUser()
commonModel = {

    /**
     * Function to get all data.
     *
     * @param String tableName Name of the table.
     * @param String columns Columns to select.
     * @param Object Where conditions.
     */
    getAllData: (tableName, columns = '*', where = {'isDelete': 0}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .where({'isDelete': 0})
            .get(tableName, (err, response) => {

                if (err) {
                    reject(err);

                    return;
                }

                resolve(response);
            });

        });
    },
    getcentralMarketIDs: (tableName, columns = '*', where = {'isDelete': 0}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .where({'isDelete': 0})
            .get(tableName, (err, response) => {

                if (err) {
                    reject(err);

                    return;
                }

                resolve(response);
            });

        });
    },
    getrunnerNAme: (tableName, columns, where = {'isDelete': 0}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(tableNamey)
            .distinct(columns)
            .where(where)
            .where({'isDelete': 0})
            .get(tableName, (err, response) => {

                if (err) {
                    reject(err);

                    return;
                }

                resolve(response);
            });

        });
    },
    /**
     * Function to get all data.
     *
     * @param String tableName Name of the table.
     * @param String columns Columns to select.
     * @param Object Where conditions.
     */
    getAllDataWithoutisDelete: (tableName, columns = '*', where = {'isDelete': 0}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .get(tableName, (err, response) => {

                if (err) {
                    reject(err);

                    return;
                }

                resolve(response);
            });

        });
    },

    /**
     * Function to get row data.
     *
     * @param String tableName Name of the table.
     * @param String columns Columns to select.
     * @param Object Where conditions.
     */
    getRow: (tableName, columns = '*', where = {'isDelete': 0}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .where({'isDelete': 0}).limit(1)
            .get(tableName, (err, response) => {
                if (err) {
                    reject(err+con.last_query());

                    return;
                }

                resolve(response);
            });

        });
    },
    /**
     * Function to get row data.
     *
     * @param String tableName Name of the table.
     * @param String columns Columns to select.
     * @param Object Where conditions.
     */
    getRowWithoutisDelete: (tableName, columns = '*', where) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where).limit(1)
            .get(tableName, (err, response) => {
                if (err) {
                    reject(err+con.last_query());

                    return;
                }

                resolve(response);
            });

        });
    },
    getRowWhere: (tableName, columns, where) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .get(tableName, (err, response) => {
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    getRowWhereOrderBy: (tableName, columns, where, order_by_column, order) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .order_by(order_by_column,order)
            .get(tableName, (err, response) => {
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    getRowWhereJoin: (select, where, from, join, on) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(select)
            .where(where)
            .from(from)
            .join(join,on,'inner')
            .get( (err, response) => {
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    getRowWhereJoin3: (select, where, from, join, on, join1, on1) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(select)
            .where(where)
            .from(from)
            .join(join,on,'inner')
            .join(join1,on1,'inner')
            .get( (err, response) => {
                
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    getRowWhereJoinDistinct: (select, where, from, join, on) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.distinct()
            .select(select)
            .where(where)
            .from(from)
            .join(join,on,'inner')
            .get( (err, response) => {
                
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    getRowWhereOrAccessToken: (tableName, columns, where) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .get(tableName, (err, response) => {
                
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    getLastRate: (tableName, columns = '*', where = {'isDelete': 0}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns, false)
            .where(where)
            .where({'isDelete': 0})
            .order_by('marketRateID','DESC').limit(1)
            .get(tableName, (err, response) => {
                if (err) {
                    reject(err+con.last_query());

                    return;
                }

                resolve(response);
            });

        });
    },
    
    getRowWhereINJoin: (select, from, join, on,isActive,where,Ids) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(select)
            .from(from)
            .join(join,on,'inner')
            .where(isActive)
            .where_in(where,Ids)
            .get( (err, response) => {
                if (err) {
                    reject(err+con.last_query());
                    return;
                }
                resolve(response);
            });
        });
    },
    /**
     * Function to get only active data.
     *
     * @param String tableName Name of the table.
     * @param String columns Columns to select.
     * @param Object where Where conditions.
     */
    getActiveData: (tableName, columns = '*', where = {'isActive': 1}) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.select(columns)
            .where(where)
            .where({'isActive': 1, 'isDelete': 0})
            .get(tableName, (err, response) => {

                if (err) {
                    reject(err);

                    return;
                }

                resolve(response);
            });

        });
    },

    /**
     * Function to add data.
     *
     * @param String tableName Name of the table.
     * @param Object data Data to insert.
     */
    addData: (tableName, data) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.insert(tableName, data, (err, result) => {
                console.log(data)
                if (err) {
                    reject(err);
                }

                resolve(result);
            });

        });
    },

    /**
     * Function to add multiple rows.
     *
     * @param String tableName Name of the table.
     * @param Object data Data to insert.
     */
    addDataBatch: (tableName, data) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.insert_batch(tableName, data, (err, result) => {
                if (err) {
                    reject(err);
                }

                resolve(result);
            });

        });
    },

    /**
     * Function to update data.
     *
     * @param String tableName Name of the table.
     * @param Object updateFields Data to update.
     * @param Object whereFields Where conditions.
     */
    updateData: (tableName, updateFields, whereFields) => {
        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query, concat updateValues and whereValues.
            con.update(tableName, updateFields, whereFields, (err, results) => {
                if (err) {
                    reject(err);
                }

                resolve(results);
            });

        });
    }
}

module.exports = commonModel;
