const con = require('../db');

// userModel is used to access its methods eg. userModel.getUser()
userModel = {

    /**
     * Function to get data.
     * 
     * @param Array fields Array of field names.
     * @param Array values Array of field values.
     */
    getUser: (fields, values) => {
        let query = 'SELECT * FROM '+ tables.TBL_USER +' WHERE 1=1';
    
        // Set fields with prepared statement.
        for (let i in fields) {
            query += ' AND ' + fields[i] + '=?';
        }

        // Use promise instead of callbacks.
        return new Promise((resolve, reject) => {
            // Execute query.
            con.query(query, values, (err, results, fields) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }

                resolve(results, fields);
            });

        });
    }
}

module.exports = userModel;
