const QueryBuilder = require('node-querybuilder');
const settings = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME
};
// const settings = {
//   host: process.env.DB_HOST,
//   user: process.env.DB_USER,
//   password: process.env.DB_PASS,
//   database: process.env.DB_NAME
// };
const con = new QueryBuilder(settings, 'mysql', 'single');

module.exports = con;
