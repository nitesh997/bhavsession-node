/**
 * Function to verify token for protected routes.
 * 
 * @param Object req Request object.
 * @param Object res Response object.
 * @param Function next Function to call next middleware.
 */
var jwt = require('jsonwebtoken');
var knex = require("../config");
var MongoClient = require('mongodb').MongoClient;
var mongoObj = undefined;
var url = "mongodb://localhost:27017/";
//var url = "mongodb://mongouser:Shashank123@127.0.0.1:27017/?authSource=admin&readPreference=primary&gssapiServiceName=mongodb&appname=MongoDB%20Compass&ssl=false";
MongoClient.connect(url, function(err, db) {
    if (err) 
        console.log(err);
    mongoObj = db.db("centralserver");
});

 // Globals.
const jwtSecret = process.env.JWT_SECRET;

const testToken = async (req, res, next) => {
    const token = req.body.access_token;
    const requestFrom = req.body.requestFrom;
    if(requestFrom == 'self'){
        next();
    }else{
        if((token == null || token == undefined || token == ''))
        {
            mongoObj.collection("token_rejections").insertOne({token:token,createdDateTime:new Date()});
            res.sendStatus(403);
        }else
        {
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            ip = ip.split(":").pop();

            if (typeof token !== 'undefined') {
                req.token = token;
                try{
                    let decoded = jwt.verify(token, jwtSecret);
                    try{
                        let result = await knex("tblagent").where("agentcode",decoded.agentid).select();

                        if(result!=null && result!=undefined && result.length>0){
                            let requestData = {
                                'agent_username': result[0].username,
                                'api_name': req.originalUrl,
                                'ip_address':req.ip,
                                'datetime':  new Date()
                            };
                            let resultAddData = await knex("tblrequestlog").insert(requestData);
                            res.locals.agentcode = decoded.agentid;
                            //We are veryfying only IP. Domain name to be added.
                            // if(ip==result[0].ip1 || ip==result[0].ip2 || ip==result[0].ip3 || ip==result[0].ip4 || ip == '116.72.16.165' || ip=='150.107.232.199'){
                            next();
                            // }
                            // else{
                            //     mongoObj.collection("token_rejections").insertOne({token:token,ip:ip,agentcode:decoded.agentid,createdDateTime:new Date()});
                            //     res.sendStatus(401);
                            // }
                        }else{
                            mongoObj.collection("token_rejections").insertOne({token:token,ip:ip,agentcode:decoded.agentid,createdDateTime:new Date()});
                            res.sendStatus(401);
                        }
                    }catch(ex)
                    {
                        mongoObj.collection("token_rejections").insertOne({token:token,ip:ip,agentcode:decoded.agentid,createdDateTime:new Date()});
                        res.sendStatus(500);
                    }
                }catch(err){
                    mongoObj.collection("token_rejections").insertOne({token:token,ip:ip,agentcode:decoded.agentid,createdDateTime:new Date()});
                    if(err.name=="TokenExpiredError" || err.name == "JsonWebTokenError")
                        res.sendStatus(403);
                    else
                        res.sendStatus(401);
                }
            } else {
                // Send forbidden.
                res.sendStatus(401);
            }
        }
    }
};

module.exports = testToken;
