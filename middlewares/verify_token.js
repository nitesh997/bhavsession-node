/**
 * Function to verify token for protected routes.
 * 
 * @param Object req Request object.
 * @param Object res Response object.
 * @param Function next Function to call next middleware.
 */
const verifyToken = (req, res, next) => {
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const token = bearer[1];
        req.token = token;

        // Next middleware.
        next();
    } else {
        // Send forbidden.
        res.sendStatus(403);
    }
};

module.exports = verifyToken;
